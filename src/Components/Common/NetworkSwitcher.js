import React, { PureComponent } from 'react';

export default class NetworkSwitcher extends PureComponent {
  handleToggleNetwork() {
    const { props } = this;
    const { value: on } = props.NetWorkModeState;
    if (on) {
      props.networkModeOffAction();
    } else {
      props.networkModeOnAction();
    }
  }

  render() {
    const { props } = this;
    const { label, value: on } = props.NetWorkModeState;
    let className = 'btn btn-lg btn-warning';
    if (on) {
      className = 'btn btn-lg btn-warning active';
    }
    return (
      <button
        type="button"
        className={className}
        data-toggle="button"
        aria-pressed="false"
        autoComplete="off"
        onClick={() => this.handleToggleNetwork()}
      >
        {label}
      </button>
    );
  }
}
