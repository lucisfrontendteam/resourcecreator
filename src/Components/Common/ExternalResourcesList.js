import React, { PureComponent } from 'react';
import { Accordion, Panel } from 'react-bootstrap';
import NetworkSwitcher from './NetworkSwitcher';
import {
  WidgetList,
  WidgetThemeList,
  WidgetViewerLayoutList,
  ProjectThemeList,
} from '../../Containers/Common';

export default class ExternalResourcesList extends PureComponent {
  render() {
    const { props } = this;
    return (
      <div className="row">
        <div
          style={{ paddingBottom: '5px' }}
          className="col-sm-offset-1 col-sm-10 text-center"
        >
          <NetworkSwitcher {...props} />
        </div>
        <div className="col-sm-offset-1 col-sm-10 text-center">
          <Accordion>
            <Panel header="프로젝트 테마" eventKey="0">
              <ProjectThemeList {...props} />
            </Panel>
            <Panel header="위젯" eventKey="1">
              <WidgetList {...props} />
            </Panel>
            <Panel header="위젯 테마" eventKey="2">
              <WidgetThemeList {...props} />
            </Panel>
            <Panel header="레이아웃 리스트" eventKey="3">
              <WidgetViewerLayoutList {...props} />
            </Panel>
          </Accordion>
        </div>
      </div>
    );
  }
}
