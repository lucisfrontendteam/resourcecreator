import React, { PureComponent } from 'react';
import SelectedWidgetViewerLayout from
  '../../Containers/WidgetViewerLayouts/SelectedWidgetViewerLayout';

export default class WidgetViewer extends PureComponent {
  render() {
    const { props } = this;
    return (<SelectedWidgetViewerLayout {...props} />);
  }
}
