import React, { PureComponent } from 'react';
import AdvencedFilter from '../../../../userdashboard/src/Components/AdvencedFilter';

export default class AdvencedFilterContainer extends PureComponent {
  render() {
    const { props } = this;
    return (
      <div style={{ height: '300px' }}>
        <AdvencedFilter {...props} />
      </div>
    );
  }
}
