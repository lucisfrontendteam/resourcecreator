import React, { PureComponent } from 'react';
import $ from 'jquery';
import { v4 } from 'uuid';

export default class ResourceList extends PureComponent {
  renderItems() {
    const { props } = this;

    return (props.TargetListState || []).map(targetState => {
      const { name, displayName, selected } = targetState;
      const className = `treeview ${selected ? 'bg-yellow' : ''}`;

      return (
        <li
          key={v4()}
          className={className}
          onClick={() => props.onItemClick(displayName)}
        >
          <a href="#">
            <p>{name}</p>
            <p>{`[ ${displayName} ]`}</p>
          </a>
        </li>
      );
    });
  }

  setUpToggleSideBar() {
    $(document).on('mousemove', $e => {
      if ($e.pageX <= 230) {
        $('header.main-header').show();
        $('aside.main-sidebar').width(230);
        $('div.content-wrapper').css('margin-left', '230px');
      } else {
        $('header.main-header').hide();
        $('aside.main-sidebar').width(50);
        $('div.content-wrapper').css('margin-left', '50px');
      }
    });
  }

  componentDidMount() {
    // this.setUpToggleSideBar();
  }

  render() {
    return (
      <ul className="sidebar-menu">
        {this.renderItems()}
      </ul>
    );
  }
}
