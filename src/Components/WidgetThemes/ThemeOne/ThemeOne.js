import React, { PureComponent } from 'react';
import './custom.css';

export class ThemeOne extends PureComponent {
  static defaultProps = {
    name: '테마 1',
    type: 'WidgetTheme',
  };

  render() {
    const { props } = this;

    return (
      <div className="lucis-flip-container">
        <div className="lucis-flipper">
          <div className="front">
            {props.children}
          </div>
          <div className="back">
          </div>
        </div>
      </div>
    );
  }
}
