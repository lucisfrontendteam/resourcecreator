import DataTableDataAdapter from './DataTableDataAdapter';
import TextRenderer from '../../../Utils/TextRenderer';
import defaultDatas from './defaultDatas';

export default {
  columns: DataTableDataAdapter.getColumnsOption(defaultDatas),
  columnDefs: [
    {
      title: '원스톱 처리율',
      targets: 0,
      render: TextRenderer.renderPercentage,
      width: '25%',
    },
    {
      title: '지점 연결비율',
      targets: 1,
      render: TextRenderer.renderPercentage,
      width: '25%',
    },
    {
      title: '평균 통화시간',
      targets: 2,
      render: TextRenderer.renderLocaleDateDuration,
      width: '25%',
    },
    {
      title: '묵음비율',
      targets: 3,
      render: TextRenderer.renderLocaleDateDuration,
      width: '25%',
    },
  ],
};
