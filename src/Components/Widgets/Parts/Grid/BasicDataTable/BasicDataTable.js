import React, { PureComponent } from 'react';
import $ from 'jquery';
import { v4 } from 'uuid';
import 'datatables.net';
import 'datatables.net-buttons';
import 'datatables.net-fixedcolumns';
import '../../../../../Resources/js/dataTables.rowsGroup';
import defaultOptions from './defaultOptions';
import sampleOptions from './sampleOptions';
import defaultDatas from './defaultDatas';
import DataTableDataAdapter from './DataTableDataAdapter';
import './custom.css';
import WidgetDefaultProps from '../../../Utils/WidgetDefaultProps';

export class BasicDataTable extends PureComponent {
  static defaultProps = {
    name: '기본형 데이터테이블',
    datas: defaultDatas,
    options: {
      ...defaultOptions,
      ...sampleOptions,
    },
    tableClassName: 'lucis-table stripe',
  };

  constructor(props) {
    super(props);
    this.tableId = v4();
    this.shouldComponentUpdate = WidgetDefaultProps.widgetPartsSCU.bind(this);
  }

  get dataTableId() {
    const { props } = this;
    return props.tableId || this.tableId;
  }

  renderDataTable() {
    const { options, datas } = this.props;
    const equipedCallbacks = DataTableDataAdapter.getTargetCallBacks(options);
    const dataTableOptions = {
      ...defaultOptions,
      ...options,
      data: datas,
      columnDefs: options.columnDefs,
      columns: DataTableDataAdapter.getColumnsOption(datas),
      ...equipedCallbacks,
    };

    // eslint-disable-next-line new-cap
    this.table = $(document.getElementById(this.dataTableId)).DataTable(dataTableOptions);
  }

  destroyDataTable() {
    const { table } = this;
    if (table) {
      table.destroy();
    }
  }

  render() {
    const { props } = this;
    return <table id={this.dataTableId} className={props.tableClassName} />;
  }

  componentDidMount() {
    this.renderDataTable();
  }

  componentDidUpdate() {
    this.destroyDataTable();
    this.renderDataTable();
  }

  componentWillUnmount() {
    this.destroyDataTable();
  }
}
