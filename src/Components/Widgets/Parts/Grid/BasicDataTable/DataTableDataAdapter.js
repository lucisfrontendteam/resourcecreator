import _ from 'lodash';

export default class DataTableDataAdapter {
  static callBackList = [
    'createdRow',
    'drawCallback',
    'footerCallback',
    'formatNumber',
    'headerCallback',
    'infoCallback',
    'initComplete',
    'preDrawCallback',
    'rowCallback',
    'stateLoadCallback',
    'stateLoaded',
    'stateLoadParams',
    'stateSaveCallback',
    'stateSaveParams',
  ];

  static getColumnsOption(data) {
    const [firstElement = {}] = data;
    const columns = Object.keys(firstElement).map(key =>
      ({
        data: key,
      })
    );
    return columns;
  }

  static getTargetCallBacks(options) {
    const targetOptionsKeys = Object.keys(options);
    const availableCallbackKeys =
      _.intersection(DataTableDataAdapter.callBackList, targetOptionsKeys);
    const callbackProperty = {};
    availableCallbackKeys.forEach(key => {
      callbackProperty[key] = options[key];
    });
    return callbackProperty;
  }

  static getLanguageOption() {
    return {
      decimal: ',',
      emptyTable: '표시할 데이터가 없습니다.',
      info: 'Showing _START_ to _END_ of _TOTAL_ entries',
      infoEmpty: 'Showing 0 to 0 of 0 entries',
      infoFiltered: '(filtered from _MAX_ total entries)',
      infoPostFix: '',
      thousands: ',',
      lengthMenu: 'Show _MENU_ entries',
      loadingRecords: '로드중..',
      processing: '처리중..',
      search: '검색',
      zeroRecords: '일치하는 데이터가 없습니다',
      paginate: {
        first: '<<',
        last: '>>',
        next: '>',
        previous: '<',
      },
      aria: {
        sortAscending: '오름차순 정렬 켜기',
        sortDescending: '내림차순 정렬 켜기',
      },
    };
  }
}
