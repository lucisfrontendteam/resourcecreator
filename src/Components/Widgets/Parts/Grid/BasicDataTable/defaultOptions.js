import DataTableDataAdapter from './DataTableDataAdapter';

export default {
  searching: false,
  paging: false,
  ordering: true,
  info: false,
  lengthChange: false,
  pageLength: 25,
  language: DataTableDataAdapter.getLanguageOption(),
};
