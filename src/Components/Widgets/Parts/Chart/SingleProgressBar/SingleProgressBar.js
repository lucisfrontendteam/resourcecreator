import React, { PureComponent } from 'react';
import TextRenderer from '../../../Utils/TextRenderer';
import { v4 } from 'uuid';
import _ from 'lodash';
import sampleData from './sampleData';
import $ from 'jquery';
import 'bootstrap';

export class SingleProgressBar extends PureComponent {
  static defaultProps = {
    name: '싱글 프로그레스 바 차트',
    data: sampleData,
  };

  renderBar(value = 0, total = 0) {
    let seed = value / total;
    if (_.isNaN(seed)) {
      seed = 0;
    }
    const percentage = TextRenderer.renderPercentage(seed);
    const style = { width: percentage };
    const labelValue = TextRenderer.renderNumberWithGun(value);
    return (
      <div
        className="progress-bar"
        style={style}
        data-toggle="tooltip"
        data-placement="top"
        title={labelValue}
      >
        {this.renderBarLabel(value, percentage)}
      </div>
    );
  }

  renderBarLabel(value = 0, percentage) {
    return (<span>{percentage}</span>);
  }

  get total() {
    const { props } = this;
    const values = _.map(props.data, ({ value }) => value);
    const total = _.reduce(values, (sum, n) => sum + n, 0);
    return total;
  }

  renderRecords() {
    const { props, total } = this;
    return (
      props.data.map(record => {
        const { categoryName, value } = record;
        return (
          <tr key={v4()}>
            <th>{categoryName}</th>
            <td className="graph-r">
              <div className="progress">
                {this.renderBar(value, total)}
              </div>
            </td>
          </tr>
        );
      })
    );
  }

  componentDidMount() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <table className="widget-graph-02 type2 one th-wide">
        <tbody>
          {this.renderRecords()}
        </tbody>
      </table>
    );
  }
}
