export default [
  {
    // 카테고리 이름
    categoryName: '일반',
    // 카테고리의 건수
    value: 8,
  },
  {
    // 카테고리 이름
    categoryName: '클래식',
    // 카테고리의 건수
    value: 2,
  },
  {
    // 카테고리 이름
    categoryName: '베스트',
    // 카테고리의 건수
    value: 23,
  },
  {
    // 카테고리 이름
    categoryName: '에이스',
    // 카테고리의 건수
    value: 2,
  },
  {
    // 카테고리 이름
    categoryName: '기타',
    // 카테고리의 건수
    value: 6,
  },
];
