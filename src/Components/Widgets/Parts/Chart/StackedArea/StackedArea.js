import React, { PureComponent } from 'react';
import NVD3Chart from '../../Core/NVD3Chart';
import { v4 } from 'uuid';
import $ from 'jquery';
import sampleData from './sampleData.json';

export default class StackedArea extends PureComponent {
  static defaultProps = {
    name: 'StackedArea',
    datum: sampleData,
    options: {
      svgHeigth: 200,
    },
    renderChart() {
      const { props } = this;
      const { options, id } = props;
      // eslint-disable-next-line
      const stackedAreaOptions = {
        ...options,
      };

      window.nv.addGraph(() => {
        this.chart = window.nv.models.stackedAreaChart()
          .margin({ right: 100 })
          .x(d => d[0])
          .y(d => d[1])
          .useInteractiveGuideline(true)
          .rightAlignYAxis(true)
          .showControls(true)
          .clipEdge(true);

        this.chart.xAxis
          .tickFormat(d => window.d3.time.format('%Y/%m/%d')(new Date(d)));

        this.chart.yAxis
          .tickFormat(d => d.toFixed(2));

        window.d3.select(document.getElementById(id))
          .datum(props.datum)
          .call(this.chart);
      });
    },
    resizeChart() {
      const { id } = this;
      $(window).resize(() => {
        const $svg = $(document.getElementById(id));
        const width = $svg.parent().width();
        $svg.width(width);
        this.chart.update();
      });
    },
  };

  constructor(props) {
    super(props);
    this.id = v4();
  }

  render() {
    const { props, id } = this;
    return (<NVD3Chart {...props} id={id} />);
  }
}
