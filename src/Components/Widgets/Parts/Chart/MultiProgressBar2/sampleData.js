export default {
  chart: {
    // Tops 등급별 비율
    data: [
      {
        // 카테고리 이름
        categoryName: '일반',
        // 카테고리의 남성 비율
        leftValue: 0.5,
        // 카테고리의 여성 비율
        rightValue: 0.5,
      },
      {
        // 카테고리 이름
        categoryName: '클래식',
        // 카테고리의 남성 비율
        leftValue: 0.7,
        // 카테고리의 여성 비율
        rightValue: 0.3,
      },
      {
        // 카테고리 이름
        categoryName: '베스트',
        // 카테고리의 남성 비율
        leftValue: 0.4,
        // 카테고리의 여성 비율
        rightValue: 0.6,
      },
      {
        // 카테고리 이름
        categoryName: '에이스',
        // 카테고리의 남성 비율
        leftValue: 0.2,
        // 카테고리의 여성 비율
        rightValue: 0.8,
      },
      {
        // 카테고리 이름
        categoryName: '기타',
        // 카테고리의 남성 비율
        leftValue: 0.6,
        // 카테고리의 여성 비율
        rightValue: 0.4,
      },
    ],
  },
};
