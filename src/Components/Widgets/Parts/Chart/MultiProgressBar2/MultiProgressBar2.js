import React, { PureComponent } from 'react';
import TextRenderer from '../../../Utils/TextRenderer';
import { v4 } from 'uuid';
import _ from 'lodash';
import sampleData from './sampleData';
import $ from 'jquery';
import 'bootstrap';

export class MultiProgressBar2 extends PureComponent {
  static defaultProps = {
    name: '멀티 프로그레스 바 차트 2',
    data: sampleData,
  };

  renderBar(value = 0, total = 0) {
    let seed = value / total;
    if (_.isNaN(seed)) {
      seed = 0;
    }
    const percentage = TextRenderer.renderPercentage(seed);
    const style = { width: percentage };
    return (
      <div
        className="progress-bar"
        style={style}
        data-toggle="tooltip"
        data-placement="top"
        title={TextRenderer.renderNumberWithGun(value)}
      >
        {this.renderBarLabel(value, percentage)}
      </div>
    );
  }

  renderBarLabel(value = 0, percentage) {
    return (<span>{percentage}</span>);
  }

  renderRecords() {
    const { props } = this;
    return (
      props.data.map(record => {
        const { categoryName, leftValue, rightValue } = record;
        const total = leftValue + rightValue;
        return (
          <tr key={v4()}>
            <th>{categoryName}</th>
            <td className="graph-l">
              <div className="progress">
                {this.renderBar(leftValue, total)}
              </div>
            </td>
            <td className="graph-r">
              <div className="progress">
                {this.renderBar(rightValue, total)}
              </div>
            </td>
          </tr>
        );
      })
    );
  }

  componentDidMount() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  componentDidUpdate() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  render() {
    return (
      <table className="widget-graph-02 type2 th-wide">
        <tbody>
          {this.renderRecords()}
        </tbody>
      </table>
    );
  }
}
