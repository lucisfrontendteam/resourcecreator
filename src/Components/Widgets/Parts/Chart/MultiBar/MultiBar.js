import React, { PureComponent } from 'react';
import NVD3Chart from '../../Core/NVD3Chart';
import { v4 } from 'uuid';
import $ from 'jquery';
import sampleData from './sampleData';

export default class MultiBar extends PureComponent {
  static defaultProps = {
    name: 'MultiBar',
    datum: sampleData,
    options: {
      svgHeigth: 200,
    },
    renderChart() {
      const { props } = this;
      const { options, id } = props;
      // eslint-disable-next-line
      const multiBarOptions = {
        ...options,
      };

      window.nv.addGraph(() => {
        this.chart = window.nv.models.multiBarChart()
          .reduceXTicks(true)
          .rotateLabels(0)
          .showControls(true)
          .groupSpacing(0.8);

        this.chart.xAxis
          .tickFormat(window.d3.format(',f'));
        this.chart.yAxis
          .tickFormat(window.d3.format(',.1f'));

        this.chart.noData('데이터가 없습니다.');
        window.d3.select(document.getElementById(id))
          .datum(props.datum)
          .call(this.chart);
      });
    },
    resizeChart() {
      const { id } = this;
      $(window).resize(() => {
        const $svg = $(document.getElementById(id));
        const width = $svg.parent().width();
        $svg.width(width);
        this.chart.update();
      });
    },
  };

  constructor(props) {
    super(props);
    this.id = v4();
  }

  render() {
    const { props, id } = this;
    return (<NVD3Chart {...props} id={id} />);
  }
}
