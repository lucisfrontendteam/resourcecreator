import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import _ from 'lodash';
import TextRenderer from '../../../Utils/TextRenderer';
import sampleData from './sampleData';

export class MultiProgressBar extends PureComponent {
  static defaultProps = {
    name: '멀티 프로그레스 바 차트',
    data: sampleData,
  };

  getLabelText(record) {
    const {
      unit,
      totalTelTimes,
      totalTels,
      categoryTelTimes,
      categoryTels,
    } = record;
    const textRenderer =
      unit === 'datetime' ? TextRenderer.renderDateDuration :
        TextRenderer.renderAvgPercentage;
    const formattedTotalValue = textRenderer(totalTelTimes, totalTels);
    let formattedCategoryValue = '';
    if (categoryTels) {
      formattedCategoryValue = textRenderer(categoryTelTimes, categoryTels);
      formattedCategoryValue = `(${formattedCategoryValue})`;
    }
    const label = `${formattedTotalValue}${formattedCategoryValue}`;
    return label;
  }

  getPercentageValue(totalValue, categoryValue) {
    let result = (categoryValue / totalValue);
    result = _.isNaN(result) ? 0 : result;
    result = result > 1 ? 1 : result;
    return result;
  }

  getTotalBarValue(total) {
    // 50분
    const max = 3000;
    return this.getPercentageValue(max, total);
  }

  getCategoryBarValue(totalValue, categoryValue) {
    return this.getPercentageValue(totalValue, categoryValue);
  }

  getBarValues(record) {
    const {
      totalTelTimes,
      categoryTelTimes,
    } = record;
    const leftValue = this.getTotalBarValue(totalTelTimes);
    const rightValue = this.getCategoryBarValue(totalTelTimes, categoryTelTimes);
    return {
      leftValue,
      rightValue,
    };
  }

  renderBar(value = 0) {
    const percentage = TextRenderer.renderPercentage(value);
    const style = { width: percentage };
    return (
      <div className="progress-bar" style={style}>
        {this.renderBarLabel(value, percentage)}
      </div>
    );
  }

  renderBarLabel(value = 0, percentage) {
    let barLabel = '';
    if (value !== 0) {
      barLabel = <span>{percentage}</span>;
    }
    return barLabel;
  }

  renderRecords() {
    const { props } = this;
    return (
      props.data.map(record => {
        const { title } = record;
        const {
          leftValue,
          rightValue,
        } = this.getBarValues(record);

        return (
          <tr key={v4()}>
            <th>{title}</th>
            <td className="percent">{this.getLabelText(record)}</td>
            <td className="graph-l">
              <div className="progress">
                {this.renderBar(leftValue)}
              </div>
            </td>
            <td className="graph-r">
              <div className="progress">
                {this.renderBar(rightValue)}
              </div>
            </td>
          </tr>
        );
      })
    );
  }

  render() {
    return (
      <table className="widget-graph-02">
        <tbody>
          {this.renderRecords()}
        </tbody>
      </table>
    );
  }
}
