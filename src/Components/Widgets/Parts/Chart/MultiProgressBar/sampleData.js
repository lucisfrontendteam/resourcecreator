export default [
  {
    title: '평균 통화시간',
    unit: 'datetime',
    // 전체 통화시간(단위 초)
    totalTelTimes: 1800,
    // 전체 통화건수(단위 건)
    totalTels: 780,
    // 카테고리의 통화시간(단위 초)
    categoryTelTimes: 1000,
    // 카테고리의 통화건수(단위 건)
    categoryTels: 500,
  },
  {
    title: '평균 묵음비율',
    unit: 'ratio',
    // 전체 묵음시간(단위 초)
    totalTelTimes: 200,
    // 전체 통화건수(단위 건)
    totalTels: 780,
    // 가테고리의 묵음시간(단위 초)
    categoryTelTimes: 30,
    // 카테고리의 통화건수(단위 건)
    categoryTels: 500,
  },
  {
    title: '상담사 발화 비율',
    unit: 'ratio',
    // 전체 묵음시간(단위 초)
    totalTelTimes: 800,
    // 전체 통화건수(단위 건)
    totalTels: 780,
    // 카테고리의 묵음시간(단위 초)
    categoryTelTimes: 500,
    // 카테고리의 통화건수(단위 건)
    categoryTels: 500,
  },
  {
    title: '고객 발화 비율',
    unit: 'ratio',
    // 전체 고객 발화시간(단위 초)
    totalTelTimes: 400,
    // 전체 통화건수(단위 건)
    totalTels: 780,
    // 카테고리의 고객 발화시간(단위 초)
    categoryTelTimes: 200,
    // 카테고리의 통화건수(단위 건)
    categoryTels: 500,
  },
  {
    title: '말겹침 비율',
    unit: 'ratio',
    // 전체 말 겹침시간(단위 초)
    totalTelTimes: 100,
    // 전체 통화건수(단위 건)
    totalTels: 780,
    // 카테고리의 말 겹침시간(단위 초)
    categoryTelTimes: 50,
    // 카테고리의 통화건수(단위 건)
    categoryTels: 500,
  },
];
