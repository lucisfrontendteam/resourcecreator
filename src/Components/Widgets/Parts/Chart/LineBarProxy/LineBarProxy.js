import React, { PureComponent } from 'react';
import NVD3Chart from '../../Core/NVD3Chart';
import './custom.css';
import { v4 } from 'uuid';
import $ from 'jquery';
import _ from 'lodash';
import TextRenderer from '../../../Utils/TextRenderer';
import defaultOptions from './defaultOptions';
import sampleData from './sampleData';

export default class LineBarProxy extends PureComponent {
  static defaultProps = {
    name: '라인 바 프록시',
    datum: sampleData,
    options: defaultOptions,
    renderChart() {
      const { props } = this;
      const { options, id } = props;
      const lineBarProxyOptions = {
        ...defaultOptions,
        ...options,
      };

      window.nv.addGraph(() => {
        this.chart = window.nv.models.linePlusBarChart()
          .margin(lineBarProxyOptions.margin)
          .legendLeftAxisHint('(왼쪽 축)')
          .legendRightAxisHint('(오른쪽 축)')
          .color(lineBarProxyOptions.colorRange);

        // http://www.d3noob.org/2013/01/smoothing-out-lines-in-d3js.html
        this.chart.interpolate('basis');
        this.chart.legend.align(false);

        this.chart.xAxis.tickFormat(value => TextRenderer.renderDateTime(value))
          .showMaxMin(lineBarProxyOptions.xAxis.showMaxMin);
        this.chart.x2Axis.tickFormat(value => TextRenderer.renderDateTime(value))
          .showMaxMin(lineBarProxyOptions.xAxis.showMaxMin);

        this.chart.y1Axis.tickFormat(value => TextRenderer.renderNumber(value));
        this.chart.y2Axis.tickFormat(value => TextRenderer.renderNumber(value));

        this.chart.bars.forceY([0]).padData(false);

        this.chart.focusMargin({ top: 5 });

        this.chart.showLegend(true);

        this.chart.focusEnable(lineBarProxyOptions.focusEnable);

        this.chart.height(lineBarProxyOptions.chartHeight);

        this.chart.noData('데이터가 없습니다.');

        window.d3.select(document.getElementById(id))
          .datum(props.datum)
          .call(this.chart);
      });
    },
    resizeChart() {
      const { id } = this;
      $(window).resize(() => {
        const { update } = this.chart;
        if (_.isFunction(update)) {
          const $svg = $(document.getElementById(id));
          const width = $svg.parent().width();
          $svg.width(width);
          update();
        }
      });
    },
  };

  constructor(props) {
    super(props);
    this.id = v4();
  }

  render() {
    const { props, id } = this;
    return (<NVD3Chart {...props} id={id} />);
  }
}
