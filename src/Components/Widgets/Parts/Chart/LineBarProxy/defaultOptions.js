import TextRenderer from '../../../Utils/TextRenderer';

export default {
  margin: {
    top: 20,
    right: 60,
    bottom: 20,
    left: 60,
  },
  showLegend: false,
  timeFormat: '%Y.%m.%d',
  colorRange: TextRenderer.renderWidget009ColorRange(),
  xAxis: {
    showMaxMin: false,
  },
  focusEnable: true,
};
