import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import '../../../../../Resources/js/skycons';

export class Weather extends PureComponent {
  static defaultProps = {
    id: v4(),
    lineColor: '#0c99c9',
    width: 64,
    height: 64,
    degree: 1,
  };

  getDegree(degree) {
    let targetDegree = degree;
    const {
      CLEAR_DAY,
      PARTLY_CLOUDY_DAY,
      CLOUDY,
      RAIN,
      SLEET,
    } = window.Skycons;
    const icons = [
      CLEAR_DAY,
      PARTLY_CLOUDY_DAY,
      CLOUDY,
      RAIN,
      SLEET,
    ];
    const min = 0;
    const max = icons.length - 1;
    if (degree < min) {
      targetDegree = min;
    }
    if (degree > max) {
      targetDegree = max;
    }
    return icons[targetDegree];
  }

  renderChart() {
    const { props } = this;
    const skycons = new window.Skycons({ color: props.lineColor });
    skycons.add(props.id, this.getDegree(props.degree));
    skycons.play();
  }

  clearChart() {
    const { props } = this;
    const canvas = document.getElementById(props.id);
    const ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  componentWillUnmount() {
    this.clearChart();
  }

  componentDidMount() {
    this.renderChart();
  }

  componentDidUpdate() {
    this.clearChart();
    this.renderChart();
  }

  render() {
    const { props } = this;
    const { id, width, height } = props;

    return (<canvas id={id} width={width} height={height} />);
  }
}
