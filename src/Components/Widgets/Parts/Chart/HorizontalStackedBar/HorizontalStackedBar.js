import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import TextRenderer from '../../../Utils/TextRenderer';
import $ from 'jquery';
import 'bootstrap';
import sampleData from './sampleData';

export class HorizontalStackedBar extends PureComponent {
  static defaultProps = {
    name: '가로 막대 차트',
    records: sampleData,
  };

  componentDidMount() {
    $('[data-toggle="tooltip"]').tooltip();
  }

  renderSector(sector, index) {
    const className = `progress-bar progress-color${index + 1}`;
    const { name, value } = sector;
    const percentage = TextRenderer.renderPercentage(value);
    const style = { width: percentage };
    return (
      <div
        key={v4()}
        className={className}
        style={style}
        data-toggle="tooltip"
        data-placement="top"
        title={name}
      >
        {percentage}
      </div>
    );
  }

  renderBar(record) {
    const { title, sectors } = record;
    return (
      <tr key={v4()}>
        <th>{title}</th>
        <td>
          <div className="progress">
            {sectors.map((sector, index) => this.renderSector(sector, index))}
          </div>
        </td>
      </tr>
    );
  }

  render() {
    const { props } = this;
    return (
      <table className="widget-graph-01">
        <tbody>
          {
            props.records.map(record => this.renderBar(record))
          }
        </tbody>
      </table>
    );
  }
}
