export default [
  {
    title: '인바운드',
    sectors: [
      {
        id: '카테고리 식별자',
        name: '유동성 예금',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '수신 이유',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '신용대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '아파트담보대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '적립식예금',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '환전',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '지점연결',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '기타',
        value: 0.1,
      },
    ],
  },
  {
    title: '금융자산',
    sectors: [
      {
        id: '카테고리 식별자',
        name: '유동성 예금',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '수신 이유',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '신용대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '아파트담보대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '적립식예금',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '환전',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '지점연결',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '기타',
        value: 0.1,
      },
    ],
  },
  {
    title: '무인점포',
    sectors: [
      {
        id: '카테고리 식별자',
        name: '유동성 예금',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '수신 이유',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '신용대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '아파트담보대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '적립식예금',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '환전',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '지점연결',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '기타',
        value: 0.1,
      },
    ],
  },
  {
    title: '스마트금융',
    sectors: [
      {
        id: '카테고리 식별자',
        name: '유동성 예금',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '수신 이유',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '신용대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '아파트담보대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '적립식예금',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '환전',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '지점연결',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '기타',
        value: 0.1,
      },
    ],
  },
  {
    title: '여신연기',
    sectors: [
      {
        id: '카테고리 식별자',
        name: '유동성 예금',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '수신 이유',
        value: 0.2,
      },
      {
        id: '카테고리 식별자',
        name: '신용대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '아파트담보대출',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '적립식예금',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '환전',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '지점연결',
        value: 0.1,
      },
      {
        id: '카테고리 식별자',
        name: '기타',
        value: 0.1,
      },
    ],
  },
];
