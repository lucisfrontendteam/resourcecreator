export default {
  maintainAspectRatio: false,
  elements: {
    rectangle: {
      borderWidth: 2,
      borderSkipped: 'bottom',
    },
  },
  responsive: true,
  legend: {
    position: 'top',
  },
  title: {
    display: false,
  },
  scales: {
    xAxes: [{
      display: true,
      scaleLabel: {
        show: true,
      },
      ticks: {
        fontColor: '#0c99c9',
      },
    }],
    yAxes: [{
      display: true,
      scaleLabel: {
        show: true,
      },
      ticks: {
        fontColor: '#0c99c9',
      },
    }],
  },
};
