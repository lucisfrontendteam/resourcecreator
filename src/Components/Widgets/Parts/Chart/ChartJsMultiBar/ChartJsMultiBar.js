import React, { PureComponent } from 'react';
import ChartJs from '../../Core/Chartjs';
import sampleData from './sampleData';
import sampleOptions from './sampleOptions';

export class ChartJsMultiBar extends PureComponent {
  static defaultProps = {
    name: '멀티 바 차트',
    type: 'bar',
    data: sampleData,
    options: sampleOptions,
  };

  render() {
    const { props } = this;
    return (<ChartJs {...props} />);
  }
}
