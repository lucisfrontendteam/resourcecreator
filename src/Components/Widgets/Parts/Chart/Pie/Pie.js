import React, { PureComponent } from 'react';
import D3pieChart from '../../Core/D3pieChart';
import sampleData from './sampleData';

export default class Pie extends PureComponent {
  static defaultProps = {
    name: '파이차트',
    data: sampleData,
    options: {
      size: {
        canvasHeight: 400,
        canvasWidth: 400,
        pieOuterRadius: '100%',
      },
      labels: {
        outer: {
          format: 'none',
          pieDistance: 32,
        },
        inner: {
          format: 'label-percentage2',
          hideWhenLessThanPercentage: 10,
        },
        mainLabel: {
          color: '#ffffff',
          font: 'verdana',
          fontSize: 14,
        },
        percentage: {
          color: '#ffffff',
          font: 'verdana',
          fontSize: 14,
          decimalPlaces: 1,
        },
        value: {
          color: '#ffffff',
          font: 'verdana',
          fontSize: 14,
        },
        truncation: {
          enabled: true,
          truncateLength: 8,
        },
      },
      tooltips: {
        enabled: true,
        type: 'placeholder',
        string: '{label}: {percentage}%',
        styles: {
          fontSize: 14,
        },
      },
      effects: {
        load: {
          speed: 500,
        },
        pullOutSegmentOnClick: {
          effect: 'linear',
          speed: 200,
          size: 8,
        },
      },
      callbacks: {},
    },
  };

  render() {
    const { props } = this;
    return (
      <D3pieChart {...props} />
    );
  }
}
