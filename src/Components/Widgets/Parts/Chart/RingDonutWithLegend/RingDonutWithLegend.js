import React, { PureComponent } from 'react';
import 'react-nvd3';
import { v4 } from 'uuid';
import _ from 'lodash';
import D3pieChart from '../../Core/D3pieChart';
import './custom.css';
import sampleData from './sampleData';
import TextRenderer from '../../../Utils/TextRenderer';

export default class RingDonutWithLegend extends PureComponent {
  static defaultProps = {
    data: sampleData,
    id: v4(),
  };

  get pieSliceIndex() {
    const { props } = this;
    const { CategoryFilterListState = { id: '' } } = props.ExtraQueryState;
    const { data } = props;
    let index = -1;
    _.each(data.content, (record, recordIndex) => {
      if (record.id === CategoryFilterListState.id) {
        index = recordIndex;
      }
    });
    return index;
  }

  get chart() {
    const { props } = this;
    const chart = window[props.id];
    return chart;
  }

  handleOnload() {
    this.chart.openSegment(this.pieSliceIndex);
  }

  shouldComponentUpdate(nextProps) {
    const {
      data: nextData,
      ExtraQueryState: nextExtraQueryState,
    } = nextProps;
    const {
      data: currentData,
      ExtraQueryState: currentExtraQueryState,
    } = this.props;
    const dataChanged = !!!_.isEqual(nextData, currentData);
    const extraQueryChanged = !!!_.isEqual(nextExtraQueryState, currentExtraQueryState);
    return dataChanged || extraQueryChanged;
  }

  render() {
    const { props } = this;
    const {
      options,
      data,
      height,
      width,
      handlePiePieceOnClick,
    } = props;

    let newOption = options;
    if (height && width) {
      newOption = {
        ...options,
        labels: {
          outer: {
            // format: 'label-percentage2',
            // pieDistance: 20,
            format: 'none',
          },
          inner: {
            hideWhenLessThanPercentage: 10,
          },
          mainLabel: {
            fontSize: 12,
          },
          percentage: {
            color: '#ffffff',
            fontSize: 12,
            decimalPlaces: 1,
          },
          value: {
            color: '#ffffff',
            fontSize: 12,
          },
          // lines: {
          //   enabled: true,
          //   style: 'straight',
          //   color: '#777777',
          // },
          truncation: {
            enabled: true,
          },
        },
        tooltips: {
          enabled: true,
          type: 'placeholder',
          string: '{label}',
          styles: {
            backgroundOpacity: 0.70,
            borderRadius: 0.5,
            fontSize: 12,
          },
        },
        effects: {
          load: {
            effect: 'none',
          },
          // pullOutSegmentOnClick: {
          //   effect: 'none',
          // },
        },
        misc: {
          colors: {
            segmentStroke: '#000000',
          },
        },
        header: {
          title: {
            text: '주요 상담현황',
            color: '#999999',
            fontSize: 15,
          },
          subtitle: {
            text: TextRenderer.renderNumberWithGun(data.total),
            fontSize: 25,
          },
          location: 'pie-center',
          titleSubtitlePadding: 12,
        },
        size: {
          canvasHeight: height,
          canvasWidth: width,
          pieInnerRadius: '45%',
          pieOuterRadius: '110%',
        },
        callbacks: {
          onClickSegment(e) {
            handlePiePieceOnClick(
              e,
              props.toggleLegends,
              props.activeAllLegends,
            );
          },
          onload: () => this.handleOnload(),
        },
      };
    }

    return (
      <D3pieChart {...props} options={newOption} />
    );
  }
}
