export default {
  sortOrder: 'value-desc',
  total: 70000,
  content: [
    {
      label: '유동성 예금',
      value: 8,
      color: '#99C3DE',
    },
    {
      label: '수신이유',
      value: 5,
      color: '#1586BD',
    },
    {
      label: '신용대출',
      value: 2,
      color: '#1A6780',
    },
    {
      label: '아파트담보대출',
      value: 3,
      color: '#1F4153',
    },
    {
      label: '적립식예금',
      value: 2,
      color: '#123544',
    },
    {
      label: '환전',
      value: 1,
      color: '#D1D1D1',
    },
    {
      label: '지점연결',
      value: 3,
      color: '#A4A4A5',
    },
    {
      label: '안보일것',
      value: 1,
      color: '#797979',
    },
  ],
};
