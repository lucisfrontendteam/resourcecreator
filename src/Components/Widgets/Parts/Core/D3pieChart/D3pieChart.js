import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import 'react-nvd3';
import d3pie from 'd3pie';
import WidgetDefaultProps from '../../../Utils/WidgetDefaultProps';

export default class D3pieChart extends PureComponent {
  static defaultProps = {
    name: 'd3pie',
    id: v4(),
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = WidgetDefaultProps.widgetPartsSCU.bind(this);
  }

  render() {
    const { props } = this;
    return (
      <div id={props.id} />
    );
  }

  get mergedOptions() {
    const { props } = this;
    const { options, data } = props;
    const mergedOptions = { ...options, data };
    return mergedOptions;
  }

  renderChart() {
    const { props } = this;
    const { id } = props;
    // eslint-disable-next-line
    this.chart = new d3pie(document.getElementById(id), this.mergedOptions);
    window[id] = this.chart;
  }

  destroyChart() {
    const { props, chart } = this;
    if (chart) {
      chart.destroy();
    }
    window[props.id] = undefined;
  }

  componentDidMount() {
    this.renderChart();
  }

  componentDidUpdate() {
    this.destroyChart();
    this.renderChart();
  }

  componentWillUnmount() {
    this.destroyChart();
  }
}
