import React, { PureComponent } from 'react';
import $ from 'jquery';
import 'react-nvd3';
import './nv.d3.min.css';
import WidgetDefaultProps from '../../../Utils/WidgetDefaultProps';

export default class NVD3Chart extends PureComponent {
  constructor(props) {
    super(props);

    this.renderChart = props.renderChart.bind(this);
    this.resizeChart = props.resizeChart.bind(this);
    this.shouldComponentUpdate = WidgetDefaultProps.widgetPartsSCU.bind(this);
  }

  render() {
    const { props } = this;
    const svgStyle = {
      height: props.options.svgHeigth,
    };
    return (
      <div className="nv-chart">
        <svg id={props.id} style={svgStyle} />
      </div>
    );
  }

  removeChart() {
    const { props } = this;
    window.d3.selectAll($(document.getElementById(props.id)).find('g')).remove();
  }

  componentDidMount() {
    this.renderChart();
    this.resizeChart();
  }

  componentDidUpdate() {
    this.removeChart();
    this.renderChart();
    this.resizeChart();
  }

  componentWillUnmount() {
    this.removeChart();
  }
}
