import React, { PureComponent } from 'react';
import $ from 'jquery';
import 'jquery-knob';

export class Knob extends PureComponent {
  static defaultProps = {
    name: 'jQuery Knob(샘플)',
  };

  componentDidMount() {
    $('.knob').knob();
  }

  render() {
    return (
      <input
        type="text" className="knob" defaultValue="30"
        data-width="90" data-height="90" data-fgColor="#3c8dbc"
        data-readonly="true"
      />
    );
  }
}
