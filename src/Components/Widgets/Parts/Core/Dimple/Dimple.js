import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import dimple from 'dimple-js/dist/dimple.latest.js';

export default class Dimple extends PureComponent {
  constructor(props) {
    super(props);
    this.id = v4();
  }

  componentDidMount() {
    const { props } = this;
    const svg = dimple.newSvg('#chartContainer', 590, 400);
    // eslint-disable-next-line
    this.chart = new dimple.chart(svg, props.data);
    this.chart.setBounds(60, 30, 510, 330);
    this.chart.addCategoryAxis('x', 'Year');
    this.chart.addMeasureAxis('y', 'Tuition');
    this.chart.addSeries('College', dimple.plot.bar);
    this.chart.addLegend(65, 10, 510, 20, 'right');
    this.chart.draw();
  }

  render() {
    return (
      <div id="chartContainer" />
    );
  }
}
