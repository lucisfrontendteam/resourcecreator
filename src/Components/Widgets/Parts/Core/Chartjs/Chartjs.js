import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import Chart from 'chart.js';
import _ from 'lodash';

export class Chartjs extends PureComponent {
  static defaultProps = {
    height: 145,
  };

  constructor(props) {
    super(props);
    this.id = v4();
  }

  shouldComponentUpdate(nextProps) {
    const {
      data: nextData,
      ExtraQueryState: nextExtraQueryState,
    } = nextProps;
    const {
      data: currentData,
      ExtraQueryState: currentExtraQueryState,
    } = this.props;

    const nextDataTrim = _.map(nextData.datasets, set => set.data);
    const currentDataTrim = _.map(currentData.datasets, set => set.data);
    const dataChanged = !!!_.isEqual(nextDataTrim, currentDataTrim);
    const extraQueryChanged = !!!_.isEqual(nextExtraQueryState, currentExtraQueryState);
    const reload = dataChanged || extraQueryChanged;
    return reload;
  }

  get chartId() {
    const { props } = this;
    return props.id || this.id;
  }

  renderChart() {
    const { props } = this;
    const ctx = document.getElementById(this.chartId).getContext('2d');
    this.chart = new Chart(ctx, {
      type: props.type,
      data: props.data,
      options: props.options,
    });
  }

  destroyChart() {
    const { chart } = this;
    if (chart) {
      chart.destroy();
    }
  }

  componentDidMount() {
    this.renderChart();
  }

  componentDidUpdate() {
    this.destroyChart();
    this.renderChart();
  }

  componentWillUnmount() {
    this.destroyChart();
  }

  render() {
    const { props } = this;
    return (
      <canvas id={this.chartId} height={props.height} />
    );
  }
}
