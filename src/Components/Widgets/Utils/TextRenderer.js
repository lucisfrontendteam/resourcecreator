import _ from 'lodash';
import moment from 'moment';
import 'moment-duration-format';
import numeral from 'numeral';
import 'jquery-sparkline';
import EnvironmentManager from '../../../Utils/EnvironmentManager';
import icoUp from '../../../Resources/images/ico_up.png';
import icoDown from '../../../Resources/images/ico_down.png';
import TreeTraveser from '../../../Utils/TreeTraveser';
import * as colorSchemes from 'd3-scale-chromatic';

export default class TextRenderer {
  static renderSparkline($target, values = []) {
    $target.sparkline(values, {
      type: 'line',
      height: '22px',
      lineColor: '#0c99c9',
      fillColor: undefined,
      lineWidth: 0.2,
      spotColor: undefined,
      minSpotColor: undefined,
      maxSpotColor: undefined,
      highlightSpotColor: undefined,
      highlightLineColor: undefined,
      spotRadius: 0,
    });
  }

  static renderText(value = '') {
    return value;
  }

  static renderDateTimeString(
    dateString = '', formatFrom = 'YYYYMMDDhmmss', formatTo = 'YYYY/MM/DD HH:mm:ss'
  ) {
    const value = moment(dateString, formatFrom).format(formatTo);
    return value;
  }

  static renderDateTime(date = new Date(), formatString = 'YYYY.MM.DD') {
    return moment(date).format(formatString);
  }

  static renderDateObject(dateString = '20160822', formatString = 'YYYYMMDD') {
    return moment(dateString, formatString).toDate();
  }

  static renderPercentage(value = 0, formatString = '0.0%') {
    let targetFormatString = formatString;
    const invalidFormatString = formatString.indexOf('%') < 0;
    if (invalidFormatString) {
      targetFormatString = '0.0%';
    }
    const stringValue = numeral(value).format(targetFormatString);
    return stringValue;
  }

  static renderPercentageNumber(value = 0, total = 0) {
    const percentage = (value / total) * 100;
    return parseFloat(percentage.toFixed(2));
  }

  static renderAvgPercentage(value = 0, total = 0) {
    let avg = value / total;
    if (_.isNaN(avg)) {
      avg = 0;
    }
    return TextRenderer.renderPercentage(avg);
  }

  static renderDuration(seconds = 0, formatString = 'HH:mm:ss') {
    return moment.duration(parseFloat(seconds), 'seconds').format(formatString);
  }

  static renderDateDuration(seconds = 0) {
    const formatString = 'h:mm:ss';
    return TextRenderer.renderDuration(seconds, formatString);
  }

  static renderLocaleDateDuration(seconds = 0) {
    const formatString = 'd일 h시간 m분 s초';
    return TextRenderer.renderDuration(seconds, formatString);
  }

  static renderLocaleMimuteDuration(seconds = 0) {
    const formatString = 'm분';
    return TextRenderer.renderDuration(seconds, formatString);
  }

  static renderNumberWithGun(value = 0) {
    const formattedValue = TextRenderer.renderNumber(value);
    return `${formattedValue} 건`;
  }

  static renderNumberWithAge(value = 0) {
    const formattedValue = TextRenderer.renderNumber(value);
    return `${formattedValue} 세`;
  }

  static renderNumberWithMonth(value = 0) {
    const formattedValue = TextRenderer.renderNumber(value);
    return `${formattedValue} 개월`;
  }

  static renderNumber(value = 0) {
    const formattedValue = numeral(value).format('0,0');
    return formattedValue;
  }

  static renderZeroPrefixNumber(value = 0) {
    return `0${value}`;
  }

  static renderCompareWithYesterday(value = 0) {
    const base64Prefix = EnvironmentManager.getBase64Prefix();
    const up = `<img src=${base64Prefix + icoUp} alt="감소" />`;
    const down = `<img src=${base64Prefix + icoDown} alt="증가" />`;
    const targetIcon = value >= 0 ? up : down;
    return `${value} calls(${targetIcon})`;
  }

  static renderRandomHexColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    _.forEach(_.range(6), () => {
      color += letters[Math.floor(Math.random() * 16)];
    });
    return color;
  }

  static renderHexColor(index = 0) {
    const defaultColors = [
      '#a8cee5',
      '#0c99c9',
      '#1c7a93',
      '#285366',
      '#144556',
      '#dadada',
      '#b3b3ba',
      '#8c8c8c',
      '#606060',
      '#00bca5',
      '#00967d',
      '#007760',
      '#005b4a',
      '#003329',
      '#000000',
    ];
    const targetColor = defaultColors[index] || TextRenderer.renderRandomHexColor();
    return targetColor;
  }

  static renderRgbColor(index = 0) {
    const fiexedIndex = 1 - (index * 0.11);
    let color = '';
    if (fiexedIndex < 0) {
      color = TextRenderer.renderRandomHexColor();
    } else {
      color = colorSchemes.interpolateYlGnBu(fiexedIndex);
    }
    return color;
  }

  static renderLineHexColor(index = 0) {
    const defaultColors = [
      '#686868',
      '#d13939',
      '#750808',
      '#3b0000',
      '#ff8a0f',
      '#e87802',
      '#ff440f',
      '#ffda75',
      '#ffbb02',
      '#c60e0e',
    ];
    const targetColor = defaultColors[index] || TextRenderer.renderRandomHexColor();
    return targetColor;
  }

  static renderLineHexColorArray() {
    const colors
      = _.map(_.range(9), index => TextRenderer.renderLineHexColor(index));
    return colors;
  }

  static renderWidget009ColorRange() {
    return [
      '#b8deed',
      ...TextRenderer.renderLineHexColorArray(),
    ];
  }

  static renderQueryState(
    QueryStateManagerState,
    AdvencedFilterArgManagerState,
    totalCount
  ) {
    let message = '요약: 카테고리 그룹 / [조회 시작일] ~ [조회 종료일] / (총 n건)';
    if (QueryStateManagerState) {
      const {
        // 선택된 날짜 / 기간 타입
        targetDateRangeState,
        // 최근일
        // recentDaysFilterState,
        // 조회 시작일
        dateRangeFromFilterState,
        // 조회 종료일
        dateRangeToFilterState,
        // 카테고리 필터 리스트
        // categoryFilterListState,
        // 카테고리 그룹 필터 리스트
        categoryGroupFilterListState,
      } = QueryStateManagerState;

      const { categoryFilterTree } = AdvencedFilterArgManagerState;

      let daterange = '';
      switch (targetDateRangeState) {
        // case 'RECENT': {
        //   daterange = `${recentDaysFilterState + 1} 일분`;
        // } break;
        case 'RECENT':
        case 'RANGE': {
          const fromValue
            = TextRenderer.renderDateTime(dateRangeFromFilterState);
          const toValue
            = TextRenderer.renderDateTime(dateRangeToFilterState);
          daterange = `${fromValue} ~ ${toValue}`;
        } break;
        default:
          daterange = '전체기간';
          break;
      }

      const categorieGroups = _.map(categoryGroupFilterListState, (state, index) => {
        let token = ', ';
        if (categoryGroupFilterListState.length === index + 1) {
          token = '';
        }
        const categoryGroup =
          _.find(categoryFilterTree, group => _.toString(group.id) === _.toString(state.id));
        return `${categoryGroup.name}${token}`;
      }).join(' ,');

      const totalCountString = `(총 ${TextRenderer.renderNumberWithGun(totalCount)})`;

      if (categorieGroups !== '') {
        message = `요약: ${categorieGroups} / ${daterange} / ${totalCountString}`;
      } else {
        message = `요약: ${daterange} / ${totalCountString}`;
      }
    }
    return message;
  }

  static renderQueryStates(
    QueryStateManagerState,
    AdvencedFilterArgManagerState,
    toShowTotal = true
  ) {
    const messages = [];
    if (QueryStateManagerState) {
      const {
        // 선택된 날짜 / 기간 타입
        targetDateRangeState,
        // 최근일
        // recentDaysFilterState,
        // 조회 시작일
        dateRangeFromFilterState,
        // 조회 종료일
        dateRangeToFilterState,
        // 카테고리 필터 리스트
        categoryFilterListState,
        // 카테고리 그룹 필터 리스트
        categoryGroupFilterListState,
      } = QueryStateManagerState;

      const categorieGroups = _.map(categoryGroupFilterListState, state => {
        const { categoryFilterTree } = AdvencedFilterArgManagerState;
        const categoryGroup =
          _.find(categoryFilterTree, group => _.toString(group.id) === _.toString(state.id));
        return categoryGroup ? categoryGroup.name : '';
      }).join(', ');
      const categoryGroupInfo = `카테고리 그룹: ${categorieGroups || '지정되지 않음'}`;
      messages.push(categoryGroupInfo);

      const categories = _.map(categoryFilterListState, state => {
        const { categoryFilterTree } = AdvencedFilterArgManagerState;
        const treeTraveser = new TreeTraveser(categoryFilterTree);
        const categoryModels = treeTraveser.getChildNodesModel();
        const categoryInfo =
          _.find(categoryModels, category => _.toString(category.id) === _.toString(state.id));
        return categoryInfo ? categoryInfo.name : '';
      }).join(', ');
      const categoryInfo = `카테고리: ${categories || '지정되지 않음'}`;
      messages.push(categoryInfo);

      let daterange = '';
      switch (targetDateRangeState) {
        // case 'RECENT': {
        //   daterange = `${recentDaysFilterState + 1} 일분`;
        // } break;
        case 'RECENT':
        case 'RANGE': {
          const fromValue
            = TextRenderer.renderDateTime(dateRangeFromFilterState);
          const toValue
            = TextRenderer.renderDateTime(dateRangeToFilterState);
          daterange = `${fromValue} ~ ${toValue}`;
        } break;
        default:
          daterange = '전체기간';
          break;
      }
      const dateRangeInfo = `조회기간: ${daterange}`;
      messages.push(dateRangeInfo);

      if (toShowTotal) {
        const { filteredCount } = QueryStateManagerState;
        const totalCountString = `총 조회건수: ${TextRenderer.renderNumberWithGun(filteredCount)}`;
        messages.push(totalCountString);
      }
    }
    return messages;
  }

  static renderTotalPages(totalCount, recordCount) {
    const totalPageNumber = Math.ceil(totalCount / recordCount);
    return totalPageNumber;
  }
}
