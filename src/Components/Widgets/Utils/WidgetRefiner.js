import _ from 'lodash';

export default class WidgetRefiner {
  constructor(widgetResources) {
    this.widgetResources = widgetResources;
  }

  get widgetCores() {
    const resources = _.map(this.widgetResources, (element, key) => {
      const isNotReducer = key.indexOf('Reducer') < 0;
      const isNotActions = key.indexOf('Actions') < 0;
      const isNotManifest = key.indexOf('Manifest') < 0;
      const isNotSampleDatas = key.indexOf('SampleDatas') < 0;
      const isNotActionTypes = key.indexOf('ActionTypes') < 0;

      let result = undefined;
      if (isNotReducer && isNotActions && isNotManifest && isNotSampleDatas && isNotActionTypes) {
        result = {
          element,
          displayName: key,
        };
      }
      return result;
    });
    return _.remove(resources, undefined);
  }

  get widgetActionTypes() {
    const result = _.filter(this.widgetResources,
      (element, key) => key.indexOf('ActionTypes') >= 0);
    const actionTypes = _.map(result, el => {
      const [firstKey] = Object.keys(el);
      return firstKey;
    });
    return actionTypes;
  }

  get widgetManifest() {
    const result = _.filter(this.widgetResources,
      (element, key) => key.indexOf('Manifest') >= 0);
    return result;
  }

  get widgetSampleDatas() {
    const result = {};
    _.forEach(this.widgetResources, (element, key) => {
      if (key.indexOf('SampleDatas') >= 0) {
        result[key] = element;
      }
    });
    return result;
  }

  get widgetReducers() {
    let result = {};
    _.forEach(this.widgetResources, (element, key) => {
      const isReducer = key.indexOf('Reducer') >= 0;
      if (isReducer) {
        result = {
          ...result,
          ...element,
        };
      }
    });
    return result;
  }

  get widgetActionList() {
    let result = {};
    _.forEach(this.widgetResources, (element, key) => {
      const isActions = key.indexOf('Actions') >= 0;
      if (isActions) {
        result = {
          ...result,
          ...element,
        };
      }
    });
    return result;
  }
}
