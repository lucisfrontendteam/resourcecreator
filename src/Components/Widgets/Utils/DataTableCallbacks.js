import $ from 'jquery';
import TextRenderer from './TextRenderer';

export default class DataTableCallbacks {
  static initSparklinesWhenDataTableComplate({ nTable }) {
    $(nTable).find('td.sparkline').each((index, td) => {
      const $target = $(td);
      const values = $target.text().split(',');
      TextRenderer.renderSparkline($target, values);
    });
  }
}
