import _ from 'lodash';
import { dataEmpty } from './WidgetConstants';

export default class WidgetDataComparator {
  static valid(adapter) {
    return _.isEmpty(adapter.errorType);
  }

  static inValid(adapter) {
    return !!!WidgetDataComparator.valid(adapter);
  }

  static dataEmpty(adapter) {
    return adapter.errorType === dataEmpty;
  }
}
