import _ from 'lodash';

export default class DataTableDataAdapter {
  static reorderedFields(fieldOrder, gridDatas) {
    const orderedDatas = _.map(gridDatas, record => {
      const orderedData = {};
      _.each(fieldOrder, key => {
        orderedData[key] = record[key];
      });
      return orderedData;
    });
    return orderedDatas;
  }

  constructor(records = [0, 0]) {
    this.records = records;
  }

  getTotal() {
    const total = _.sum(this.records);
    return total;
  }

  getLength() {
    return this.records.length;
  }

  getAverage() {
    return this.getTotal() / this.getLength();
  }

  getTodayValue() {
    const length = this.getLength();
    return this.records[length - 1];
  }

  getYeterdayValue() {
    const length = this.getLength();
    return this.records[length - 2];
  }

  getComparedValueWithYesterDay() {
    const comparedValueWithYesterDay = this.getTodayValue() - this.getYeterdayValue();
    return comparedValueWithYesterDay;
  }

  getComparedValueWithYesterDayRatio() {
    return this.getComparedValueWithYesterDay() / this.getTodayValue();
  }
}
