import _ from 'lodash';
import $ from 'jquery';
import WidgetRenderer from './WidgetRenderer';
import PropsCreator from '../../../Utils/PropsCreator';

export default class WidgetDefaultProps {
  static shouldComponentUpdate(nextProps) {
    const { props: currentProps } = this;
    const { id } = this.constructor.defaultProps;
    const stateName = `${id}State`;
    const stateChanged =
      !!!_.isEqual(nextProps[stateName], currentProps[stateName]);

    const {
      ExtraQueryState: nextExtraQueryState,
      QueryStateManagerState: nextQueryState,
      RefresherState: nextRefresherState,
    } = nextProps;
    const {
      ExtraQueryState: currentExtraQueryState,
      QueryStateManagerState: currentQueryState,
      RefresherState: currentRefresherState,
    } = this.props;

    const queryChanged = !!!_.isEqual(nextQueryState, currentQueryState);
    const extraQueryChanged = !!!_.isEqual(nextExtraQueryState, currentExtraQueryState);
    const refresherChanged = !!!_.isEqual(nextRefresherState, currentRefresherState);

    let reload = false;
    if (_.includes(stateName, 'Widget009')) {
      reload = (stateChanged || queryChanged || extraQueryChanged || refresherChanged);
    } else if (_.includes(stateName, 'DetailPlayer')) {
      reload = (stateChanged || queryChanged || extraQueryChanged);
    } else {
      reload = (stateChanged || queryChanged || refresherChanged);
    }
    // if (reload) {
    //   console.log(`-----widget ${id} scu-----`);
    //   // console.log('query diff', diff(nextQueryState, currentQueryState));
    //   // console.log('pageChanged', pageChanged);
    //   // console.log('queryChanged', queryChanged);
    //   console.log('stateChanged', stateChanged);
    //   // console.log('networkStatusChanged', networkStatusChanged);
    //   // console.log('extraQueryChanged', extraQueryChanged);
    //   console.log('-----widget scu-----');
    // }
    return reload;
  }

  static widgetPartsSCU(nextProps) {
    const {
      data: nextData,
      datas: nextDatas,
      ExtraQueryState: nextExtraQueryState,
    } = nextProps;
    const {
      data: currentData,
      datas: currentDatas,
      ExtraQueryState: currentExtraQueryState,
    } = this.props;
    const dataChanged = !!!_.isEqual(nextData, currentData);
    const datasChanged = !!!_.isEqual(nextDatas, currentDatas);
    const extraQueryChanged = !!!_.isEqual(nextExtraQueryState, currentExtraQueryState);
    const reload = dataChanged || extraQueryChanged || datasChanged;
    return reload;
  }

  static getPagingProps() {
    return {
      containerClassName: 'lucis-paginate',
      pageRangeDisplayed: 3,
      marginPagesDisplayed: 2,
      initialSelected: 0,
      previousLabel: '<',
      nextLabel: '>',
    };
  }

  static handleCellClick(e, pageNumber = 1, pageSize = 25) {
    e.preventDefault();
    e.stopPropagation();
    const { props } = this;
    const extraIds = $(e.target).data();
    const newProps = PropsCreator.getDetailPlayerProps(props, extraIds, pageNumber, pageSize);
    const { detailplayerInitAction } = props;
    if (detailplayerInitAction) {
      detailplayerInitAction(newProps);
    }
  }

  static firePagingAction(props, action, pageNumber = 1, pageSize = 25) {
    const newProps = PropsCreator.getPaginationProps(props, pageNumber, pageSize);
    // debugger;
    action(newProps);
  }

  static handlePagingClick(paging) {
    const currentPageNumber = paging.selected + 1;
    this.setState({ pageNumber: currentPageNumber });
    this.forceUpdate();
  }

  static render() {
    const { props } = this;
    const { id } = this.constructor.defaultProps;
    const stateName = `${id}State`;
    const TargetState = props[stateName];
    let widget = {};
    if (_.isEmpty(TargetState)) {
      widget = WidgetRenderer.renderLoadingWidgetCore();
    } else if (TargetState.err) {
      widget = WidgetRenderer.renderNetworkFailWidgetCore(TargetState);
    } else {
      widget = this.renderWidgetCore(TargetState);
    }
    return widget;
  }
}
