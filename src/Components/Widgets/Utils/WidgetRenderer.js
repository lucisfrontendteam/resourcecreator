import React from 'react';
import _ from 'lodash';
import { ErrorWidgetCore } from '../Cores/ErrorWidgetCore';
import { WidgetDummy } from '../Cores/WidgetDummy';

export default class WidgetRenderer {
  static renderWidget(widget, placeholderSize, showNow = true) {
    let targetWidget = {};
    let targetProps = {};
    if (_.isEmpty(widget)) {
      targetWidget = WidgetDummy;
    } else {
      const existanceValid = WidgetRenderer.validateWidgetExistence(widget);
      const sizeValid = WidgetRenderer.validateWidgetSize(widget, placeholderSize);
      if (existanceValid.widgetFound && sizeValid.placeable) {
        targetWidget = widget;
      } else {
        targetProps = {
          ...existanceValid.errorProps,
          showNow,
        };
        if (!!!sizeValid.placeable) {
          targetProps = {
            ...sizeValid.errorProps,
            TargetWidgetCore: ErrorWidgetCore,
            showNow,
          };
        }
        targetWidget = React.cloneElement(widget, targetProps);
      }
    }
    return targetWidget;
  }

  static validateWidgetSize(widget, placeholderSize) {
    const { props } = widget;
    const { defaultProps } = props.TargetWidgetCore;
    const placeable = _.includes(defaultProps.sizes, placeholderSize);
    let result = {};
    if (placeable) {
      result = { placeable };
    } else {
      const errorProps = {
        strong1: '에러',
        strong2: '가 발생했습니다.',
        message: [`${defaultProps.name} 위젯은 이 곳에 배치할 수 없습니다.`],
      };
      result = { placeable, errorProps };
    }
    return result;
  }

  static validateWidgetExistence(widget) {
    let result = {};
    const { widgetFound, widgetCoreId } = widget.props;
    if (widgetFound) {
      result = { widgetFound };
    } else {
      const errorProps = {
        strong1: '에러',
        strong2: '가 발생했습니다.',
        message: [` 위젯 ${widgetCoreId}을 UIContents.js 안에서 찾을 수 없습니다.`],
      };
      result = { widgetFound, errorProps };
    }
    return result;
  }

  static renderNetworkFailWidgetCore(widgetState, showNow = true) {
    const { status, statusText, message } = widgetState;
    const errorProps = {
      strong1: status,
      strong2: statusText,
      message: [message],
    };
    return (<ErrorWidgetCore {...errorProps} showNow={showNow} />);
  }

  static renderLoadingWidgetCore(showNow = true) {
    const errorProps = {
      strong1: '데이터 로딩중',
      strong2: '입니다.',
      message: ['잠시만 기다려주세요.'],
    };
    return (<ErrorWidgetCore {...errorProps} showNow={showNow} />);
  }

  static renderDataFailWidgetCore(adapter = {}, showNow = true) {
    const errorProps = {
      strong1: '데이터에 문제가 있습니다.',
      message: adapter.errors,
    };
    return (<ErrorWidgetCore {...errorProps} showNow={showNow} />);
  }

  static renderDataEmptyWidgetCore(showNow = true) {
    const errorProps = { strong1: '표시할 데이터가 없습니다.' };
    return (<ErrorWidgetCore {...errorProps} showNow={showNow} />);
  }
}
