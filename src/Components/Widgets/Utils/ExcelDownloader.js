export default class ExcelDownloader {
  constructor(props, manifest) {
    this.props = props;
    this.manifest = manifest;
  }

  get host() {
    let host = '';
    if (location.port === '3001') {
      host = 'http://localhost:8089';
    }
    return host;
  }

  get scenarioId() {
    const { props } = this;
    const { scenarioId } = props.QueryStateManagerState;
    return scenarioId;
  }

  get excelUrl() {
    const { scenarioId, manifest, host } = this;
    const excelUrl = `${host}${manifest.excelUrl}?scenarioId=${scenarioId}`;
    return excelUrl;
  }
}
