export default {
  id: 'Widget040',
  stateId: 'Widget040',
  name: '카테고리별 인입현황',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'pageNumber',
      id: 'Widget040pageNumber',
    },
    {
      key: 'pageSize',
      id: 'Widget040pageSize',
    },
  ],
  sizes: [12],
  recordCount: 25,
  method: 'post',
  apiUrl: '/category/widget040.api',
  excelUrl: '/category/widget040.excel',
};
