/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid": {
      "type": "object",
      "properties": {
        "totalCount": {
          "type": "integer"
        },
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "record": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "value": {
                      "type": "object",
                      "properties": {
                        "dateFrom": {
                          "type": "string"
                        },
                        "dateTo": {
                          "type": "string"
                        },
                        "name": {
                          "type": "string"
                        }
                      },
                      "required": [
                        "dateFrom",
                        "dateTo",
                        "name"
                      ]
                    }
                  },
                  "required": [
                    "value"
                  ]
                }
              }
            },
            "required": [
              "record"
            ]
          }
        }
      },
      "required": [
        "totalCount",
        "datas"
      ]
    }
  },
  "required": [
    "grid"
  ]
};
