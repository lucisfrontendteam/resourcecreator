import _ from 'lodash';
import moment from 'moment';
import 'moment/locale/ko';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import TextRenderer from '../../Utils/TextRenderer';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget040DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState, widgetOptions) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }

    this.totalCount = widgetState.grid.totalCount;
    this.datas = widgetState.grid.datas;
    this.options = widgetOptions.grid.options;
  }

  get dataArray() {
    const records = _.map(this.datas, data => data.record);
    return records;
  }

  getSummuryValue(values) {
    return _.reduce(values, (sum, n) => sum + n, 0);
  }

  get summuries() {
    const { pivotArray } = this;
    const [,,, ...numbers] = pivotArray;
    return _.map(numbers, record => this.getSummuryValue(record));
  }

  get pivotArray() {
    const pivotArray = [];
    _.forEach(this.dataArray, (data, recordIndex) => {
      _.forEach(data, (point, pointIndex) => {
        if (!!!_.isArray(pivotArray[pointIndex])) {
          pivotArray[pointIndex] = [];
        }
        pivotArray[pointIndex][recordIndex] = point;
      });
    });
    return pivotArray;
  }

  get finalDatas() {
    const { summuries, dataArray } = this;
    const summurieRecord = ['계', '', ...summuries];
    return [summurieRecord, ...dataArray];
  }

  get yearTitle() {
    const year = moment().get('year');
    return `${year}년 계`;
  }

  get lastMonthTitle() {
    const month = moment().add(-1, 'month').get('month') + 1;
    return `전월(${month}월)`;
  }

  get monthTitle() {
    const month = moment().get('month') + 1;
    return `당월(${month}월)`;
  }

  getDateTitle(count) {
    const formattedDate =
      moment()
        .startOf('week')
        .add('days', count)
        .format('M-D (ddd)');
    return formattedDate;
  }

  get finalOptions() {
    const { options } = this;
    const [
      no,
      col1,
      col2,
      year,
      lastMonth,
      month,
      lastWeek,
      week,
      d4,
      d3,
      d2,
      d1,
      today,
    ] = options.columnDefs;

    const newOptions = {
      ...options,
      columnDefs: [
        no,
        col1,
        col2,
        {
          ...year,
          title: this.yearTitle,
        },
        {
          ...lastMonth,
          title: this.lastMonthTitle,
        },
        {
          ...month,
          title: this.monthTitle,
        },
        lastWeek,
        week,
        {
          ...d4,
          title: this.getDateTitle(1),
        },
        {
          ...d3,
          title: this.getDateTitle(2),
        },
        {
          ...d2,
          title: this.getDateTitle(3),
        },
        {
          ...d1,
          title: this.getDateTitle(4),
        },
        {
          ...today,
          title: this.getDateTitle(5),
        },
      ],
    };

    return newOptions;
  }

  get totalPageNumber() {
    // 그리드 데이터에서 취득해야 한다는 점에 주의
    const totalPageNumber = TextRenderer.renderTotalPages(this.totalCount, manifest.recordCount);
    return totalPageNumber;
  }

  get defaultData() {
    const { totalCount, dataArray, finalOptions, totalPageNumber } = this;
    return {
      grid: {
        totalCount,
        totalPageNumber,
        datas: dataArray,
        options: finalOptions,
      },
    };
  }
}
