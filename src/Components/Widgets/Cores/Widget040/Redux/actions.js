import * as actionTypes from './actionTypes';
import Widget040SampleDatas from '../Widget040SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget040InitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryStateManagerState, QueryIdMap } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET040_INIT,
      query: {
        ...QueryStateManagerState,
        ...QueryIdMap,
      },
      ajaxType: 'normal',
      sampleData: Widget040SampleDatas,
    };
    AjaxManager.run(param);
  };
}
