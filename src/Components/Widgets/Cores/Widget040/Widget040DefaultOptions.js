import $ from 'jquery';
import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      searching: false,
      paging: false,
      ordering: false,
      rowsGroup: [0],
      initComplete() {
        $(this).find('tbody tr[role="row"]:eq(0)')
               .find('td:eq(1)')
               .attr('colspan', 2);
        $(this).find('tbody tr[role="row"]:eq(0)')
               .find('td:eq(2)')
               .css('display', 'none');
      },
      headerCallback(thead) {
        if ($(this).find('thead tr').length === 1) {
          const $firstHeader = $(thead);
          const $secondHeader = $firstHeader.clone();
          $firstHeader.find('th:eq(0)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(1)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(2)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(3)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(4)').attr('colspan', 2).text('월간현황');
          $firstHeader.find('th:eq(5)').remove();

          $firstHeader.find('th:eq(5)').attr('colspan', 2).text('주간현황');
          $firstHeader.find('th:eq(6)').remove();

          $firstHeader.find('th').slice(6, 10).remove();
          $firstHeader.find('th:eq(6)').attr('colspan', 5).text('일간현황');

          $firstHeader.after($secondHeader);
        }
      },
      rowCallback(row, fields) {
        const headerLength = 3;
        $(row).find('td').slice(headerLength)
          .each((columnIndex, column) => {
            const [, col1Field, col2Field] = fields;
            const targetField = fields[columnIndex + headerLength];
            const datas = {
              ...col1Field,
              ...col2Field,
              ...targetField,
            };
            $(column).data(datas).attr('detailPlayer', true);
          });
      },
      columnDefs: [
        {
          title: 'No',
          targets: 0,
          width: '8%',
          render(field = {}) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderText(name);
          },
        },
        {
          title: '업무구분',
          targets: 1,
          width: '8%',
          render(field = {}) {
            const {
              TargetHoFilterState = {
                name: '',
              },
            } = field;
            const { name } = TargetHoFilterState;
            return TextRenderer.renderText(name);
          },
        },
        {
          title: '카테고리명',
          targets: 2,
          width: 'auto',
          render(field) {
            const {
              CategoryFilterListState = {
                name: '',
              },
            } = field;
            const { name } = CategoryFilterListState;
            return TextRenderer.renderText(name);
          },
        },
        {
          title: '2016년 계',
          targets: 3,
          width: '10%',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '전월(7월)',
          targets: 4,
          width: '7%',
          className: 'color-01 th-bg',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '당월(8월)',
          targets: 5,
          width: '7%',
          className: 'color-01',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '전주',
          targets: 6,
          width: '7%',
          className: 'color-02',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '금주',
          targets: 7,
          width: '7%',
          className: 'color-02',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '20160815',
          targets: 8,
          width: '7%',
          className: 'color-03',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '20160816',
          targets: 9,
          width: '7%',
          className: 'color-03',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '20160817',
          targets: 10,
          width: '7%',
          className: 'color-03',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '20160818',
          targets: 11,
          width: '7%',
          className: 'color-03',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
        {
          title: '20160819',
          targets: 12,
          width: '7%',
          className: 'color-03',
          render(field) {
            const {
              value = {
                name: '',
              },
            } = field;
            const { name } = value;
            return TextRenderer.renderNumberWithGun(name);
          },
        },
      ],
    },
  },
};
