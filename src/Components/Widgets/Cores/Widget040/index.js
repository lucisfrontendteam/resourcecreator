import * as actions from './Redux/actions';
import * as reducer from './Redux/reducer';
import * as actionTypes from './Redux/actionTypes';
export {
  actions as Widget040Actions,
  reducer as Widget040Reducer,
  actionTypes as Widget040ActionTypes,
};
import manifest from './manifest';
export { manifest as Widget040Manifest };
import Widget040SampleDatas from './Widget040SampleDatas';
export { Widget040SampleDatas };
export { Widget040 } from './Widget040';
