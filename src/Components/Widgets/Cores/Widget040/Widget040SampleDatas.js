/* eslint-disable */
/**
 * 위젯 식별자: WIDGET040
 * 위젯 이름: 카테고리별 인입 현황
 */
export default {
  grid: {
    totalCount: 1000,
    // 카테고리별 인입 현황 그리드의 데이터 배열
    /*
     업무구분',
     카테고리명,
     당해년도 총합,
     조회일 기준 전월 총합,
     조회일 기준 당월 총합,
     조회일 기준 이전 주 총합,
     조회일 기준 이번 주 총합,
     조회일 D -4 데이터,
     조회일 D -3 데이터,
     조회일 D -2 데이터,
     조회일 D -1 데이터,
     조회일 데이터,
     */
    datas: [
      // 컬럼의 총 합
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "계"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '1890'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '6606'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '3571'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '4221'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '4246'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '4360'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '3628'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '7290'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '3110'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '3595'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "1",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "2",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "3",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "4",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "5",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "6",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "7",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "8",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "9",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "10",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "11",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
      {
        record: [
          {
            value: {
              dateFrom: "",
              dateTo: "",
              name: "12",
            }
          },
          {
            TargetHoFilterState: {
              id: 71,
              name: "업무구분"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              dateFrom: "",
              dateTo: "",
              name: "",
            }
          },
          {
            value: {
              dateFrom: "20160101",
              dateTo: "20161231",
              name: '322'
            }
          },
          {
            value: {
              dateFrom: "20160901",
              dateTo: "20160930",
              name: '224'
            }
          },
          {
            value: {
              dateFrom: "20161001",
              dateTo: "20161031",
              name: '225'
            }
          },
          {
            value: {
              dateFrom: "20161016",
              dateTo: "20161022",
              name: '226'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161029",
              name: '227'
            }
          },
          {
            value: {
              dateFrom: "20161022",
              dateTo: "20161022",
              name: '228'
            }
          },
          {
            value: {
              dateFrom: "20161023",
              dateTo: "20161023",
              name: '229'
            }
          },
          {
            value: {
              dateFrom: "20161024",
              dateTo: "20161024",
              name: '120'
            }
          },
          {
            value: {
              dateFrom: "20161025",
              dateTo: "20161025",
              name: '121'
            }
          },
          {
            value: {
              dateFrom: "20161026",
              dateTo: "20161026",
              name: '122'
            }
          }
        ]
      },
    ],
  },
};
