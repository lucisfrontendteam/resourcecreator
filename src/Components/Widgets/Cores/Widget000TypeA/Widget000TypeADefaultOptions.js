import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      columnDefs: [
        {
          title: '원스톱 처리율',
          targets: 0,
          render: TextRenderer.renderPercentage,
          width: '33%',
        },
        {
          title: '평균 통화시간',
          targets: 1,
          render: TextRenderer.renderLocaleDateDuration,
          width: '33%',
        },
        {
          title: '묵음비율',
          targets: 2,
          render: TextRenderer.renderPercentage,
          width: '33%',
        },
      ],
    },
  },
};
