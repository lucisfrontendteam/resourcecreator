/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "label": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "value": {
                "type": "integer"
              }
            },
            "required": [
              "label",
              "id",
              "value"
            ]
          }
        }
      },
      "required": [
        "data"
      ]
    },
    "grid": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "teltimes": {
                "type": "integer"
              },
              "silence": {
                "type": "number"
              }
            },
            "required": [
              "teltimes",
              "silence"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    }
  },
  "required": [
    "chart",
    "grid"
  ]
};
