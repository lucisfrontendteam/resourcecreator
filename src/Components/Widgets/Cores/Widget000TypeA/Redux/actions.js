import * as actionTypes from './actionTypes';
import Widget000TypeASampleDatas from '../Widget000TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget000typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { QueryStateManagerState: queryState } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET000_TYPE_A_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget000TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
