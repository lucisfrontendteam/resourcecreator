import * as actionTypes from './actionTypes';

export function Widget000TypeAState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET000_TYPE_A_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
