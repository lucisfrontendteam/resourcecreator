import Widget000TypeASampleDatas from './Widget000TypeASampleDatas';
import Widget000TypeADefaultOptions from './Widget000TypeADefaultOptions';

const {
  chart = {
    data: [],
  },
  grid = {
    data: [],
  },
} = Widget000TypeASampleDatas;

export default {
  chart: {
    data: chart.data,
  },
  grid: {
    datas: grid.data,
    options: Widget000TypeADefaultOptions.grid.options,
  },
};
