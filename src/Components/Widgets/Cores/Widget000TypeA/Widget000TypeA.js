import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import $ from 'jquery';
import _ from 'lodash';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import RingDonut from '../../../../Components/Widgets/Parts/Chart/RingDonut';
import Widget000TypeADataAdapter from './Widget000TypeADataAdapter';
import defaultDatas from './Widget000TypeADefaultDatas';
import TextRenderer from '../../Utils/TextRenderer';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import './custom.css';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget000TypeA extends PureComponent {
  static defaultProps = {
    ringDonutId: v4(),
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget000typeaInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget000typeaInitAction(props);
    }
  }

  componentWillUnmount() {
    const {
      extraqueryInitAction = () => {},
    } = this.props;
    extraqueryInitAction();
  }

  getLegendClassName(formattedIndex, selectedCategoryId, recordId) {
    let deActivateClassName = '';
    if (selectedCategoryId !== '') {
      deActivateClassName = selectedCategoryId === recordId ? '' : 'deactive';
    }
    const labelClassName = `label-${formattedIndex} ${deActivateClassName}`;
    return labelClassName;
  }

  renderChartLegend(records, legendTotal, start, end, CategoryFilterListState = { id: '' }) {
    const { id: selectedCategoryId } = CategoryFilterListState;
    const targetRecords = records.slice(start, end);
    return targetRecords.map((record, index) => {
      const formattedIndex = TextRenderer.renderZeroPrefixNumber(index + start + 1);
      const formattedValue = TextRenderer.renderNumber(record.value);
      const formattedAvg = TextRenderer.renderAvgPercentage(record.value, legendTotal);
      const legendText = `${formattedValue} 건(${formattedAvg})`;
      const { id: recordId } = record;
      const labelClass = this.getLegendClassName(formattedIndex, selectedCategoryId, recordId);
      return (
        <li
          id={recordId}
          key={v4()}
          className={labelClass}
          onClick={() => {
            this.handleLegendOnClick(recordId, index + start);
          }}
        >
          <p className="marker">{formattedIndex}</p>
          <dl>
            <dt>{record.label}</dt>
            <dd>{legendText}</dd>
          </dl>
        </li>
      );
    });
  }

  toggleLegends(segmentId, props) {
    const id = _.toString(segmentId);
    $('div.widget-chart-01 li').removeClass('deactive').each((index, li) => {
      if (li.id === id) {
        const {
          extraCategory1FilterListStateInitAction = () => {},
        } = props;
        extraCategory1FilterListStateInitAction(segmentId);
      } else {
        $(li).addClass('deactive');
      }
    });
  }

  activeAllLegends(props) {
    $('div.widget-chart-01 li').removeClass('deactive');
    const {
      extraqueryInitAction = () => {},
    } = props;
    extraqueryInitAction();
  }

  handleLegendOnClick(segmentId, index) {
    const {
      props,
      toggleLegends,
      activeAllLegends,
    } = this;
    const d3pie = window[props.ringDonutId];
    const openPiece = d3pie.getOpenSegment();

    if (openPiece) {
      activeAllLegends(props);
      d3pie.closeSegment(openPiece.index);
    } else {
      toggleLegends(segmentId, props);
      d3pie.openSegment(index);
    }
  }

  handlePiePieceOnClick(e, toggleLegends, activeAllLegends) {
    const activateToBe = !!!e.expanded;
    if (activateToBe) {
      const { id } = e.data;
      toggleLegends(id);
    } else {
      activeAllLegends();
    }
  }

  get chartStyle() {
    const { props } = this;
    const { ProjectState } = props;
    const projectClassName = ProjectState.projectThemeId;
    return projectClassName === 'theme-b' ? { marginBottom: '15px' } : {};
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget000TypeADataAdapter(widgetState);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { chartData, gridData } = adapter;
      const {
        content: legendRecords,
        total: legendTotal,
      } = chartData;

      const { CategoryFilterListState } = props.ExtraQueryState;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-core">
              <div className="lucis-core-body">
                <div className="widget-chart-01" style={this.chartStyle}>
                  <div className="chart-01">
                    <RingDonut
                      {...props}
                      data={chartData} height={345} width={320}
                      id={props.ringDonutId}
                      handlePiePieceOnClick={this.handlePiePieceOnClick}
                      toggleLegends={(id) => this.toggleLegends(id, props)}
                      activeAllLegends={() => this.activeAllLegends(props)}
                    />
                  </div>
                  <div className="chart-01-label">
                    <ul>
                      {
                        this.renderChartLegend(
                          legendRecords,
                          legendTotal, 0, 4,
                          CategoryFilterListState
                        )
                      }
                    </ul>
                    <ul>
                      {
                        this.renderChartLegend(
                          legendRecords,
                          legendTotal, 4, 8,
                          CategoryFilterListState
                        )
                      }
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div className="lucis-core pos-bottom-fix">
              <div className="lucis-core-body">
                <BasicDataTable
                  {...props}
                  options={defaultDatas.grid.options}
                  datas={gridData}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }

    return core;
  }
}
