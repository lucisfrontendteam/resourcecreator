export default {
  id: 'Widget000TypeA',
  stateId: 'Widget000TypeA',
  name: '주요 상담현황',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/category/widget000TypeA.api',
  gridFieldOrder: [
    'onestop',
    'teltimes',
    'silence',
  ],
};
