import _ from 'lodash';
import manifest from './manifest';
import DataTableDataAdapter from '../../Utils/DataTableDataAdapter';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import TextRenderer from '../../Utils/TextRenderer';
import { validationError, dataEmpty } from '../../Utils/WidgetConstants';

export default class Widget000TypeADataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  get rankColors() {
    const colors = _.map(_.range(0, 8), value => TextRenderer.renderRgbColor(value));
    return colors;
  }

  constructor(state = this.emptyState) {
    const result = validate(state, scheme);
    const filteredChartRecords = _.filter(state.chart.data, record => record.value > 0);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    } else if (_.isEmpty(filteredChartRecords) || _.isEmpty(state.grid.datas)) {
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        '표시할 데이터가 없습니다.',
      ];
      this.errorType = dataEmpty;
    }
    this.state = {
      ...state,
      chart: {
        data: filteredChartRecords,
      },
    };
  }

  get chartRecords() {
    const { chart } = this.state;
    let chartRecords = [];
    if (chart && chart.data) {
      chartRecords = _.orderBy(chart.data, ['value'], ['desc']);
    }
    return chartRecords;
  }

  get gridRecords() {
    const { grid } = this.state;
    let gridRecords = [];
    if (grid && grid.datas) {
      const [gridData] = grid.datas;
      const { onestop, teltimes, silence } = gridData;
      gridRecords = [{ onestop, teltimes, silence }];
    }
    return gridRecords;
  }

  get chartContent() {
    // const filteredRecords = _.filter(this.chartRecords, record => record.value > 0);
    const content = _.map(this.chartRecords, (record, index) => ({
      ...record,
      color: this.rankColors[index],
    }));

    return content;
  }

  get totalData() {
    const total = _.sumBy(this.chartRecords, 'value');
    return total;
  }

  get chartData() {
    const chartData = {
      total: this.totalData,
      content: this.chartContent,
      sortOrder: 'value-desc',
    };
    return chartData;
  }

  get gridData() {
    const newGridRecords = this.gridRecords;
    const gridData = DataTableDataAdapter.reorderedFields(
      manifest.gridFieldOrder,
      newGridRecords,
    );
    return gridData;
  }
}
