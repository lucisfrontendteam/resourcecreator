export default {
  id: 'Widget002',
  stateId: 'Widget002',
  name: '키워드 변화량',
  usage: '읽기',
  type: 'Widget',
  sizes: [4],
  method: 'post',
  apiUrl: '/keyterm/widget002.api',
  gridFieldOrder: [
    'keyword',
    'keywordTels',
    'totalTels',
    'concernKeyWord',
    'keywordApparentFrequency',
  ],
};
