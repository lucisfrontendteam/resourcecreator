import TextRenderer from '../../Utils/TextRenderer';
import DataTableCallbacks from '../../Utils/DataTableCallbacks';

export default {
  grid: {
    options: {
      columnDefs: [
        {
          title: '주요 키워드',
          targets: 0,
          width: '33%',
          render: TextRenderer.renderText,
        },
        {
          title: '녹취 비율',
          targets: 1,
          width: '16%',
          render: TextRenderer.renderPercentage,
        },
        {
          title: '증/감',
          targets: 2,
          width: '16%',
          render: TextRenderer.renderPercentage,
        },
        {
          title: '연관어',
          targets: 3,
          width: '20%',
          render: TextRenderer.renderText,
        },
        {
          title: '추이',
          targets: 4,
          width: 'auto',
          className: 'sparkline',
        },
      ],
      initComplete: DataTableCallbacks.initSparklinesWhenDataTableComplate,
    },
  },
};
