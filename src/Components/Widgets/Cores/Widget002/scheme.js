/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "keyword": {
                "type": "string"
              },
              "keywordTels": {
                "type": "integer"
              },
              "totalTels": {
                "type": "integer"
              },
              "concernKeyWord": {
                "type": "string"
              },
              "keywordApparentFrequency": {
                "type": "array",
                "items": {
                  "type": "integer"
                }
              }
            },
            "required": [
              "keyword",
              "keywordTels",
              "totalTels",
              "concernKeyWord",
              "keywordApparentFrequency"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    }
  },
  "required": [
    "grid"
  ]
}
