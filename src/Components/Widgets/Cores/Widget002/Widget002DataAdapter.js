import _ from 'lodash';
import DataTableDataAdapter from '../../Utils/DataTableDataAdapter';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget002DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState, option) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.data = data;
    this.option = option;
  }

  get datas() {
    return this.data.grid.datas.map(record => {
      const {
        keyword,
        keywordTels,
        totalTels,
        concernKeyWord,
        keywordApparentFrequency,
      } = record;
      const comparedValueWithYesterDayRatio =
        new DataTableDataAdapter(keywordApparentFrequency).getComparedValueWithYesterDayRatio();

      return {
        keyword,
        recordingRate: keywordTels / totalTels,
        comparedValueWithYesterDayRatio,
        concernKeyWord,
        keywordApparentFrequency,
      };
    });
  }

  get options() {
    return this.option.grid.options;
  }

  get defaultData() {
    return {
      grid: {
        datas: this.datas,
        options: this.options,
      },
    };
  }
}
