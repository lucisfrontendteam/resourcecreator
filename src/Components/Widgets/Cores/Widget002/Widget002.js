import React, { PureComponent } from 'react';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import Widget002DefaultOptions from './Widget002DefaultOptions';
import Widget002DataAdapter from './Widget002DataAdapter';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget002 extends PureComponent {
  static defaultProps = {
    tableClassName: 'lucis-table stripe nowrap',
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget002InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget002InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget002DataAdapter(
      widgetState,
      Widget002DefaultOptions
    );
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-core">
              <div className="lucis-core-header">
                <h3 className="lucis-core-title">{props.name}</h3>
              </div>
              <div className="lucis-core-body">
                <BasicDataTable
                  {...props}
                  options={defaultData.grid.options}
                  datas={defaultData.grid.datas}
                  tableClassName={props.tableClassName}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
