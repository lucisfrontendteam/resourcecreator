import * as actionTypes from './actionTypes';
import Widget002SampleDatas from '../Widget002SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget002InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET002_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget002SampleDatas,
    };
    AjaxManager.run(param);
  };
}
