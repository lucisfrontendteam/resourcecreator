export default {
  id: 'Widget001TypeA',
  stateId: 'Widget001TypeA',
  name: '상담품질 지표',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/category/widget001TypeA.api',
  gridFieldOrder: [
    'keyword',
    'tels',
    'teltimes',
  ],
};
