import React, { PureComponent } from 'react';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import Widget001TypeADataAdapter from './Widget001TypeADataAdapter';
import Widget001TypeADefaultOptions from './Widget001TypeADefaultOptions';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget001TypeA extends PureComponent {
  static defaultProps = {
    tableClassName: 'lucis-table stripe nowrap',
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget001typeaInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget001typeaInitAction(props);
    }
  }

  get grid2Style() {
    const { props } = this;
    const { ProjectState } = props;
    const projectClassName = ProjectState.projectThemeId;
    return projectClassName === 'theme-b' ? { marginTop: '36px' } : {};
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter
      = new Widget001TypeADataAdapter(widgetState, Widget001TypeADefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="row">
              <div className="col-md-12">
                <div className="lucis-core">
                  <div className="lucis-core-header">
                    <h3 className="lucis-core-title">평균 통화시간</h3>
                  </div>
                  <div className="lucis-core-body">
                    <BasicDataTable
                      {...props}
                      options={defaultData.grid1.options}
                      datas={defaultData.grid1.datas}
                      tableClassName={props.tableClassName}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="row" style={this.grid2Style}>
              <div className="col-md-12">
                <div className="lucis-core">
                  <div className="lucis-core-header">
                    <h3 className="lucis-core-title">평균 묵음비율</h3>
                  </div>
                  <div className="lucis-core-body">
                    <BasicDataTable
                      {...props}
                      options={defaultData.grid2.options}
                      datas={defaultData.grid2.datas}
                      tableClassName={props.tableClassName}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
