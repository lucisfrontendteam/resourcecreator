import * as actionTypes from './actionTypes';
import Widget001TypeASampleDatas from '../Widget001TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget001typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET001_TYPE_A_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget001TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
