import * as actionTypes from './actionTypes';

export function Widget001TypeAState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET001_TYPE_A_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
