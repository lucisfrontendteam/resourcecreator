/* eslint-disable */
/**
 * 위젯 식별자: WIDGET001TypeA
 * 위젯 이름: 상담품질 지표
 */
export default {
  grid1: {
    // 평균 통화시간 그리드의 데이터 배열
    labels: ['도급사 A', '도급사 B', '도급사 C'],
    datas: [{
      keyword: '유통성 예금',
      values: [38000, 22000, 12000],
    }, {
      keyword: '수신 이슈',
      values: [38000, 22000, 12000],
    }, {
      keyword: '신용 대출',
      values: [38000, 22000, 12000],
    }, {
      keyword: '아파트 담보대출',
      values: [38000, 22000, 12000],
    }, {
      keyword: '적립식 예금',
      values: [38000, 22000, 12000],
    }],
  },
  grid2: {
    // 평균 묵음비율 그리드의 데이터 배열
    labels: ['도급사 A', '도급사 B', '도급사 C'],
    datas: [{
      keyword: '유통성 예금',
      values: [0.1, 0.22, 0.33],
    }, {
      keyword: '수신 이슈',
      values: [0.38, 0.22, 0.2],
    }, {
      keyword: '신용 대출',
      values: [0.38, 0.22, 0.2],
    }, {
      keyword: '아파트 담보대출',
      values: [0.38, 0.22, 0.2],
    }, {
      keyword: '적립식 예금',
      values: [0.38, 0.22, 0.2],
    }],
  },
};
