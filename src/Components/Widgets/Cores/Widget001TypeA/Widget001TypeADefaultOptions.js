export default {
  grid1: {
    options: {
      scrollY: 125,
      scrollX: true,
      fixedColumns: {
        leftColumns: 1,
      },
    },
  },
  grid2: {
    options: {
      scrollY: 125,
      scrollX: true,
      fixedColumns: {
        leftColumns: 1,
      },
    },
  },
};
