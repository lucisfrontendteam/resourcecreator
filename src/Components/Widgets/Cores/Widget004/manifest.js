export default {
  id: 'Widget004',
  stateId: 'Widget004',
  name: '이슈 / VOC 현황',
  usage: '읽기',
  type: 'Widget',
  sizes: [4],
  method: 'post',
  apiUrl: '/category/widget004.api',
};
