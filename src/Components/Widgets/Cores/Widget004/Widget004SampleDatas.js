/**
 * 위젯 식별자: WIDGET004
 * 위젯 이름: 이슈/VOC 현황
 */
export default {
  grid: {
    // 이슈/VOC 현황 그리드의 데이터 배열(당일 상담 건수 내림차순)
    datas: [{
      // 이슈/VOC 현황 카테고리
      category: '언론보도',
      // 이슈/VOC 현황 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [34, 56],
    }, {
      // 이슈/VOC 현황 카테고리
      category: '고소/고발',
      // 이슈/VOC 현황 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [31, 54],
    }, {
      // 이슈/VOC 현황 카테고리
      category: '불만/분쟁',
      // 이슈/VOC 현황 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [28, 48],
    }, {
      // 이슈/VOC 현황 카테고리
      category: '욕설',
      // 이슈/VOC 현황 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [22, 40],
    }, {
      // 이슈/VOC 현황 카테고리
      category: '칭찬 콜',
      // 이슈/VOC 현황 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [18, 10],
    }],
  },
};
