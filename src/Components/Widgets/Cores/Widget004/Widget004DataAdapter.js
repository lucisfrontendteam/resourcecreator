import _ from 'lodash';
import DataTableDataAdapter from '../../Utils/DataTableDataAdapter';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget004DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState, option) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.data = data;
    this.option = option;
  }

  get datas() {
    return this.data.grid.datas.map(record => {
      const {
        category,
        categoryApparentFrequency,
      } = record;
      const adapter = new DataTableDataAdapter(categoryApparentFrequency);
      const todayValue = adapter.getTodayValue();
      const yesterdayValue = adapter.getYeterdayValue();

      return {
        category,
        todayValue,
        yesterdayValue,
      };
    });
  }

  get options() {
    return this.option.grid.options;
  }

  get degree() {
    const todayValues = _.map(this.datas, data => data.todayValue);
    const sums = _.reduce(todayValues, (sum, n) => sum + n, 0);
    let degree = 0;
    if (_.inRange(sums, 0, 20)) {
      degree = 0;
    } else if (_.inRange(sums, 20, 40)) {
      degree = 1;
    } else if (_.inRange(sums, 40, 60)) {
      degree = 2;
    } else if (_.inRange(sums, 60, 80)) {
      degree = 3;
    } else {
      degree = 4;
    }
    return degree;
  }

  get defaultData() {
    return {
      chart: {
        degree: this.degree,
      },
      grid: {
        datas: this.datas,
        options: this.options,
      },
    };
  }
}
