import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      columnDefs: [
        {
          title: '카테고리',
          targets: 0,
          width: '50%',
          render: TextRenderer.renderText,
        },
        {
          title: '당일 통화량',
          targets: 1,
          width: '30%',
          render: TextRenderer.renderNumberWithGun,
        },
        {
          title: '전일',
          targets: 2,
          width: 'auto',
          render: TextRenderer.renderNumberWithGun,
        },
      ],
    },
  },
};
