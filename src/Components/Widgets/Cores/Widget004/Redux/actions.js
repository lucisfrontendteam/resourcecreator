import * as actionTypes from './actionTypes';
import Widget004SampleDatas from '../Widget004SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget004InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET004_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget004SampleDatas,
    };
    AjaxManager.run(param);
  };
}
