import React, { PureComponent } from 'react';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import Widget004DataAdapter from './Widget004DataAdapter';
import Widget004DefaultOptions from './Widget004DefaultOptions';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import { Weather } from '../../Parts/Chart/Weather';
import './custom.css';

export class Widget004 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget004InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget004InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter
      = new Widget004DataAdapter(widgetState, Widget004DefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
            <div className="img-weather">
              <Weather {...props} degree={defaultData.chart.degree} />
            </div>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-core">
              <div className="lucis-core-header">
                <h3 className="lucis-core-title">이슈 / VOC 현황</h3>
              </div>
              <div className="lucis-core-body">
                <BasicDataTable
                  {...props}
                  options={defaultData.grid.options}
                  datas={defaultData.grid.datas}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
