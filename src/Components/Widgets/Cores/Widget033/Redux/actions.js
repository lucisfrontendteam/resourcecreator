import * as actionTypes from './actionTypes';
import Widget033SampleDatas from '../Widget033SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget033InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET033_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget033SampleDatas,
    };
    AjaxManager.run(param);
  };
}
