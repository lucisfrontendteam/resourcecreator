export default {
  id: 'Widget033',
  stateId: 'Widget033',
  name: 'Tops 등급별 비율',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/keyterm/widget033.api',
};
