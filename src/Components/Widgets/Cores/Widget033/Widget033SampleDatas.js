/**
 * 위젯 식별자: WIDGET033
 * 위젯 이름: Tops 등급별 비율
 */
export default {
  chart: {
    // Tops 등급별 횟수
    data: [
      {
        // 카테고리 이름
        categoryName: '일반',
        // 카테고리의 남성 횟수
        leftValue: 50,
        // 카테고리의 여성 횟수
        rightValue: 50,
      },
      {
        // 카테고리 이름
        categoryName: '클래식',
        // 카테고리의 남성 횟수
        leftValue: 70,
        // 카테고리의 여성 횟수
        rightValue: 30,
      },
      {
        // 카테고리 이름
        categoryName: '베스트',
        // 카테고리의 남성 횟수
        leftValue: 40,
        // 카테고리의 여성 횟수
        rightValue: 60,
      },
      {
        // 카테고리 이름
        categoryName: '에이스',
        // 카테고리의 남성 횟수
        leftValue: 20,
        // 카테고리의 여성 횟수
        rightValue: 80,
      },
      {
        // 카테고리 이름
        categoryName: '기타',
        // 카테고리의 남성 횟수
        leftValue: 60,
        // 카테고리의 여성 횟수
        rightValue: 40,
      },
    ],
  },
};
