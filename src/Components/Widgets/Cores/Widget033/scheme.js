/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "categoryName": {
                "type": "string"
              },
              "leftValue": {
                "type": "integer"
              },
              "rightValue": {
                "type": "integer"
              }
            },
            "required": [
              "categoryName",
              "leftValue",
              "rightValue"
            ]
          }
        }
      },
      "required": [
        "data"
      ]
    }
  },
  "required": [
    "chart"
  ]
};
