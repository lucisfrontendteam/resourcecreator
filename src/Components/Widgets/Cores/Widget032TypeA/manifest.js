export default {
  id: 'Widget032TypeA',
  stateId: 'Widget032TypeA',
  name: '통화시간 분포',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/organize/widget032TypeA.api',
};
