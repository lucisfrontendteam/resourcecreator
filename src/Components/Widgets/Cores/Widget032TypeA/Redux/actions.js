import * as actionTypes from './actionTypes';
import Widget032TypeASampleDatas from '../Widget032TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget032typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET032_TYPEA_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget032TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
