import * as actions from './Redux/actions';
import * as reducer from './Redux/reducer';
import * as actionTypes from './Redux/actionTypes';
export {
  actions as Widget032TypeAActions,
  reducer as Widget032TypeAReducer,
  actionTypes as Widget032TypeAActionTypes,
};
import manifest from './manifest';
export { manifest as Widget032TypeAManifest };
import Widget032TypeASampleDatas from './Widget032TypeASampleDatas';
export { Widget032TypeASampleDatas };
export { Widget032TypeA } from './Widget032TypeA';
