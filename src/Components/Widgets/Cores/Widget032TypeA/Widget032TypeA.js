import React, { PureComponent } from 'react';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import Widget032TypeADataAdapter from './Widget032TypeADataAdapter';
import Widget032TypeADefaultOptions from './Widget032TypeADefaultOptions';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget032TypeA extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget032typeaInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget032typeaInitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget032TypeADataAdapter(widgetState, Widget032TypeADefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const { data, options } = defaultData.chart;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="widget-chart-13">
              <ChartJsMultiBar data={data} options={options} />
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
