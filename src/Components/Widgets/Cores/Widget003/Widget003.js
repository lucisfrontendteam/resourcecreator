import React, { PureComponent } from 'react';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import Widget003DataAdapter from './Widget003DataAdapter';
import Widget003DefaultOptions from './Widget003DefaultOptions';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget003 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget003InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget003InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter
      = new Widget003DataAdapter(widgetState, Widget003DefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-core">
              <div className="lucis-core-header">
                <h3 className="lucis-core-title">관심 통화 추이</h3>
              </div>
              <div className="lucis-core-body">
                <BasicDataTable
                  {...props}
                  options={defaultData.grid.options}
                  datas={defaultData.grid.datas}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
