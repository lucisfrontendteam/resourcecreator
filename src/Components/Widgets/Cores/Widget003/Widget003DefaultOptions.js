import TextRenderer from '../../Utils/TextRenderer';
import DataTableCallbacks from '../../Utils/DataTableCallbacks';

export default {
  grid: {
    options: {
      columnDefs: [
        {
          title: '카테고리',
          targets: 0,
          width: '35%',
          render: TextRenderer.renderText,
        },
        {
          title: '당일 통화량',
          targets: 1,
          width: '25%',
          render: TextRenderer.renderNumberWithGun,
        },
        {
          title: '전일',
          targets: 2,
          width: '20%',
          render: TextRenderer.renderNumberWithGun,
        },
        {
          title: '최근 1주',
          targets: 3,
          width: 'auto',
          className: 'sparkline',
        },
      ],
      initComplete: DataTableCallbacks.initSparklinesWhenDataTableComplate,
    },
  },
};
