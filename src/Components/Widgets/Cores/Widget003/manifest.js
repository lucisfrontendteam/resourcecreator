export default {
  id: 'Widget003',
  stateId: 'Widget003',
  name: '관심 통화 추이',
  usage: '읽기',
  type: 'Widget',
  sizes: [4],
  method: 'post',
  apiUrl: '/category/widget003.api',
  gridFieldOrder: [
    'keyword',
    'keywordApparentFrequency',
  ],
};
