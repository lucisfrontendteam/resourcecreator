/**
 * 위젯 식별자: WIDGET003
 * 위젯 이름: 관심 통화 추이
 */
export default {
  grid: {
    // 관심 통화 추이 그리드의 데이터 배열(당일 상담 건수 내림차순)
    datas: [{
      // 관심 통화 카테고리
      category: '써니 뱅크',
      // 관심 통화 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [
        12, 24, 43, 34, 25, 16, 97,
      ],
    }, {
      // 관심 통화 카테고리
      category: '브렉시트',
      // 관심 통화 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [
        12, 24, 43, 34, 25, 8, 80,
      ],
    }, {
      // 관심 통화 카테고리
      category: '펀드 환매',
      // 관심 통화 카테고리에 매칭된 상담 건수 (통화일 오름차순)
      categoryApparentFrequency: [
        12, 24, 43, 34, 25, 7, 50,
      ],
    }],
  },
};
