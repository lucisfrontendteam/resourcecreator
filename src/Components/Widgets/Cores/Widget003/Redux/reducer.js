import * as actionTypes from './actionTypes';

export function Widget003State(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET003_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
