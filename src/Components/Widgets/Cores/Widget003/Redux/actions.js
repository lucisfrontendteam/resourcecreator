import * as actionTypes from './actionTypes';
import Widget003SampleDatas from '../Widget003SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget003InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET003_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget003SampleDatas,
    };
    AjaxManager.run(param);
  };
}
