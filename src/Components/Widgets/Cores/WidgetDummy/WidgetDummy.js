import { PureComponent } from 'react';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class WidgetDummy extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  renderWidgetCore() {
    return WidgetRenderer.renderDataFailWidgetCore(true, []);
  }
}
