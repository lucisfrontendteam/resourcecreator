export default {
  id: 'WidgetDummy',
  stateId: 'WidgetDummy',
  name: '더미 위젯',
  type: 'Widget',
  sizes: [4, 6, 8, 12],
  method: 'post',
};
