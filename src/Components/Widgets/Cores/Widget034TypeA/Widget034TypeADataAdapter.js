import _ from 'lodash';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError, dataEmpty } from '../../Utils/WidgetConstants';

export default class Widget034TypeADataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    } else if (_.isEmpty(widgetState.chart.data)) {
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        '표시할 데이터가 없습니다.',
      ];
      this.errorType = dataEmpty;
    }
    this.datas = widgetState.chart.data;
  }

  get defaultData() {
    const computeData = _.map(this.datas, record => {
      const { categoryName, leftValue, rightValue } = record;
      return {
        categoryName,
        value: leftValue + rightValue,
      };
    });
    return {
      chart: {
        data: computeData,
      },
    };
  }
}
