import * as actionTypes from './actionTypes';
import Widget034TypeASampleDatas from '../Widget034TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget034typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET034_TYPEA_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget034TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
