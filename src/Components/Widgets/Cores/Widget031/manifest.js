export default {
  id: 'Widget031',
  stateId: 'Widget031',
  name: '그룹별 통화량',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/organize/widget031.api',
};
