import * as actionTypes from './actionTypes';
import Widget031SampleDatas from '../Widget031SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget031InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET031_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget031SampleDatas,
    };
    AjaxManager.run(param);
  };
}
