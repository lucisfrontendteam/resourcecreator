import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget031DataAdapter {
  static commonDataOption = {
    label: '하청1',
    fill: false,
    lineTension: 0.1,
    backgroundColor: '#0c99c9',
    borderColor: '#0c99c9',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    pointBorderColor: '#0c99c9',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: '#0c99c9',
    pointHoverBorderColor: '#0c99c9',
    pointHoverBorderWidth: 2,
    pointRadius: 1,
    pointHitRadius: 10,
    data: [65, 59, 80, 81],
    spanGaps: false,
  };

  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(datas = this.emptyState, options) {
    const result = validate(datas, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.datas = datas;
    this.options = options.chart.options;
  }

  get labels() {
    const { labels } = this.datas.chart;
    return labels;
  }

  get datasets() {
    const { data: records } = this.datas.chart;
    return _.map(records, (record, index) => {
      const { data, label } = record;
      const color = TextRenderer.renderHexColor(index);
      return {
        ...Widget031DataAdapter.commonDataOption,
        data,
        label,
        borderColor: color,
        backgroundColor: color,
        pointBorderColor: color,
        pointHoverBackgroundColor: color,
        pointHoverBorderColor: color,
        pointBackgroundColor: color,
      };
    });
  }

  get defaultData() {
    return {
      chart: {
        data: {
          labels: this.labels,
          datasets: this.datasets,
        },
        options: this.options,
      },
    };
  }
}
