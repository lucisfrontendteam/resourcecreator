import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import TextRenderer from '../../Utils/TextRenderer';
import Widget043DataAdapter from './Widget043DataAdapter';
import Widget043DefaultOptions from './Widget043DefaultOptions';
import manifest from './manifest';
import ExcelDownloader from '../../Utils/ExcelDownloader';
import _ from 'lodash';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import ReactPaginate from 'react-paginate';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget043 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.firePagingAction = props.firePagingAction.bind(this);
    this.handlePagingClick = props.handlePagingClick.bind(this);
    this.render = props.render.bind(this);
    this.state = { pageNumber: 1 };
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    const pageChanged = state.pageNumber !== prevState.pageNumber;
    const isFirstPage = state.pageNumber === 1;
    if (pageChanged || isFirstPage) {
      this.firePagingAction(
        props,
        props.widget043InitAction,
        state.pageNumber,
        manifest.recordCount
      );
    }
  }

  componentDidMount() {
    const { props, state } = this;
    if (props.isTestMode === 'true') {
      this.firePagingAction(
        props,
        props.widget043InitAction,
        state.pageNumber,
        manifest.recordCount
      );
    }
  }

  handleExportExcelClick(e) {
    e.preventDefault();
    const { props } = this;
    const { excelUrl } = new ExcelDownloader(props, manifest);
    window.open(excelUrl);
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const { QueryStateManagerState, AdvencedFilterArgManagerState } = props;
    const adapter = new Widget043DataAdapter(widgetState, Widget043DefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const infos = TextRenderer.renderQueryStates(
        QueryStateManagerState,
        AdvencedFilterArgManagerState
      );
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
            <div className="pos-right">
              <button
                type="button"
                className="btn-tool btn-file"
                onClick={(e) => this.handleExportExcelClick(e)}
              >엑셀저장</button>
            </div>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-table-label" style={{ height: '66px' }}>
              {this.renderInfos(infos)}
            </div>
            <BasicDataTable
              {...props}
              options={defaultData.grid.options}
              datas={defaultData.grid.datas}
              tableClassName="lucis-table stripe nowrap"
            />
            <div className="clearfix">
              <ReactPaginate
                pageNum={defaultData.grid.totalPageNumber}
                clickCallback={this.handlePagingClick}
                {...props.pagingProps}
              />
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }

  renderInfos(infos) {
    const messages = _.map(infos, info => <p key={v4()} style={{ marginBottom: '4px' }}>{info}</p>);
    return messages;
  }
}
