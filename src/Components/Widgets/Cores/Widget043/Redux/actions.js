import * as actionTypes from './actionTypes';
import Widget043SampleDatas from '../Widget043SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget043InitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryStateManagerState, QueryIdMap } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET043_INIT,
      query: {
        ...QueryStateManagerState,
        ...QueryIdMap,
      },
      ajaxType: 'normal',
      sampleData: Widget043SampleDatas,
    };
    AjaxManager.run(param);
  };
}
