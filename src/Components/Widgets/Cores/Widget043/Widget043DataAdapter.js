import _ from 'lodash';
import 'moment/locale/ko';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import TextRenderer from '../../Utils/TextRenderer';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget043DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState, widgetOptions) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.totalCount = widgetState.grid.totalCount;
    this.datas = widgetState.grid.datas;
    this.options = widgetOptions.grid.options;
  }

  get dataArray() {
    return _.map(this.datas, data => data.record);
  }

  getSummuryValue(values) {
    return _.reduce(values, (sum, n) => sum + n, 0);
  }

  get summuries() {
    const { pivotArray } = this;
    const [, , ...numbers] = pivotArray;
    return _.map(numbers, record => this.getSummuryValue(record));
  }

  get pivotArray() {
    const pivotArray = [];
    _.forEach(this.dataArray, (data, recordIndex) => {
      _.forEach(data, (point, pointIndex) => {
        if (!!!_.isArray(pivotArray[pointIndex])) {
          pivotArray[pointIndex] = [];
        }
        pivotArray[pointIndex][recordIndex] = point;
      });
    });
    return pivotArray;
  }

  get finalDatas() {
    const { summuries, dataArray } = this;
    const summurieRecord = ['계', '', ...summuries];
    return [summurieRecord, ...dataArray];
  }

  get totalPageNumber() {
    // 그리드 데이터에서 취득해야 한다는 점에 주의
    const totalPageNumber = TextRenderer.renderTotalPages(this.totalCount, manifest.recordCount);
    return totalPageNumber;
  }

  get defaultData() {
    const { totalCount, dataArray, options, totalPageNumber } = this;
    return {
      grid: {
        totalCount,
        totalPageNumber,
        datas: dataArray,
        options,
      },
    };
  }
}
