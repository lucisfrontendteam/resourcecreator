import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      searching: false,
      paging: false,
      ordering: false,
      info: false,
      scrollX: true,
      fixedColumns: {
        leftColumns: 2,
      },
      columnDefs: [
        {
          title: 'No',
          targets: 0,
          render: TextRenderer.renderNumber,
        },
        {
          title: '카테고리명',
          targets: 1,
          render: TextRenderer.renderText,
        },
        {
          title: '통화 시작시간',
          targets: 2,
          render(value) {
            return TextRenderer.renderDateTimeString(value);
          },
        },
        {
          title: '통화시간대',
          targets: 3,
          render: TextRenderer.renderText,
        },
        {
          title: '통화시간',
          targets: 4,
          render: TextRenderer.renderLocaleDateDuration,
        },
        {
          title: '묵음비율',
          targets: 5,
          render: TextRenderer.renderPercentage,
        },
        {
          title: '묵음시간',
          targets: 6,
          render: TextRenderer.renderLocaleDateDuration,
        },
        {
          title: '호 방향',
          targets: 7,
          render: TextRenderer.renderText,
        },
        {
          title: 'INDEX',
          targets: 8,
          render: TextRenderer.renderText,
        },
        {
          title: '통화유형 코드^통화유형명',
          targets: 9,
          render: TextRenderer.renderText,
        },
        {
          title: '팀명',
          targets: 10,
          render: TextRenderer.renderText,
        },
        {
          title: '사번',
          targets: 11,
          render: TextRenderer.renderText,
        },
        {
          title: '회사명',
          targets: 12,
          render: TextRenderer.renderText,
        },
        {
          title: '상담사명',
          targets: 13,
          render: TextRenderer.renderText,
        },
        {
          title: '근속 개월수',
          targets: 14,
          render: TextRenderer.renderNumberWithMonth,
        },
        {
          title: '업무그룹',
          targets: 15,
          render: TextRenderer.renderText,
        },
        {
          title: 'Top등급코드^Tops등급명',
          targets: 16,
          render: TextRenderer.renderText,
        },
        {
          title: '고객나이',
          targets: 17,
          render: TextRenderer.renderNumberWithAge,
        },
        {
          title: '고객성별',
          targets: 18,
          render: TextRenderer.renderText,
        },
        {
          title: '고객번호',
          targets: 19,
          render: TextRenderer.renderText,
        },
      ],
    },
  },
};
