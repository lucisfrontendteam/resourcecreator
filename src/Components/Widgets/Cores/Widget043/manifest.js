export default {
  id: 'Widget043',
  stateId: 'Widget043',
  name: '상세 콜 조회',
  usage: '읽기',
  sizes: [12],
  recordCount: 25,
  type: 'IdList',
  inputIdList: [
    {
      key: 'pageNumber',
      id: 'Widget043pageNumber',
    },
    {
      key: 'pageSize',
      id: 'Widget043pageSize',
    },
  ],
  method: 'post',
  apiUrl: '/category/widget043.api',
  excelUrl: '/category/widget043.excel',
};
