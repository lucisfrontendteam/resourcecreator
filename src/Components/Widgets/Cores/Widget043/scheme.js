/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid": {
      "type": "object",
      "properties": {
        "totalCount": {
          "type": "integer"
        },
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "record": {
                "type": "array",
                "items": {
                  "type": ["any"]
                }
              }
            },
            "required": [
              "record"
            ]
          }
        }
      },
      "required": [
        "totalCount",
        "datas"
      ]
    }
  },
  "required": [
    "grid"
  ]
}
