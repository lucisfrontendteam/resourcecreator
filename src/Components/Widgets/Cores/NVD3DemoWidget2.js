import React, { PureComponent } from 'react';
import MultiBar from '../Parts/Chart/MultiBar';
import StackedArea from '../Parts/Chart/StackedArea';

export class NVD3DemoWidget2 extends PureComponent {
  static defaultProps = {
    id: 'NVD3DemoWidget2',
    name: 'NVD3 데모2',
    type: 'Widget',
    sizes: [6],
  };

  render() {
    const { props } = this;

    return (
      <div>
        <MultiBar {...props} />
        <StackedArea {...props} />
      </div>
    );
  }
}
