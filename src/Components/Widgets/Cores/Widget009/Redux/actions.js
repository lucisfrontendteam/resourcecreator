import * as actionTypes from './actionTypes';
import Widget009SampleDatas from '../Widget009SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget009InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET009_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget009SampleDatas,
    };
    AjaxManager.run(param);
  };
}
