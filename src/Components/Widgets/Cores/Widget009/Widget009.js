import React, { PureComponent } from 'react';
import LineBarProxy from '../../../Widgets/Parts/Chart/LineBarProxy';
import MultiProgressBar from '../../../Widgets/Parts/Chart/MultiProgressBar';
import Widget009DefaultOptions from './Widget009DefaultOptions';
import Widget009DataAdapter from './Widget009DataAdapter';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget009 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget009InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget009InitAction(props);
    }
  }

  handleSearch(e) {
    // eslint-disable-next-line
    alert('유형검색 준비중입니다.');
    e.preventDefault();
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget009DataAdapter(
      widgetState,
      Widget009DefaultOptions,
      props.ExtraQueryState,
    );
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">트렌드</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="row">
              <div className="col-md-6">
                <div className="lucis-core">
                  <div className="lucis-core-header">
                    <h3 className="lucis-core-title">트렌드 그래프</h3>
                  </div>
                  <div className="lucis-core-body">
                    <div className="widget-chart-02">
                      <LineBarProxy
                        {...props}
                        datum={defaultData.chart1.data}
                        options={defaultData.chart1.options}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="lucis-core">
                  <div className="lucis-core-header">
                    <h3 className="lucis-core-title">상담 유형 트렌드</h3>
                  </div>
                  <div className="lucis-core-body">
                    <MultiProgressBar {...props} data={defaultData.chart2.data} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
