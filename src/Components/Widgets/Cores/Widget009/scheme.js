/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart1": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "key": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "values": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "x": {
                      "type": "string"
                    },
                    "y": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "x",
                    "y"
                  ]
                }
              }
            },
            "required": [
              "key",
              "id",
              "values"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    },
    "chart2": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "categoryName": {
                "type": "string"
              },
              "categoryId": {
                "type": "string"
              },
              "totalTels": {
                "type": "integer"
              },
              "totalTelTimes": {
                "type": "integer"
              },
              "silenceTelTimes": {
                "type": "integer"
              },
              "counselorTelTimes": {
                "type": "integer"
              },
              "customerTelTimes": {
                "type": "integer"
              },
              "dupsTelTimes": {
                "type": "integer"
              }
            },
            "required": [
              "categoryName",
              "categoryId",
              "totalTels",
              "totalTelTimes",
              "silenceTelTimes",
              "counselorTelTimes",
              "customerTelTimes",
              "dupsTelTimes"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    }
  },
  "required": [
    "chart1",
    "chart2"
  ]
};
