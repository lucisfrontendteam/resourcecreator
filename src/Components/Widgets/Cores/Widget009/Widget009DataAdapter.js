import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget009DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState, options, ExtraQueryState) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    const { datas: chart1Data } = data.chart1;
    const { datas: chart2Data } = data.chart2;
    this.chart1OrginData = chart1Data;
    this.chart2OrginData = chart2Data;
    this.chart1OrginOptions = options.chart1.options;
    const { CategoryFilterListState = {} } = ExtraQueryState;
    this.CategoryFilterListState = CategoryFilterListState;
  }

  get chart1TotalBar() {
    const totalRecord = _.find(this.chart1OrginData, data => data.id === 'total');
    const { key, values } = totalRecord;
    const totalBar = {
      key,
      bar: true,
      values: _.map(values, field => {
        const { x, y } = field;
        return {
          x: TextRenderer.renderDateObject(x),
          y,
        };
      }),
    };
    return totalBar;
  }

  get chart1Lines() {
    const { id: Category1Id } = this.CategoryFilterListState;
    const lineRecords = _.filter(this.chart1OrginData, data => {
      const { id: lineId } = data;
      const isNotTotal = lineId !== 'total';
      const selected = _.isEmpty(Category1Id) ? true : Category1Id === lineId;
      return isNotTotal && selected;
    });
    const lines = _.map(lineRecords, line => {
      const { key, values } = line;
      const data = {
        key,
        values: _.map(values, field => {
          const { x, y } = field;
          return {
            x: TextRenderer.renderDateObject(x),
            y,
          };
        }),
      };
      return data;
    });
    return lines;
  }

  get chart1Data() {
    const chart1DataList = [
      this.chart1TotalBar,
      ...this.chart1Lines,
    ];
    return chart1DataList;
  }

  get chart2TotalData() {
    const record = _.find(this.chart2OrginData, data => data.categoryId === 'total');
    return record;
  }

  get chart2CategoryData() {
    const { id: Category1Id } = this.CategoryFilterListState;
    let record = {};
    if (Category1Id) {
      record = _.find(this.chart2OrginData, data => data.categoryId === Category1Id);
    } else {
      record = {
        // 카테고리 이름
        categotyName: undefined,
        // 카테고리 아이디
        categoryId: undefined,
        // 카테고리 통화건수(단위 건)
        totalTels: undefined,
        // 카테고리 통화시간(단위 초)
        totalTelTimes: undefined,
        // 카테고리의 묵음시간(단위 초)
        silenceTelTimes: undefined,
        // 카테고리의 상담사 발화시간(단위 초)
        counselorTelTimes: undefined,
        // 카테고리의 고객 발화시간(단위 초)
        customerTelTimes: undefined,
        // 카테고리의 동시 발화시간(단위 초)
        dupsTelTimes: undefined,
      };
    }
    return record;
  }

  get chart2Data() {
    const {
      chart2TotalData,
      chart2CategoryData,
    } = this;
    const chart2DataList = [
      {
        title: '평균 통화시간',
        unit: 'datetime',
        // 전체 통화시간(단위 초)
        totalTelTimes: chart2TotalData.totalTelTimes,
        // 전체 통화건수(단위 건)
        totalTels: chart2TotalData.totalTels,
        // 카테고리의 통화시간(단위 초)
        categoryTelTimes: chart2CategoryData.totalTels,
        // 카테고리의 통화건수(단위 건)
        categoryTels: chart2CategoryData.totalTels,
      },
      {
        title: '상담사 발화 비율',
        unit: 'ratio',
        // 전체 상담사 발화시간(단위 초)
        totalTelTimes: chart2TotalData.counselorTelTimes,
        // 전체 통화건수(단위 건)
        totalTels: chart2TotalData.totalTels,
        // 카테고리의 상담사 발화시간(단위 초)
        categoryTelTimes: chart2CategoryData.counselorTelTimes,
        // 카테고리의 통화건수(단위 건)
        categoryTels: chart2CategoryData.totalTels,
      },
      {
        title: '평균 묵음비율',
        unit: 'ratio',
        // 전체 묵음시간(단위 초)
        totalTelTimes: chart2TotalData.silenceTelTimes,
        // 전체 통화건수(단위 건)
        totalTels: chart2TotalData.totalTels,
        // 가테고리의 묵음시간(단위 초)
        categoryTelTimes: chart2CategoryData.silenceTelTimes,
        // 카테고리의 통화건수(단위 건)
        categoryTels: chart2CategoryData.totalTels,
      },
      {
        title: '고객 발화 비율',
        unit: 'ratio',
        // 전체 고객 발화시간(단위 초)
        totalTelTimes: chart2TotalData.customerTelTimes,
        // 전체 통화건수(단위 건)
        totalTels: chart2TotalData.totalTels,
        // 카테고리의 고객 발화시간(단위 초)
        categoryTelTimes: chart2CategoryData.customerTelTimes,
        // 카테고리의 통화건수(단위 건)
        categoryTels: chart2CategoryData.totalTels,
      },
      {
        title: '말겹침 비율',
        unit: 'ratio',
        // 전체 말 겹침시간(단위 초)
        totalTelTimes: chart2TotalData.dupsTelTimes,
        // 전체 통화건수(단위 건)
        totalTels: chart2TotalData.totalTels,
        // 카테고리의 말 겹침시간(단위 초)
        categoryTelTimes: chart2CategoryData.dupsTelTimes,
        // 카테고리의 통화건수(단위 건)
        categoryTels: chart2CategoryData.totalTels,
      },
    ];
    return chart2DataList;
  }

  get defaultData() {
    const result = {
      chart1: {
        // 트랜드 그래프 차트의 데이터 배열
        data: this.chart1Data,
        options: this.chart1OrginOptions,
      },
      chart2: {
        // 상담 유형 트랜드 그래프 차트의 데이터 배열
        // 카테고리 전체 데이터와 카테고리 1개의 데이터가 필수
        data: this.chart2Data,
      },
    };
    return result;
  }
}
