export default {
  id: 'Widget033TypeA',
  stateId: 'Widget033TypeA',
  name: 'Tops 등급별 비율',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/keyterm/widget033.api',
};
