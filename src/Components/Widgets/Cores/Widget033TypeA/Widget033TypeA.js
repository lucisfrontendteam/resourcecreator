import React, { PureComponent } from 'react';
import SingleProgressBar from '../../../Widgets/Parts/Chart/SingleProgressBar';
import Widget033TypeADataAdapter from './Widget033TypeADataAdapter';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget033TypeA extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget033typeaInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget033typeaInitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget033TypeADataAdapter(widgetState);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <SingleProgressBar data={defaultData.chart.data} />
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
