import * as actionTypes from './actionTypes';
import Widget033TypeASampleDatas from '../Widget033TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget033typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET033_TYPEA_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget033TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
