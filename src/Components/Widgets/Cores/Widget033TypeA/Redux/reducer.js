import * as actionTypes from './actionTypes';

export function Widget033TypeAState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET033_TYPEA_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
