export default {
  id: 'Widget008',
  stateId: 'Widget008',
  name: '유형별 세부 현황',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/multicategory/widget008.api',
};
