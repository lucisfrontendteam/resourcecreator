/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart": {
      "type": "object",
      "properties": {
        "data": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "title": {
                "type": "string"
              },
              "sectors": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "id": {
                      "type": "string"
                    },
                    "name": {
                      "type": "string"
                    },
                    "value": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "id",
                    "name",
                    "value"
                  ]
                }
              }
            },
            "required": [
              "title",
              "sectors"
            ]
          }
        }
      },
      "required": [
        "data"
      ]
    }
  },
  "required": [
    "chart"
  ]
};
