import _ from 'lodash';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget008DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.data008 = data;
  }

  get data() {
    return this.data008.chart.data;
  }

  get defaultData() {
    return {
      chart: {
        data: this.data,
      },
    };
  }
}
