import React, { PureComponent } from 'react';
import HorizontalStackedBar from '../../Parts/Chart/HorizontalStackedBar';
import Widget008DataAdapter from './Widget008DataAdapter';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget008 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget008InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget008InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget008DataAdapter(widgetState);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-core">
              <div className="lucis-core-header">
                <h3 className="lucis-core-title">{props.name}</h3>
              </div>
              <div className="lucis-core-body">
                <HorizontalStackedBar {...props} records={defaultData.chart.data} />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
