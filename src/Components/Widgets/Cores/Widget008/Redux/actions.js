import * as actionTypes from './actionTypes';
import Widget008SampleDatas from '../Widget008SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget008InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET008_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget008SampleDatas,
    };
    AjaxManager.run(param);
  };
}
