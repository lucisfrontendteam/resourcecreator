import TextRenderer from '../../Utils/TextRenderer';

export default {
  chart: {
    options: {
      animation: { duration: 0 },
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { yLabel, datasetIndex } = tooltipItem;
            const { label, id } = data.datasets[datasetIndex];
            let suffix = '%';
            let value = yLabel;
            if (id === 'total') {
              suffix = '건';
              value = TextRenderer.renderNumber(yLabel);
            }
            return `${label}: ${value}${suffix}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
          usePointStyle: true,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          barThickness: 10,
          categoryPercentage: 0.2,
          barPercentage: 0.2,
          ticks: {
            fontColor: '#0c99c9',
            autoSkip: false,
            callback(value) {
              const [start, end] = value.split(':');
              const startValue = TextRenderer.renderDateTimeString(start, 'hhmm', 'HH:mm');
              const endValue = TextRenderer.renderDateTimeString(end, 'hhmm', 'HH:mm');
              const label = `${startValue} ~ ${endValue}`;
              return label;
            },
          },
          gridLines: {
            offsetGridLines: true,
          },
        }],
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-1',
            ticks: {
              fontColor: '#0c99c9',
              callback: TextRenderer.renderNumber,
              fixedStepSize: 100,
              stepSize: 1000,
              maxTicksLimit: 8,
            },
          }, {
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderPercentage(value / 100);
              },
              maxTicksLimit: 10,
              stepSize: 10,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  },
};
