export default {
  id: 'Widget030',
  stateId: 'Widget030',
  name: '시간대별 통화량',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/timezone/widget030.api',
};
