import * as actionTypes from './actionTypes';

export function Widget030State(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET030_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
