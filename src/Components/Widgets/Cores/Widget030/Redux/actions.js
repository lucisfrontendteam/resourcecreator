import * as actionTypes from './actionTypes';
import Widget030SampleDatas from '../Widget030SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget030InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET030_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget030SampleDatas,
    };
    AjaxManager.run(param);
  };
}
