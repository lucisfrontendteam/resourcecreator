import React, { PureComponent } from 'react';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import Widget030DataAdapter from './Widget030DataAdapter';
import Widget030DefaultOptions from './Widget030DefaultOptions';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget030 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget030InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget030InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter
      = new Widget030DataAdapter(widgetState, Widget030DefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const { data, options } = defaultData.chart;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="widget-chart-13">
              <ChartJsMultiBar
                data={data}
                options={options}
                type="bar"
              />
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
