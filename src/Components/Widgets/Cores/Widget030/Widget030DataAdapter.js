import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget030DefaultDatas {
  static commonDataOption = {
    label: '계열명',
    fill: false,
    lineTension: 0.1,
    backgroundColor: '#0c99c9',
    borderColor: '#0c99c9',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    data: [],
    spanGaps: false,
    type: 'line',
    yAxisID: 'y-axis-2',
    pointStyle: 'line',
  };

  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState, widgetOptions) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.datas = widgetState.chart;
    this.records = this.datas.data;
    this.options = widgetOptions.chart.options;
  }

  getColor(index) {
    return TextRenderer.renderLineHexColor(index);
  }

  getTotalDataSet(color = '#B8DEED', record) {
    const { data, label, id } = record;
    return {
      ...Widget030DefaultDatas.commonDataOption,
      data,
      label,
      borderColor: color,
      backgroundColor: color,
      pointBorderColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: color,
      pointBackgroundColor: color,
      type: 'bar',
      yAxisID: 'y-axis-1',
      id,
      pointStyle: 'rect',
    };
  }

  getPercentageData(total, data) {
    const percentages = _.map(data, (point, index) => {
      const totalValue = total[index];
      return TextRenderer.renderPercentageNumber(point, totalValue);
    });
    return percentages;
  }

  getLineDataSet(totalData, color, record) {
    const { data, label } = record;
    return {
      ...Widget030DefaultDatas.commonDataOption,
      data: this.getPercentageData(totalData, data),
      label,
      borderWidth: 1,
      borderColor: color,
      backgroundColor: color,
      pointBorderColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: color,
      pointBackgroundColor: color,
    };
  }

  get dataSets() {
    const { data: totalData } = _.find(this.records, record => record.id === 'total');
    const dataSets = _.map(this.records, (record, index) => {
      const color = this.getColor(index);
      let targetDataSet = {};
      if (record.id === 'total') {
        targetDataSet = this.getTotalDataSet(undefined, record);
      } else {
        targetDataSet = this.getLineDataSet(totalData, color, record);
      }
      return targetDataSet;
    }) || [];
    return dataSets;
  }

  get defaultData() {
    return {
      chart: {
        data: {
          labels: this.datas.xLabels,
          datasets: this.dataSets,
        },
        options: this.options,
      },
    };
  }
}
