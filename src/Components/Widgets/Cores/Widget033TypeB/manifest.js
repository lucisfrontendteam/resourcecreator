export default {
  id: 'Widget033TypeB',
  stateId: 'Widget033TypeB',
  name: 'Tops 등급별 비율',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/keyterm/widget033TypeB.api',
};
