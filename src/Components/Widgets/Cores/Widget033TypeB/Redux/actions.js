import * as actionTypes from './actionTypes';
import Widget033TypeBSampleDatas from '../Widget033TypeBSampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget033typebInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET033_TYPEB_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget033TypeBSampleDatas,
    };
    AjaxManager.run(param);
  };
}
