import React, { PureComponent } from 'react';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import Widget033TypeBDataAdapter from './Widget033TypeBDataAdapter';
import Widget033TypeBDefaultOptions from './Widget033TypeBDefaultOptions';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget033TypeB extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget033typebInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget033typebInitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter
      = new Widget033TypeBDataAdapter(widgetState, Widget033TypeBDefaultOptions);

    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const { data, type, options } = defaultData.chart;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="widget-chart-13">
              <ChartJsMultiBar data={data} type={type} options={options} />
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
