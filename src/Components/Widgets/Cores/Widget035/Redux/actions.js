import * as actionTypes from './actionTypes';
import Widget035SampleDatas from '../Widget035SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget035InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET035_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget035SampleDatas,
    };
    AjaxManager.run(param);
  };
}
