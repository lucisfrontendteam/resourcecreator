export default {
  id: 'Widget035',
  stateId: 'Widget035',
  name: '상담 유형별 성비',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/category/widget035.api',
};
