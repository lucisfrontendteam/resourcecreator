import React, { PureComponent } from 'react';
import LineBarProxy from '../../../Widgets/Parts/Chart/LineBarProxy';
import Widget009TypeADefaultOptions from './Widget009TypeADefaultOptions';
import Widget009TypeADataAdapter from './Widget009TypeADataAdapter';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget009TypeA extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget009typeaInitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget009typeaInitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget009TypeADataAdapter(
      widgetState,
      Widget009TypeADefaultOptions,
      props.ExtraQueryState,
      props.Widget000State,
    );
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData, chart2Title, chart3Title } = adapter;
      const { chart1, chart2, chart3 } = defaultData;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">트렌드</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="row">
              <div className="col-md-6">
                <div className="lucis-core">
                  <div className="lucis-core-header">
                    <h3 className="lucis-core-title">트렌드 그래프</h3>
                  </div>
                  <div className="lucis-core-body">
                    <div className="widget-chart-02">
                      <LineBarProxy
                        {...props}
                        datum={chart1.data}
                        options={chart1.options}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-6">
                <div className="row">
                  <div className="col-md-12">
                    <div className="lucis-core">
                      <div className="lucis-core-header">
                        <h3 className="lucis-core-title">{chart2Title}</h3>
                      </div>
                      <div className="lucis-core-body">
                        <div className="chart-area">
                          <ChartJsMultiBar
                            data={chart2.data}
                            options={chart2.options}
                            height={85}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12">
                    <div className="lucis-core">
                      <div className="lucis-core-header">
                        <h3 className="lucis-core-title">{chart3Title}</h3>
                      </div>
                      <div className="lucis-core-body">
                        <div className="chart-area">
                          <ChartJsMultiBar
                            data={chart3.data}
                            options={chart3.options}
                            height={85}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
