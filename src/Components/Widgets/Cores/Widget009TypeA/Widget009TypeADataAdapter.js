import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget009TypeADataAdapter {
  static defaultChartJsDataSetOptions = {
    label: '계열명',
    fill: false,
    lineTension: 0.1,
    backgroundColor: '#0c99c9',
    borderColor: '#0c99c9',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    data: [],
    spanGaps: false,
    type: 'line',
    yAxisID: 'y-axis-2',
    pointStyle: 'line',
  };

  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState, options, ExtraQueryState, Widget000State) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    const chart1Empty = _.isEmpty(data.chart1.datas);
    const chart2Empty = _.isEmpty(data.chart2.datas);
    const chart3Empty = _.isEmpty(data.chart3.datas);
    if (chart1Empty || chart2Empty || chart3Empty) {
      this.errors = [`${manifest.apiUrl} 을 점검하여 주십시오.`];
      if (chart1Empty) {
        this.errors.push('트랜드 차트의 데이터가 없습니다.');
      }
      if (chart2Empty) {
        this.errors.push('통화시간 차트의 데이터가 없습니다.');
      }
      if (chart3Empty) {
        this.errors.push('묵음비율 차트의 데이터가 없습니다.');
      }
    }

    this.data = data;
    this.options = options;
    this.ExtraQueryState = ExtraQueryState;
    this.Widget000State = Widget000State || { chart: { data: [] } };
  }

  get availableWidget000Ids() {
    const { data } = this.Widget000State.chart;
    const filtered = _.filter(data, record => record.value > 0);
    const ids = _.map(filtered, record => record.id) || [];
    return ids;
  }

  get chart1OrginData() {
    const { datas: chart1Datas } = this.data.chart1;
    // let newChart1Datas = [];
    // const available = this.availableWidget000Ids;
    // if (_.isEmpty(available)) {
    //   newChart1Datas = { ...chart1Datas };
    // } else {
    //   newChart1Datas
    //     = _.filter(['total', ...chart1Datas], record => _.includes(available, record.id));
    // }
    return chart1Datas;
  }

  get chart1OrginOptions() {
    return this.options.chart1.options;
  }

  get chart2OrginData() {
    const { datas: chart2Datas } = this.data.chart2;
    // let newChart2Datas = [];
    // const available = this.availableWidget000Ids;
    // if (_.isEmpty(available)) {
    //   newChart2Datas = { ...chart2Datas };
    // } else {
    //   newChart2Datas
    //     = _.filter(['total', ...chart2Datas], record => _.includes(available, record.id));
    // }
    return chart2Datas;
  }

  get chart2OrginOptions() {
    return this.options.chart2.options;
  }

  get chart3OrginData() {
    const { datas: chart3Datas } = this.data.chart3;
    // let newChart3Datas = [];
    // const available = this.availableWidget000Ids;
    // if (_.isEmpty(available)) {
    //   newChart3Datas = { ...chart3Datas };
    // } else {
    //   newChart3Datas
    //     = _.filter(['total', ...chart3Datas], record => _.includes(available, record.id));
    // }
    return chart3Datas;
  }

  get chart3OrginOptions() {
    return this.options.chart3.options;
  }

  get CategoryFilterListState() {
    const { CategoryFilterListState = {} } = this.ExtraQueryState;
    return CategoryFilterListState;
  }

  get dummyChartData() {
    return { id: '', key: '' };
  }

  getChartJsTargetChartData(datas) {
    const { id: CategoryId = 'total' } = this.CategoryFilterListState;
    const targetChartData = _.find(datas, record => record.id === CategoryId);
    return targetChartData || this.dummyChartData;
  }

  getChartJsRecordLabel(datas) {
    const targetChartData = this.getChartJsTargetChartData(datas);
    return targetChartData.key;
  }

  getChartJsRecordId(datas) {
    const targetChartData = this.getChartJsTargetChartData(datas);
    return targetChartData.id;
  }

  getChartJsLabels(datas) {
    const targetChartData = this.getChartJsTargetChartData(datas);
    const labels = _.map(targetChartData.values, record => record.x);
    return labels;
  }

  getChartJsValues(datas) {
    const targetChartData = this.getChartJsTargetChartData(datas);
    const values = _.map(targetChartData.values, record => record.y);
    return values;
  }

  getChartJsMaxValue(datas) {
    const { max } = this.getChartJsTargetChartData(datas);
    return max;
  }

  getChartJsMaxValueIndex(datas) {
    const allValues = this.getChartJsValues(datas);
    const maxValue = this.getChartJsMaxValue(datas);
    const maxValueIndex = _.findIndex(allValues, el => el === maxValue);
    return maxValueIndex;
  }

  getChartJsBarColorList(datas, normalColor) {
    const allValues = this.getChartJsValues(datas);
    const colorList = _.map(_.range(allValues.length), () => normalColor);
    const highlightColor = '#FD0B59';
    const maxValueIndex = this.getChartJsMaxValueIndex(datas);
    colorList[maxValueIndex] = highlightColor;
    return colorList;
  }

  getChartJsAvgValue(datas) {
    const { avg } = this.getChartJsTargetChartData(datas);
    return avg;
  }

  getChartJsAvgValues(datas) {
    const avg = this.getChartJsAvgValue(datas);
    const avgValues = _.map([...datas, 'dummy'], () => avg);
    return avgValues;
  }

  get chart2Title() {
    const { chart2OrginData } = this;
    const maxValue
      = TextRenderer.renderLocaleDateDuration(this.getChartJsMaxValue(chart2OrginData));
    const avgValue
      = TextRenderer.renderLocaleDateDuration(this.getChartJsAvgValue(chart2OrginData));
    const title = `통화시간 (최대 ${maxValue}, 평균 ${avgValue})`;
    return title;
  }

  get chart2DataSetRecordId() {
    const { chart2OrginData } = this;
    const recordId = this.getChartJsRecordId(chart2OrginData);
    return recordId;
  }

  get chart2DataSetRecordLabel() {
    const { chart2OrginData } = this;
    const recordLabel = this.getChartJsRecordLabel(chart2OrginData);
    return recordLabel;
  }

  get chart2Labels() {
    const { chart2OrginData } = this;
    const labels = this.getChartJsLabels(chart2OrginData);
    return labels;
  }

  get chart2Values() {
    const { chart2OrginData } = this;
    const values = this.getChartJsValues(chart2OrginData);
    return values;
  }

  get chart2AvgValues() {
    const { chart2OrginData } = this;
    const avgValues = this.getChartJsAvgValues(chart2OrginData);
    return avgValues;
  }

  getChart2BarColorList(normalColor) {
    const { chart2OrginData } = this;
    const colorList = this.getChartJsBarColorList(chart2OrginData, normalColor);
    return colorList;
  }

  get chart2BarDataSet() {
    const colors = TextRenderer.renderWidget009ColorRange();
    const barDataSet = {
      ...Widget009TypeADataAdapter.defaultChartJsDataSetOptions,
      borderColor: colors[0],
      backgroundColor: colors[0],
      pointBorderColor: colors[0],
      pointHoverBackgroundColor: colors[0],
      pointHoverBorderColor: colors[0],
      pointBackgroundColor: colors[0],
      type: 'bar',
      borderWidth: 0,
      yAxisID: 'y-axis-2',
      id: this.chart2DataSetRecordId,
      pointStyle: 'rect',
      label: this.chart2DataSetRecordLabel,
      data: this.chart2Values,
    };
    return barDataSet;
  }

  get chart2LineDataSet() {
    const color = '#E67E22';
    const barDataSet = {
      ...Widget009TypeADataAdapter.defaultChartJsDataSetOptions,
      borderColor: color,
      backgroundColor: color,
      pointBorderColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: color,
      pointBackgroundColor: color,
      borderWidth: 1,
      type: 'line',
      yAxisID: 'y-axis-1',
      id: 'chart2Avg',
      pointStyle: 'line',
      label: `${this.chart2DataSetRecordLabel} 평균`,
      data: this.chart2AvgValues,
    };
    return barDataSet;
  }

  get chart2DataSets() {
    return [this.chart2BarDataSet];
  }

  get chart2Data() {
    const data = {
      labels: this.chart2Labels,
      datasets: this.chart2DataSets,
    };
    return data;
  }

  get chart3Title() {
    const { chart3OrginData } = this;
    const maxValue
      = TextRenderer.renderPercentage(this.getChartJsMaxValue(chart3OrginData));
    const avgValue
      = TextRenderer.renderPercentage(this.getChartJsAvgValue(chart3OrginData));
    const title = `묵음비율 (최대 ${maxValue}, 평균 ${avgValue})`;
    return title;
  }

  get chart3DataSetRecordId() {
    const { chart3OrginData } = this;
    const recordId = this.getChartJsRecordId(chart3OrginData);
    return recordId;
  }

  get chart3DataSetRecordLabel() {
    const { chart3OrginData } = this;
    const recordLabel = this.getChartJsRecordLabel(chart3OrginData);
    return recordLabel;
  }

  get chart3Values() {
    const { chart3OrginData } = this;
    const values = this.getChartJsValues(chart3OrginData);
    return values;
  }

  get chart3AvgValues() {
    const { chart3OrginData } = this;
    const avgValues = this.getChartJsAvgValues(chart3OrginData);
    return avgValues;
  }

  getChart3BarColorList(normalColor) {
    const { chart3OrginData } = this;
    const colorList = this.getChartJsBarColorList(chart3OrginData, normalColor);
    return colorList;
  }

  get chart3BarDataSet() {
    const colors = TextRenderer.renderWidget009ColorRange();
    const barDataSet = {
      ...Widget009TypeADataAdapter.defaultChartJsDataSetOptions,
      borderColor: colors[0],
      backgroundColor: colors[0],
      pointBorderColor: colors[0],
      pointHoverBackgroundColor: colors[0],
      pointHoverBorderColor: colors[0],
      pointBackgroundColor: colors[0],
      type: 'bar',
      borderWidth: 0,
      yAxisID: 'y-axis-2',
      id: this.chart3DataSetRecordId,
      pointStyle: 'rect',
      label: this.chart3DataSetRecordLabel,
      data: this.chart3Values,
    };
    return barDataSet;
  }

  get chart3LineDataSet() {
    const color = '#E67E22';
    const barDataSet = {
      ...Widget009TypeADataAdapter.defaultChartJsDataSetOptions,
      borderColor: color,
      backgroundColor: color,
      pointBorderColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: color,
      pointBackgroundColor: color,
      borderWidth: 1,
      type: 'line',
      yAxisID: 'y-axis-1',
      id: 'chart3Avg',
      pointStyle: 'line',
      label: `${this.chart3DataSetRecordLabel} 평균`,
      data: this.chart3AvgValues,
    };
    return barDataSet;
  }

  get chart3DataSets() {
    return [this.chart3BarDataSet];
  }

  get chart3Labels() {
    const { chart3OrginData } = this;
    const labels = this.getChartJsLabels(chart3OrginData);
    return labels;
  }

  get chart3Data() {
    const data = {
      labels: this.chart3Labels,
      datasets: this.chart3DataSets,
    };
    return data;
  }

  get chart1TotalBar() {
    const totalRecord = _.find(this.chart1OrginData, data => data.id === 'total');
    const { key, values } = totalRecord;
    const totalBar = {
      key,
      bar: true,
      values: _.map(values, field => {
        const { x, y } = field;
        return {
          x: TextRenderer.renderDateObject(x),
          y,
        };
      }),
    };
    return totalBar;
  }

  get chart1Lines() {
    const { id: Category1Id } = this.CategoryFilterListState;
    const lineRecords = _.filter(this.chart1OrginData, data => {
      const { id: lineId } = data;
      const isNotTotal = lineId !== 'total';
      const selected = _.isEmpty(Category1Id) ? true : Category1Id === lineId;
      return isNotTotal && selected;
    });
    const lines = _.map(lineRecords, line => {
      const { key, values } = line;
      const data = {
        key,
        values: _.map(values, field => {
          const { x, y } = field;
          return {
            x: TextRenderer.renderDateObject(x),
            y,
          };
        }),
      };
      return data;
    });
    return lines;
  }

  get chart1Data() {
    const chart1DataList = [
      this.chart1TotalBar,
      ...this.chart1Lines,
    ];
    return chart1DataList;
  }

  get defaultData() {
    const result = {
      chart1: {
        // 트랜드 그래프 차트의 데이터 배열
        data: this.chart1Data,
        options: this.chart1OrginOptions,
      },
      chart2: {
        // 통화시간 차트의 데이터 배열
        data: this.chart2Data,
        options: this.chart2OrginOptions,
      },
      chart3: {
        // 묵음비율 차트의 데이터 배열
        data: this.chart3Data,
        options: this.chart3OrginOptions,
      },
    };
    return result;
  }
}
