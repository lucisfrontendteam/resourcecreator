import TextRenderer from '../../Utils/TextRenderer';

export default {
  chart1: {
    options: {
      svgHeigth: 400,
      chartHeight: 400,
    },
  },
  chart2: {
    options: {
      animation: { duration: 0 },
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { yLabel, datasetIndex } = tooltipItem;
            const { label } = data.datasets[datasetIndex];
            const value = TextRenderer.renderNumberWithGun(yLabel);
            return `${label}: ${value}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
          usePointStyle: true,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          barThickness: 20,
          categoryPercentage: 0.2,
          barPercentage: 0.2,
          ticks: {
            fontColor: '#0c99c9',
            callback(value) {
              return TextRenderer.renderText(value);
            },
          },
          gridLines: {
            offsetGridLines: true,
          },
        }],
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-2',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderNumberWithGun(value);
              },
              // fixedStepSize: 5000,
              // stepSize: 10000,
              maxTicksLimit: 3,
              min: 0,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  },
  chart3: {
    options: {
      animation: { duration: 0 },
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { yLabel, datasetIndex } = tooltipItem;
            const { label } = data.datasets[datasetIndex];
            const value = TextRenderer.renderNumberWithGun(yLabel);
            return `${label}: ${value}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
          usePointStyle: true,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          barThickness: 20,
          categoryPercentage: 0.2,
          barPercentage: 0.2,
          ticks: {
            fontColor: '#0c99c9',
            callback(value) {
              return TextRenderer.renderText(value);
            },
          },
          gridLines: {
            offsetGridLines: true,
          },
        }],
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-2',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderNumberWithGun(value);
              },
              // fixedStepSize: 5000,
              // stepSize: 10000,
              maxTicksLimit: 3,
              min: 0,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  },
};
