/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart1": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "key": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "values": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "x": {
                      "type": "string"
                    },
                    "y": {
                      "type": "number"
                    }
                  },
                  "required": [
                    "x",
                    "y"
                  ]
                }
              }
            },
            "required": [
              "key",
              "id",
              "values"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    },
    "chart2": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "key": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "max": {
                "type": "integer"
              },
              "avg": {
                "type": "integer"
              },
              "values": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "x": {
                      "type": "string"
                    },
                    "y": {
                      "type": "integer"
                    }
                  },
                  "required": [
                    "x",
                    "y"
                  ]
                }
              }
            },
            "required": [
              "key",
              "id",
              "max",
              "avg",
              "values"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    },
    "chart3": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "key": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "max": {
                "type": "number"
              },
              "avg": {
                "type": "number"
              },
              "values": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "x": {
                      "type": "string"
                    },
                    "y": {
                      "type": "integer"
                    }
                  },
                  "required": [
                    "x",
                    "y"
                  ]
                }
              }
            },
            "required": [
              "key",
              "id",
              "max",
              "avg",
              "values"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    }
  },
  "required": [
    "chart1",
    "chart2",
    "chart3"
  ]
};
