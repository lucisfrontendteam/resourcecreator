export default {
  id: 'Widget009TypeA',
  stateId: 'Widget009TypeA',
  name: '상담 유형 트랜드',
  usage: '읽기',
  type: 'Widget',
  sizes: [12],
  method: 'post',
  apiUrl: '/category/widget009TypeA.api',
};
