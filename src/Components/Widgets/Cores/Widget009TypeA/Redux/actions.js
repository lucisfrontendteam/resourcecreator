import * as actionTypes from './actionTypes';
import Widget009TypeASampleDatas from '../Widget009TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget009typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET009_TYPEA_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget009TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
