import * as actionTypes from './actionTypes';
import Widget028SampleDatas from '../Widget028SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget028InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET028_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget028SampleDatas,
    };
    AjaxManager.run(param);
  };
}
