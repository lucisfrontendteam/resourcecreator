/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "chart": {
      "type": "object",
      "properties": {
        "labels": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "data": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "label": {
                "type": "string"
              },
              "id": {
                "type": "string"
              },
              "data": {
                "type": "array",
                "items": {
                  "type": "integer"
                }
              }
            },
            "required": [
              "label",
              "id",
              "data"
            ]
          }
        }
      },
      "required": [
        "labels",
        "data"
      ]
    }
  },
  "required": [
    "chart"
  ]
};
