import TextRenderer from '../../Utils/TextRenderer';

export default {
  chart: {
    options: {
      animation: { duration: 0 },
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { yLabel, datasetIndex } = tooltipItem;
            const { label, id } = data.datasets[datasetIndex];
            let suffix = '%';
            let value = yLabel;
            if (id === 'total') {
              suffix = '건';
              value = TextRenderer.renderNumber(yLabel);
            }
            return `${label}: ${value}${suffix}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
          usePointStyle: true,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          type: 'time',
          time: {
            displayFormats: {
              day: 'YYYY.MM.DD',
            },
          },
          barThickness: 10,
          categoryPercentage: 0.2,
          barPercentage: 0.2,
          ticks: {
            fontColor: '#0c99c9',
          },
          gridLines: {
            offsetGridLines: true,
          },
        }],
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-1',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderNumber(value);
              },
              fixedStepSize: 100,
              stepSize: 1000,
              maxTicksLimit: 10,
            },
          }, {
            type: 'linear',
            display: true,
            position: 'right',
            id: 'y-axis-2',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderPercentage(value / 100);
              },
              maxTicksLimit: 10,
              stepSize: 10,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  },
};
