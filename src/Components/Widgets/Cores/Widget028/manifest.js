export default {
  id: 'Widget028',
  stateId: 'Widget028',
  name: '일별 트렌드',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/volume/widget028.api',
};
