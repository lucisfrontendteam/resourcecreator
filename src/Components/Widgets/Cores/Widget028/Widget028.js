import React, { PureComponent } from 'react';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import Widget028DefaultOptions from './Widget028DefaultOptions';
import Widget028DataAdapter from './Widget028DataAdapter';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget028 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget028InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget028InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter =
      new Widget028DataAdapter(widgetState, Widget028DefaultOptions);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const { data, options } = defaultData.chart;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
          </div>
          <div className="lucis-widget-body">
            <div className="widget-chart-13">
              <ChartJsMultiBar
                data={data}
                options={options}
                height={210}
              />
            </div>
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
