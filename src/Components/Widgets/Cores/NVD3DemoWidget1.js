import React, { PureComponent } from 'react';
import LineBarProxy from '../Parts/Chart/LineBarProxy';
import MultiBar from '../Parts/Chart/MultiBar';

export class NVD3DemoWidget1 extends PureComponent {
  static defaultProps = {
    id: 'NVD3DemoWidget1',
    name: 'NVD3 데모 1',
    type: 'Widget',
    sizes: [6],
  };

  render() {
    const { props } = this;

    return (
      <div>
        <LineBarProxy {...props} />
        <MultiBar {...props} />
      </div>
    );
  }
}
