export default {
  id: 'Widget034TypeB',
  stateId: 'Widget034TypeB',
  name: '고객 연령대 비율',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/keyterm/widget034TypeB.api',
};
