import TextRenderer from '../../Utils/TextRenderer';

export default {
  chart: {
    options: {
      animation: { duration: 0 },
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { xLabel, datasetIndex } = tooltipItem;
            const { label } = data.datasets[datasetIndex];
            const value = TextRenderer.renderNumberWithGun(xLabel);
            return `${label}: ${value}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          // borderColor: '#0c99c9',
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          stacked: true,
          display: true,
          scaleLabel: {
            show: true,
          },
          ticks: {
            fontColor: '#0c99c9',
          },
        }],
        yAxes: [{
          stacked: true,
          display: true,
          scaleLabel: {
            show: true,
          },
          ticks: {
            fontColor: '#0c99c9',
            fixedStepSize: 100,
            stepSize: 1000,
            maxTicksLimit: 8,
          },
        }],
      },
    },
  },
};
