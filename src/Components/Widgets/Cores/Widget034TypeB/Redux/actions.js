import * as actionTypes from './actionTypes';
import Widget034TypeBSampleDatas from '../Widget034TypeBSampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget034typebInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET034_TYPEB_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget034TypeBSampleDatas,
    };
    AjaxManager.run(param);
  };
}
