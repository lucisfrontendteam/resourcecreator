import * as actionTypes from './actionTypes';

export function Widget034TypeBState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET034_TYPEB_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
