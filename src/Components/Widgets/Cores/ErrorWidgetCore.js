import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import _ from 'lodash';

export class ErrorWidgetCore extends PureComponent {
  static defaultProps = {
    id: 'ErrorWidgetCore',
    name: '에러 위젯',
    type: 'Widget',
    sizes: [4, 6, 12],
    message: ['위젯을 렌더링할 수 없습니다.'],
  };

  renderError(messages) {
    // eslint-disable-next-line
    return _.map(messages, message => {
      return (
        <p key={v4()} className="loading-msg-s">
          {message}
        </p>
      );
    });
  }

  render() {
    const { props } = this;
    const { strong1, strong2, message, showNow } = props;
    const style = showNow ? {} : { display: 'none' };
    return (
      <section className="content-inner" style={style}>
        <div className="row">
          <div className="col-md-12">
            <div className="error-wrap">
              <div className="error-in">
                <table className="error-con">
                  <tbody>
                    <tr>
                      <td>
                        <p className="loading-msg">
                          <strong>{strong1}</strong>{strong2}
                        </p>
                        {this.renderError(message)}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
