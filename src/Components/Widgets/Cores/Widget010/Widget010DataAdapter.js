import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError, dataEmpty } from '../../Utils/WidgetConstants';

export default class Widget010DataAdapter {
  static commonDataOption = {
    label: '계열명',
    fill: false,
    lineTension: 0.1,
    backgroundColor: '#0c99c9',
    borderColor: '#0c99c9',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    pointBorderColor: '#0c99c9',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: '#0c99c9',
    pointHoverBorderColor: '#0c99c9',
    pointHoverBorderWidth: 2,
    pointRadius: 1,
    pointHitRadius: 10,
    data: [],
    spanGaps: true,
    type: 'bar',
    yAxisID: 'y-axis-1',
  };

  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(datas = this.emptyState, options) {
    const result = validate(datas, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    } else if (_.isEmpty(datas.grid.datas) || _.isEmpty(datas.chart.datas)) {
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        '표시할 데이터가 없습니다.',
      ];
      this.errorType = dataEmpty;
    }
    this.datas = datas;
    this.options = options;
  }

  get gridDatas() {
    const datas = _.map(this.datas.grid.datas, data => {
      const { records } = data;
      const formattedRecords = _.map(records, record => {
        const {
          no,
          telDateTime,
          counselor,
          teltimes,
          counselingType,
        } = record;
        return {
          no,
          telDateTime: TextRenderer.renderDateTimeString(telDateTime),
          counselor,
          teltimes: TextRenderer.renderLocaleDateDuration(teltimes),
          counselingType,
        };
      });
      return {
        ...data,
        records: formattedRecords,
      };
    });
    return datas;
  }

  get gridOptions() {
    return this.options.grid.options;
  }

  get chartOptions() {
    return this.options.chart.options;
  }

  getData(record) {
    const datas = _.map(record.values, value => value.y);
    return datas;
  }

  getLabels(record) {
    const labels = _.map(record.values, value => value.x);
    return labels;
  }

  // from http://bl.ocks.org/benvandyke/8459843
  getLeastSquares(labels, data) {
    const xSeries = _.range(1, labels.length + 1);
    const ySeries = _.map(data, d => parseFloat(d));

    const reduceSumFunc = (prev, cur) => prev + cur;

    const xBar = _.reduce(xSeries, reduceSumFunc) * 1.0 / xSeries.length;
    const yBar = _.reduce(ySeries, reduceSumFunc) * 1.0 / ySeries.length;

    const ssXX = _.reduce(_.map(xSeries, d => Math.pow(d - xBar, 2)), reduceSumFunc);
    const ssYY = _.reduce(_.map(ySeries, d => Math.pow(d - yBar, 2)), reduceSumFunc);

    const ssXY =
      _.reduce(_.map(xSeries, (d, i) => (d - xBar) * (ySeries[i] - yBar)), reduceSumFunc);

    const slope = ssXY / ssXX;
    const intercept = yBar - (xBar * slope);
    const rSquare = Math.pow(ssXY, 2) / (ssXX * ssYY);
    return [slope, intercept, rSquare];
  }

  getTrendLineData(labels, data) {
    const leastSquaresCoeff = this.getLeastSquares(labels, data);
    const y1 = leastSquaresCoeff[0] + leastSquaresCoeff[1];
    const y2 = leastSquaresCoeff[0] * labels.length + leastSquaresCoeff[1];
    const trendData = _.map(labels, (label, index) => {
      let value = undefined;
      if (index === 0) {
        value = y1.toFixed(2);
      } else if (index === labels.length - 1) {
        value = y2.toFixed(2);
      } else {
        value = undefined;
      }
      if (value < 0) {
        value = 0;
      }
      return value;
    });
    return trendData;
  }

  getBarColorList(record, recordIndex) {
    const { values } = record;
    const barColor = TextRenderer.renderHexColor(recordIndex);
    const lastBarColor = '#FD0B59';
    const colorList = _.map(values, (value, index) => {
      let targetColor = barColor;
      if (values.length === index + 1) {
        targetColor = lastBarColor;
      }
      return targetColor;
    });
    return colorList;
  }

  get chartRecords() {
    const { datas: chartDatas } = this.datas.chart;
    const chartRecords = _.map(chartDatas, (chartData, index) => {
      const {
        records,
        categoryId,
        category: label,
      } = chartData;
      let dataSet = {};
      if (records) {
        const record = records[0];
        const data = this.getData(record);
        const labels = this.getLabels(record);
        const barColorList = this.getBarColorList(record, index);
        const lineColor = TextRenderer.renderLineHexColor(index);
        dataSet = {
          labels,
          categoryId,
          datasets: [
            {
              ...Widget010DataAdapter.commonDataOption,
              label,
              data: this.getTrendLineData(labels, data),
              borderColor: lineColor,
              borderWidth: 1,
              backgroundColor: lineColor,
              pointBorderColor: lineColor,
              pointHoverBackgroundColor: lineColor,
              pointHoverBorderColor: lineColor,
              pointBackgroundColor: lineColor,
              type: 'line',
              yAxisID: 'y-axis-1',
            },
            {
              ...Widget010DataAdapter.commonDataOption,
              label,
              data,
              borderColor: barColorList,
              backgroundColor: barColorList,
              pointBorderColor: barColorList,
              pointHoverBackgroundColor: barColorList,
              pointHoverBorderColor: barColorList,
              pointBackgroundColor: barColorList,
            },
          ],
        };
      }
      return dataSet;
    });
    return chartRecords;
  }

  get defaultData() {
    const {
      gridDatas,
      gridOptions,
      chartOptions,
      chartRecords,
    } = this;
    return {
      grid: {
        datas: gridDatas,
        options: gridOptions,
      },
      chart: {
        datas: chartRecords,
        options: chartOptions,
      },
    };
  }
}
