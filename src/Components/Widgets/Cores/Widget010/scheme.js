/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "category": {
                "type": "string"
              },
              "totalCount": {
                "type": "integer"
              },
              "categoryId": {
                "type": "string"
              },
              "records": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "no": {
                      "type": "integer"
                    },
                    "keywords": {
                      "type": "string"
                    },
                    "telDateTime": {
                      "type": "string"
                    },
                    "counselor": {
                      "type": "string"
                    },
                    "teltimes": {
                      "type": "integer"
                    },
                    "counselingType": {
                      "type": "string"
                    },
                    "extensionNumber": {
                      "type": "integer"
                    }
                  },
                  "required": [
                    "no",
                    "keywords",
                    "telDateTime",
                    "counselor",
                    "teltimes",
                    "counselingType",
                    "extensionNumber"
                  ]
                }
              }
            },
            "required": [
              "category",
              // "totalCount",
              "categoryId",
              "records"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    },
    "chart": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "category": {
                "type": "string"
              },
              "categoryId": {
                "type": "string"
              },
              "records": {
                "type": "array",
                "items": {
                  "type": "object",
                  "properties": {
                    "key": {
                      "type": "string"
                    },
                    "values": {
                      "type": "array",
                      "items": {
                        "type": "object",
                        "properties": {
                          "x": {
                            "type": "string"
                          },
                          "y": {
                            "type": "integer"
                          }
                        },
                        "required": [
                          "x",
                          "y"
                        ]
                      }
                    }
                  },
                  "required": [
                    "key",
                    "values"
                  ]
                }
              }
            },
            "required": [
              "category",
              "categoryId",
              "records"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    }
  },
  "required": [
    "grid",
    "chart"
  ]
};
