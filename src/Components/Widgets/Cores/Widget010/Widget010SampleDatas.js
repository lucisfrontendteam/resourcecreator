/**
 * 위젯 식별자: WIDGET010
 * 위젯 이름: VOC 검출 목록 및 일별 트랜드
 */
export default {
  grid: {
    // VOC현황 그리드의 데이터
    datas: [{
      totalCount: 100,
      category: '언론 보도',
      // 카테고리 식별자
      categoryId: '1',
      records: [{
        // 순위
        no: 1,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 2,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 3,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 4,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }],
    },
    {
      category: '고소 / 고발',
      totalCount: 100,
      // 카테고리 식별자
      categoryId: '2',
      records: [{
        // 순위
        no: 1,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 2,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 3,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 4,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }],
    },
    {
      category: '불만 / 분쟁',
      totalCount: 100,
      // 카테고리 식별자
      categoryId: '3',
      records: [{
        // 순위
        no: 1,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 2,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 3,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 4,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }],
    },
    {
      category: '욕설',
      totalCount: 100,
      // 카테고리 식별자
      categoryId: '4',
      records: [{
        // 순위
        no: 1,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 2,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 3,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913093021',
        // 상담사
        counselor: '김성원',
        // 통화시간 (단위 초)
        teltimes: 333,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }, {
        // 순위
        no: 4,
        // 검출 키워드
        keywords: '중앙일보1,조선일보1,뿌릴거야1',
        // 통화 일시
        telDateTime: '20160913103021',
        // 상담사
        counselor: '김용현',
        // 통화시간 (단위 초)
        teltimes: 444,
        // 상담 유형
        counselingType: '외환',
        // 내선번호
        extensionNumber: 3850,
      }],
    }],
  },
  chart: {
    datas: [
      {
        category: '언론 보도',
        // 카테고리 식별자
        categoryId: '1',
        // 해당 카테고리의 상담건수 리스트 (상담일 오름차순)
        records: [
          {
            key: '상담 건수',
            values: [
              {
                // 상담일
                x: '20160802',
                // 상담 건수
                y: 600,
              },
              {
                x: '20160803',
                y: 199,
              },
              {
                x: '20160804',
                y: 484,
              },
              {
                x: '20160805',
                y: 464,
              },
              {
                x: '20160806',
                y: 450,
              },
              {
                x: '20160807',
                y: 231,
              },
              {
                x: '20160808',
                y: 491,
              },
              {
                x: '20160809',
                y: 219,
              },
              {
                x: '20160810',
                y: 458,
              },
              {
                x: '20160811',
                y: 237,
              },
            ],
          },
        ],
      },
      {
        category: '고소 / 고발',
        // 카테고리 식별자
        categoryId: '2',
        // 해당 카테고리의 상담건수 리스트 (상담일 오름차순)
        records: [
          {
            key: '상담 건수',
            values: [
              {
                // 상담일
                x: '20160802',
                // 상담 건수
                y: 600,
              },
              {
                x: '20160803',
                y: 199,
              },
              {
                x: '20160804',
                y: 484,
              },
              {
                x: '20160805',
                y: 464,
              },
              {
                x: '20160806',
                y: 450,
              },
              {
                x: '20160807',
                y: 231,
              },
              {
                x: '20160808',
                y: 491,
              },
              {
                x: '20160809',
                y: 219,
              },
              {
                x: '20160810',
                y: 458,
              },
              {
                x: '20160811',
                y: 237,
              },
            ],
          },
        ],
      },
      {
        category: '불만 / 분쟁',
        // 카테고리 식별자
        categoryId: '3',
        // 해당 카테고리의 상담건수 리스트 (상담일 오름차순)
        records: [
          {
            key: '상담 건수',
            values: [
              {
                // 상담일
                x: '20160802',
                // 상담 건수
                y: 600,
              },
              {
                x: '20160803',
                y: 199,
              },
              {
                x: '20160804',
                y: 484,
              },
              {
                x: '20160805',
                y: 464,
              },
              {
                x: '20160806',
                y: 450,
              },
              {
                x: '20160807',
                y: 231,
              },
              {
                x: '20160808',
                y: 491,
              },
              {
                x: '20160809',
                y: 219,
              },
              {
                x: '20160810',
                y: 458,
              },
              {
                x: '20160811',
                y: 237,
              },
            ],
          },
        ],
      },
      {
        category: '욕설',
        // 카테고리 식별자
        categoryId: '4',
        // 해당 카테고리의 상담건수 리스트 (상담일 오름차순)
        records: [
          {
            key: '상담 건수',
            values: [
              {
                // 상담일
                x: '20160802',
                // 상담 건수
                y: 600,
              },
              {
                x: '20160803',
                y: 199,
              },
              {
                x: '20160804',
                y: 484,
              },
              {
                x: '20160805',
                y: 464,
              },
              {
                x: '20160806',
                y: 450,
              },
              {
                x: '20160807',
                y: 231,
              },
              {
                x: '20160808',
                y: 491,
              },
              {
                x: '20160809',
                y: 219,
              },
              {
                x: '20160810',
                y: 458,
              },
              {
                x: '20160811',
                y: 237,
              },
            ],
          },
        ],
      },
    ],
  },
};
