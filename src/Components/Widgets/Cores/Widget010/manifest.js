export default {
  id: 'Widget010',
  stateId: 'Widget010',
  name: '벨류 콜 현황',
  usage: '읽기',
  type: 'Widget',
  sizes: [12],
  method: 'post',
  apiUrl: '/category/widget010.api',
};
