import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import ChartJsMultiBar from '../../../../Components/Widgets/Parts/Chart/ChartJsMultiBar';
import _ from 'lodash';
import Widget010DefaultOptions from './Widget010DefaultOptions';
import Widget010DataAdapter from './Widget010DataAdapter';
import manifest from './manifest';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget010 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget010InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget010InitAction(props);
    }
  }

  renderVOCRecords(widgetState) {
    const { props } = this;
    const adapter = new Widget010DataAdapter(
      widgetState,
      Widget010DefaultOptions,
    );
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = _.map(defaultData.grid.datas, gridData => {
        const {
          category,
          categoryId,
          records: gridRecords,
        } = gridData;

        const dataSet = _.find(defaultData.chart.datas,
          chartData => chartData.categoryId === categoryId);

        return (
          <div className="row" key={v4()}>
            <div className="col-md-12">
              <div className="lucis-core">
                <div className="lucis-core-header">
                  <h3 className="lucis-core-title">{category}</h3>
                </div>
                <div className="lucis-core-body">
                  <div className="row">
                    <div className="col-md-6">
                      <BasicDataTable
                        {...props}
                        options={defaultData.grid.options}
                        datas={gridRecords}
                      />
                    </div>
                    <div className="col-md-6">
                      <div className="widget-chart-03">
                        <ChartJsMultiBar
                          {...props}
                          type="bar"
                          height={80}
                          options={defaultData.chart.options}
                          data={dataSet}
                          id={v4()}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      });
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    return (
      <div className="lucis-widget">
        <div className="lucis-widget-header">
          <h2 className="lucis-widget-title">{props.name}</h2>
        </div>
        <div className="lucis-widget-body">
          {this.renderVOCRecords(widgetState)}
        </div>
      </div>
    );
  }
}
