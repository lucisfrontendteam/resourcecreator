import * as actionTypes from './actionTypes';
import Widget010SampleDatas from '../Widget010SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget010InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET010_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget010SampleDatas,
    };
    AjaxManager.run(param);
  };
}
