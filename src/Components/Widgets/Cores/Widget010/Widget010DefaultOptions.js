import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      scrollY: 120,
      scrollCollapse: true,
      columnDefs: [
        {
          title: 'No',
          targets: 0,
          width: '34px',
          render: TextRenderer.renderText,
        },
        {
          title: '통화 일시',
          targets: 1,
          width: 'auto',
          render: TextRenderer.renderText,
        },
        {
          title: '상담사',
          targets: 2,
          width: 'auto',
          render: TextRenderer.renderText,
        },
        {
          title: '통화시간',
          targets: 3,
          width: 'auto',
          render: TextRenderer.renderText,
        },
        {
          title: '소속',
          targets: 4,
          width: 'auto',
          render: TextRenderer.renderText,
        },
      ],
    },
  },
  chart: {
    options: {
      animation: { duration: 0 },
      elements: {
        rectangle: {
          borderWidth: 2,
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        display: false,
        position: 'top',
        labels: {
          boxWidth: 12,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          barThickness: 10,
          categoryPercentage: 0.2,
          barPercentage: 0.1,
          ticks: {
            fontColor: '#0c99c9',
            // labelOffset: 30,
            padding: 0,
            fontSize: 10,
            callback(value) {
              return TextRenderer.renderDateTimeString(value, 'YYYYMMDDhmmss', 'YYYY.MM.DD');
            },
          },
          gridLines: {
            offsetGridLines: true,
          },
        }],
        yAxes: [
          {
            type: 'linear',
            display: true,
            position: 'left',
            id: 'y-axis-1',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderNumber(value);
              },
              fixedStepSize: 100,
              stepSize: 1000,
              min: 0,
              maxTicksLimit: 3,
            },
          },
          {
            type: 'linear',
            display: false,
            position: 'right',
            id: 'y-axis-2',
            ticks: {
              fontColor: '#0c99c9',
              callback(value) {
                return TextRenderer.renderNumber(value);
              },
              fixedStepSize: 100,
              stepSize: 1000,
              min: 0,
              maxTicksLimit: 3,
            },
            gridLines: {
              drawOnChartArea: false,
            },
          },
        ],
      },
    },
  },
};
