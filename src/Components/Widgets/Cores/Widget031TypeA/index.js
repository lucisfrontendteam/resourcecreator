import * as actions from './Redux/actions';
import * as reducer from './Redux/reducer';
import * as actionTypes from './Redux/actionTypes';
export {
  actions as Widget031TypeAActions,
  reducer as Widget031TypeAReducer,
  actionTypes as Widget031TypeAActionTypes,
};
import manifest from './manifest';
export { manifest as Widget031TypeAManifest };
import Widget031TypeASampleDatas from './Widget031TypeASampleDatas';
export { Widget031TypeASampleDatas };
export { Widget031TypeA } from './Widget031TypeA';
