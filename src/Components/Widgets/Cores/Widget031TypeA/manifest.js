export default {
  id: 'Widget031TypeA',
  stateId: 'Widget031TypeA',
  name: '그룹별 통화량',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/organize/widget031TypeA.api',
};
