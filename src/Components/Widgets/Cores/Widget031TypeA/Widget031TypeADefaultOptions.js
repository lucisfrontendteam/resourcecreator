import Chart from 'chart.js';
import TextRenderer from '../../Utils/TextRenderer';

export default {
  chart: {
    options: {
      tooltips: {
        mode: 'label',
        callbacks: {
          label(tooltipItem, data) {
            const { yLabel, datasetIndex } = tooltipItem;
            const { label } = data.datasets[datasetIndex];
            const suffix = '건';
            const value = TextRenderer.renderNumber(yLabel);
            return `${label}: ${value}${suffix}`;
          },
        },
      },
      elements: {
        rectangle: {
          borderWidth: 2,
          // borderColor: '#0c99c9',
          borderSkipped: 'bottom',
        },
      },
      responsive: true,
      legend: {
        position: 'top',
        labels: {
          boxWidth: 12,
        },
      },
      title: {
        display: false,
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            show: true,
          },
          ticks: {
            fontColor: '#0c99c9',
          },
        }],
        yAxes: [{
          display: true,
          scaleLabel: {
            show: true,
          },
          ticks: {
            fontColor: '#0c99c9',
            fixedStepSize: 100,
            stepSize: 1000,
            maxTicksLimit: 10,
          },
        }],
      },
      animation: {
        duration: 0.001,
        onComplete() {
          const self = this;
          const chartInstance = self.chart;
          const ctx = chartInstance.ctx;
          ctx.textAlign = 'center';

          Chart.helpers.each(this.data.datasets.forEach((dataset, i) => {
            const meta = chartInstance.controller.getDatasetMeta(i);
            Chart.helpers.each(meta.data.forEach((bar, index) => {
              ctx.fillStyle = '#285366';
              // eslint-disable-next-line
              ctx.fillText(dataset.data[index], bar._model.x, bar._model.y - 15);
            }), this);
          }), this);
        },
      },
    },
  },
};
