import * as actionTypes from './actionTypes';

export function Widget031TypeAState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET031_TYPEA_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
