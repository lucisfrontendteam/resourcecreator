import * as actionTypes from './actionTypes';
import Widget031TypeASampleDatas from '../Widget031TypeASampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget031typeaInitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET031_TYPEA_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget031TypeASampleDatas,
    };
    AjaxManager.run(param);
  };
}
