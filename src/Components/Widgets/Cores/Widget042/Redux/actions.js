import * as actionTypes from './actionTypes';
import Widget042SampleDatas from '../Widget042SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget042InitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryStateManagerState, QueryIdMap } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET042_INIT,
      query: {
        ...QueryStateManagerState,
        ...QueryIdMap,
      },
      ajaxType: 'normal',
      sampleData: Widget042SampleDatas,
    };
    AjaxManager.run(param);
  };
}
