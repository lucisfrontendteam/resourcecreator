/* eslint-disable */
/**
 * 위젯 식별자: WIDGET042
 * 위젯 이름: 상담 유형별 장시간 콜 분석
 */
export default {
  grid: {
    totalCount: 1000,
    // 상담유형별 묵음비율 분석 그리드의 데이터 배열
    /*
     카테고리명,
     구분값이 차지하는 통화시간(단위는 초),
     1분 미만 통화 건수,
     1분 ~ 3분 미만 통화 건수,
     3분 ~ 5분 미만 통화 건수,
     5분 ~ 7분 미만 통화 건수,
     7분 ~ 10분 미만 통화 건수,
     10분 ~ 15분 미만 통화 건수,
     15분 ~ 20분 미만 통화 건수,
     20분 ~ 30분 미만 통화 건수,
     30분 ~ 50분 미만 통화 건수,
     50분 이상 통화 건수,
    */
    datas: [
      // 컬럼별 평균값
      {
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            CategoryFilterListState: {
              id: '0',
              name: "평균"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '1'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '2'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '3'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '4'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '5'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '6'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '7'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '8'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '9'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },{
        record: [
          {
            value: {
              TargetTelTimeFilterStateId: '',
              name: '10'
            }
          },
          {
            CategoryFilterListState: {
              id: 71,
              name: "[수신] 점하나를 조회"
            },
            value: {
              TargetTelTimeFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: 'avg',
              name: '1890'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '1',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '2',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '3',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '4',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '5',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '6',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '7',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '8',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '9',
              name: '123'
            }
          },
          {
            value: {
              TargetTelTimeFilterStateId: '10',
              name: '123'
            }
          },
        ],
      },
    ],
  },
};
