import * as actions from './Redux/actions';
import * as reducer from './Redux/reducer';
import * as actionTypes from './Redux/actionTypes';
export {
  actions as Widget042Actions,
  reducer as Widget042Reducer,
  actionTypes as Widget042ActionTypes,
};
import manifest from './manifest';
export { manifest as Widget042Manifest };
import Widget042SampleDatas from './Widget042SampleDatas';
export { Widget042SampleDatas };
export { Widget042 } from './Widget042';
