export default {
  id: 'Widget042',
  stateId: 'Widget042',
  name: '상담 유형별 장시간 콜 분석',
  usage: '읽기',
  sizes: [12],
  method: 'post',
  recordCount: 25,
  type: 'IdList',
  inputIdList: [
    {
      key: 'pageNumber',
      id: 'Widget042pageNumber',
    },
    {
      key: 'pageSize',
      id: 'Widget042pageSize',
    },
  ],
  apiUrl: '/categoryorg/widget042.api',
  excelUrl: '/categoryorg/widget042.excel',
};
