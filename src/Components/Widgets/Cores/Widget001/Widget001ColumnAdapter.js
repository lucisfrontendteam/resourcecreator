import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';

export default class Widget001ColumnAdapter {
  constructor(labels = [], renderer = () => {}) {
    this.labels = labels;
    this.renderer = renderer;
  }

  get columnDefs() {
    const { labels, renderer } = this;
    const columnDefs = _.map(labels, (label, index) => {
      const newIndex = index + 1;
      return {
        title: label,
        targets: newIndex,
        render: renderer,
      };
    });
    columnDefs.push({
      title: '카테고리',
      targets: 0,
      render: TextRenderer.renderText,
    });
    return columnDefs;
  }
}
