import DataTableDataAdapter from '../../Utils/DataTableDataAdapter';
import Widget001ColumnAdapter from './Widget001ColumnAdapter';
import TextRenderer from '../../Utils/TextRenderer';
import _ from 'lodash';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget001DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(data = this.emptyState, options) {
    const result = validate(data, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.datas = data;
    this.options = options;
  }

  get grid1Datas() {
    return this.datas.grid1.datas.map(record => {
      const { category, tels, teltimes } = record;
      return {
        // 카테고리
        category,
        // 통화량 (전일비)
        telsCompareWithYesterday: new DataTableDataAdapter(tels).getComparedValueWithYesterDay(),
        // 평균 통화시간
        teltimes: new DataTableDataAdapter(teltimes).getAverage(),
        // 일별 통화량
        tels,
      };
    });
  }

  getGridDatas(gridDatas) {
    const { datas, labels } = gridDatas;
    const records = _.map(datas, data => {
      // eslint-disable-next-line
      const fields = { '카테고리': data.keyword };
      _.each(data.values, (value, index) => {
        fields[labels[index]] = value;
      });
      return fields;
    });
    return records;
  }

  get grid2Datas() {
    return this.getGridDatas(this.datas.grid2);
  }

  get grid1Options() {
    return this.options.grid1.options;
  }

  getGridOptions(labels, otherFieldRenderer) {
    const { columnDefs } = new Widget001ColumnAdapter(labels, otherFieldRenderer);
    return {
      columnDefs,
    };
  }

  get grid2Options() {
    const { options } = this.options.grid2;
    const columnOptions
      = this.getGridOptions(this.datas.grid2.labels, TextRenderer.renderLocaleDateDuration);
    const grid2Options = {
      ...options,
      ...columnOptions,
    };
    return grid2Options;
  }

  get defaultData() {
    return {
      grid1: {
        datas: this.grid1Datas,
        options: this.grid1Options,
      },
      grid2: {
        datas: this.grid2Datas,
        options: this.grid2Options,
      },
    };
  }
}
