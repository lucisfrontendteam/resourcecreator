export default {
  id: 'Widget001',
  stateId: 'Widget001',
  name: '상담품질 지표',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/category/widget001.api',
  gridFieldOrder: [
    'keyword',
    'tels',
    'teltimes',
  ],
};
