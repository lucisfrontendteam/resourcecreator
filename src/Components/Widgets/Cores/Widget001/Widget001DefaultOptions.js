import TextRenderer from '../../Utils/TextRenderer';
import DataTableCallbacks from '../../Utils/DataTableCallbacks';

export default {
  grid1: {
    options: {
      columnDefs: [
        {
          title: '카테고리',
          targets: 0,
          width: '100px',
        },
        {
          title: '통화량(전일비)',
          targets: 1,
          width: '100px',
          render: TextRenderer.renderCompareWithYesterday,
        },
        {
          title: '평균 통화시간',
          targets: 2,
          width: 'auto',
          render: TextRenderer.renderLocaleDateDuration,
        },
        {
          title: '변화추이',
          targets: 3,
          width: '80px',
          className: 'sparkline',
        },
      ],
      initComplete: DataTableCallbacks.initSparklinesWhenDataTableComplate,
    },
  },
  grid2: {
    options: {
      scrollY: 125,
      scrollX: true,
      fixedColumns: {
        leftColumns: 1,
      },
    },
  },
};
