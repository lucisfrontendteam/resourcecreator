/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "object",
  "properties": {
    "grid1": {
      "type": "object",
      "properties": {
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "category": {
                "type": "string"
              },
              "tels": {
                "type": "array",
                "items": {
                  "type": "integer"
                }
              },
              "teltimes": {
                "type": "array",
                "items": {
                  "type": "integer"
                }
              }
            },
            "required": [
              "category",
              "tels",
              "teltimes"
            ]
          }
        }
      },
      "required": [
        "datas"
      ]
    },
    "grid2": {
      "type": "object",
      "properties": {
        "labels": {
          "type": "array",
          "items": {
            "type": "string"
          }
        },
        "datas": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "keyword": {
                "type": "string"
              },
              "values": {
                "type": "array",
                "items": {
                  "type": "number"
                }
              }
            },
            "required": [
              "keyword",
              "values"
            ]
          }
        }
      },
      "required": [
        "labels",
        "datas"
      ]
    }
  },
  "required": [
    "grid1",
    "grid2"
  ]
}