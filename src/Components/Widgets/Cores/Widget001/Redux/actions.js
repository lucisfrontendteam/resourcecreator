import * as actionTypes from './actionTypes';
import Widget001SampleDatas from '../Widget001SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget001InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET001_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget001SampleDatas,
    };
    AjaxManager.run(param);
  };
}
