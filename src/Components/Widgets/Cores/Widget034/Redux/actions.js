import * as actionTypes from './actionTypes';
import Widget034SampleDatas from '../Widget034SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget034InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET034_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget034SampleDatas,
    };
    AjaxManager.run(param);
  };
}
