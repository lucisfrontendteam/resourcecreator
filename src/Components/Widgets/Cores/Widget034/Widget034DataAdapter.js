import _ from 'lodash';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget034DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState) {
    this.widgetState = widgetState;
    const result = validate(this.widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
  }

  get datas() {
    const { chart } = this.widgetState;
    let datas = [];
    if (chart && chart.data) {
      datas = chart.data;
    }
    return datas;
  }

  get data() {
    return this.datas;
  }

  get defaultData() {
    return {
      chart: {
        data: this.data,
      },
    };
  }
}
