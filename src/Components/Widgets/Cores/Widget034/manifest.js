export default {
  id: 'Widget034',
  stateId: 'Widget034',
  name: '고객 연령대 비율',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/keyterm/widget034.api',
};
