import React, { PureComponent } from 'react';
import Widget034DataAdapter from './Widget034DataAdapter';
import MultiProgressBar2 from '../../../Widgets/Parts/Chart/MultiProgressBar2';
import manifest from './manifest';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';
import WidgetRenderer from '../../Utils/WidgetRenderer';

export class Widget034 extends PureComponent {
  static defaultProps = {
    ...manifest,
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.render = props.render.bind(this);
  }

  componentDidUpdate() {
    const { props } = this;
    props.widget034InitAction(props);
  }

  componentDidMount() {
    const { props } = this;
    if (props.isTestMode === 'true') {
      props.widget034InitAction(props);
    }
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const adapter = new Widget034DataAdapter(widgetState);
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
            <ul className="widget-head-label">
              <li className="blue"><span></span>남</li>
              <li className="red"><span></span>여</li>
            </ul>
          </div>
          <div className="lucis-widget-body">
            <MultiProgressBar2 data={defaultData.chart.data} />
          </div>
        </div>
      );
    } else if (WidgetDataComparator.dataEmpty(adapter)) {
      core = WidgetRenderer.renderDataEmptyWidgetCore();
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
