/**
 * 위젯 식별자: WIDGET034
 * 위젯 이름: 고객 연령 비율
 */
export default {
  chart: {
    // Tops 등급별 횟수
    data: [
      {
        // 카테고리 이름
        categoryName: '10대 이하',
        // 카테고리의 남성 횟수
        leftValue: 50,
        // 카테고리의 여성 횟수
        rightValue: 50,
      },
      {
        // 카테고리 이름
        categoryName: '10대',
        // 카테고리의 남성 횟수
        leftValue: 70,
        // 카테고리의 여성 횟수
        rightValue: 30,
      },
      {
        // 카테고리 이름
        categoryName: '20대',
        // 카테고리의 남성 횟수
        leftValue: 40,
        // 카테고리의 여성 횟수
        rightValue: 60,
      },
      {
        // 카테고리 이름
        categoryName: '30대',
        // 카테고리의 남성 횟수
        leftValue: 20,
        // 카테고리의 여성 횟수
        rightValue: 80,
      },
      {
        // 카테고리 이름
        categoryName: '40대',
        // 카테고리의 남성 횟수
        leftValue: 60,
        // 카테고리의 여성 횟수
        rightValue: 40,
      },
      {
        // 카테고리 이름
        categoryName: '50대',
        // 카테고리의 남성 횟수
        leftValue: 40,
        // 카테고리의 여성 횟수
        rightValue: 60,
      },
      {
        // 카테고리 이름
        categoryName: '60대',
        // 카테고리의 남성 횟수
        leftValue: 10,
        // 카테고리의 여성 횟수
        rightValue: 90,
      },
      {
        // 카테고리 이름
        categoryName: '70대',
        // 카테고리의 남성 횟수
        leftValue: 51,
        // 카테고리의 여성 횟수
        rightValue: 49,
      },
      {
        // 카테고리 이름
        categoryName: '80대 이상',
        // 카테고리의 남성 횟수
        leftValue: 20,
        // 카테고리의 여성 횟수
        rightValue: 80,
      },
    ],
  },
};
