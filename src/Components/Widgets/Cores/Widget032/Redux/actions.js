import * as actionTypes from './actionTypes';
import Widget032SampleDatas from '../Widget032SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget032InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { apiUrl, QueryStateManagerState: queryState } = props;
    const { method } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET032_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget032SampleDatas,
    };
    AjaxManager.run(param);
  };
}
