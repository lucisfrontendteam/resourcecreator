export default {
  id: 'Widget032',
  stateId: 'Widget032',
  name: '통화시간 분포',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/organize/widget032.api',
};
