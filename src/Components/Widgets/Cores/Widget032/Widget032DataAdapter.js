import _ from 'lodash';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError } from '../../Utils/WidgetConstants';

export default class Widget032DataAdapter {
  static commonDataOption = {
    label: '업체명',
    fill: false,
    lineTension: 0.1,
    backgroundColor: '#0c99c9',
    borderColor: '#0c99c9',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    pointBorderColor: '#0c99c9',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: '#0c99c9',
    pointHoverBorderColor: '#0c99c9',
    pointHoverBorderWidth: 2,
    pointRadius: 1,
    pointHitRadius: 10,
    data: [65, 59, 80, 81],
    spanGaps: false,
    type: 'bar',
    yAxisID: 'y-axis-1',
    pointStyle: 'rect',
  };

  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState, widgetOptions) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    }
    this.datas = widgetState.chart;
    this.records = this.datas.data;
    this.options = widgetOptions.chart.options;
  }

  getColor(index) {
    return TextRenderer.renderHexColor(index);
  }

  getSummuryValue(values) {
    return _.reduce(values, (sum, n) => sum + n, 0);
  }

  get totalDatas() {
    return _.map(this.records, record => record.data);
  }

  get pivotDatas() {
    const pivotArray = [];
    _.forEach(this.totalDatas, (data, recordIndex) => {
      _.forEach(data, (point, pointIndex) => {
        if (!!!_.isArray(pivotArray[pointIndex])) {
          pivotArray[pointIndex] = [];
        }
        pivotArray[pointIndex][recordIndex] = point;
      });
    });
    return pivotArray;
  }

  get summuries() {
    return _.map(this.pivotDatas, record => this.getSummuryValue(record));
  }

  get accumulateValues() {
    const { summuries } = this;
    const accumulateArr = [];
    _.forEach(summuries, (sum, index) => {
      const elementCount = index + 1;
      const chunk = _.take(summuries, elementCount);
      const acc = this.getSummuryValue(chunk);
      accumulateArr.push(acc);
    });
    return accumulateArr;
  }

  get accumulatePercentages() {
    const { accumulateValues } = this;
    const total = accumulateValues[accumulateValues.length - 1];
    return _.map(accumulateValues, value => {
      const fixedValue = ((value / total) * 100).toFixed(2);
      return parseFloat(fixedValue);
    });
  }

  get accumulateLineDataSet() {
    const { accumulatePercentages } = this;
    const color = TextRenderer.renderLineHexColor(2);
    return {
      ...Widget032DataAdapter.commonDataOption,
      data: accumulatePercentages,
      label: '누적',
      borderWidth: 1,
      borderColor: color,
      backgroundColor: color,
      pointBorderColor: color,
      pointHoverBackgroundColor: color,
      pointHoverBorderColor: color,
      pointBackgroundColor: color,
      type: 'line',
      yAxisID: 'y-axis-2',
      id: 'total',
      pointStyle: 'line',
    };
  }

  get dataSets() {
    const dataSets = [this.accumulateLineDataSet];
    _.each(this.records, (record, index) => {
      const { data, label } = record;
      const color = this.getColor(index);
      const bar = {
        ...Widget032DataAdapter.commonDataOption,
        data,
        label,
        borderColor: color,
        backgroundColor: color,
        pointBorderColor: color,
        pointHoverBackgroundColor: color,
        pointHoverBorderColor: color,
        pointBackgroundColor: color,
      };
      dataSets.push(bar);
    });
    return dataSets;
  }

  get defaultData() {
    const { labels } = this.datas;
    const defaultData = {
      chart: {
        data: {
          labels,
          datasets: this.dataSets,
        },
        options: this.options,
      },
    };
    return defaultData;
  }
}
