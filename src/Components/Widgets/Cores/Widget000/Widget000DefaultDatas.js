import Widget000SampleDatas from './Widget000SampleDatas';
import Widget000DefaultOptions from './Widget000DefaultOptions';

const {
  chart = {
    data: [],
  },
  grid = {
    data: [],
  },
} = Widget000SampleDatas;

export default {
  chart: {
    data: chart.data,
  },
  grid: {
    datas: grid.data,
    options: Widget000DefaultOptions.grid.options,
  },
};
