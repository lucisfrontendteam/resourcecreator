export default {
  id: 'Widget000',
  stateId: 'Widget000',
  name: '주요 상담현황',
  usage: '읽기',
  type: 'Widget',
  sizes: [6],
  method: 'post',
  apiUrl: '/category/widget000.api',
  gridFieldOrder: [
    'teltimes',
    'silence',
  ],
};
