import * as actionTypes from './actionTypes';
import Widget000SampleDatas from '../Widget000SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget000InitAction(props) {
  return dispatch => {
    const { NetWorkModeState } = props;
    const { QueryStateManagerState: queryState } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET000_INIT,
      query: queryState,
      ajaxType: 'normal',
      sampleData: Widget000SampleDatas,
    };
    AjaxManager.run(param);
  };
}
