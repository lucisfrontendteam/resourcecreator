import TextRenderer from '../../Utils/TextRenderer';

export default {
  grid: {
    options: {
      columnDefs: [
        {
          title: '평균 통화시간',
          targets: 0,
          render: TextRenderer.renderLocaleDateDuration,
          width: '50%',
        },
        {
          title: '묵음비율',
          targets: 1,
          render: TextRenderer.renderPercentage,
          width: '50%',
        },
      ],
    },
  },
};
