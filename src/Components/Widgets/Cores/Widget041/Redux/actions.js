import * as actionTypes from './actionTypes';
import Widget041SampleDatas from '../Widget041SampleDatas';
import manifest from '../manifest';
import AjaxManager from '../../../../../Utils/AjaxManager';

export function widget041InitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryStateManagerState, QueryIdMap } = props;
    const { method, apiUrl } = manifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.WIDGET041_INIT,
      query: {
        ...QueryStateManagerState,
        ...QueryIdMap,
      },
      ajaxType: 'normal',
      sampleData: Widget041SampleDatas,
    };
    AjaxManager.run(param);
  };
}
