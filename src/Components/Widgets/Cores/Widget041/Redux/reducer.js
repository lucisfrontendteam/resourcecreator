import * as actionTypes from './actionTypes';

export function Widget041State(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.WIDGET041_INIT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
