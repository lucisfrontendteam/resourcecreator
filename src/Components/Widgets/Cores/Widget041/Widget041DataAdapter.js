import _ from 'lodash';
import 'moment/locale/ko';
import TextRenderer from '../../Utils/TextRenderer';
import manifest from './manifest';
import { AdvencedFilterArgManagerReadManifest }
  from '../../../../Utils/CommonManagers/AdvencedFilterArgManager';
import scheme from './scheme';
import { validate } from 'jsonschema';
import empty from 'json-schema-empty';
import { validationError, dataEmpty } from '../../Utils/WidgetConstants';

export default class Widget041DataAdapter {
  get emptyState() {
    const emptyState = empty(scheme);
    return emptyState;
  }

  constructor(widgetState = this.emptyState, widgetOptions, advencedFilterArgManagerState = {}) {
    const result = validate(widgetState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${manifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
      this.errorType = validationError;
    } else if (_.isEmpty(advencedFilterArgManagerState.silenceRatioList)) {
      this.errors = [
        `${AdvencedFilterArgManagerReadManifest.apiUrl} 을 점검하여 주십시오.`,
        'silenceRatioList 데이터가 필요합니다.',
      ];
      this.errorType = dataEmpty;
    }
    this.totalCount = widgetState.grid.totalCount;
    this.datas = widgetState.grid.datas;
    this.options = widgetOptions.grid.options;
    this.silenceRatioList = advencedFilterArgManagerState.silenceRatioList;
  }

  get dataArray() {
    return _.map(this.datas, data => data.record);
  }

  getSummuryValue(values) {
    return _.reduce(values, (sum, n) => sum + n, 0);
  }

  get summuries() {
    const { pivotArray } = this;
    const [, , ...numbers] = pivotArray;
    return _.map(numbers, record => this.getSummuryValue(record));
  }

  get pivotArray() {
    const pivotArray = [];
    _.forEach(this.dataArray, (data, recordIndex) => {
      _.forEach(data, (point, pointIndex) => {
        if (!!!_.isArray(pivotArray[pointIndex])) {
          pivotArray[pointIndex] = [];
        }
        pivotArray[pointIndex][recordIndex] = point;
      });
    });
    return pivotArray;
  }

  get finalDatas() {
    const { summuries, dataArray } = this;
    const summurieRecord = ['계', '', ...summuries];
    return [summurieRecord, ...dataArray];
  }

  get noColumnHeaderDefTemplate() {
    return {
      title: 'No',
      targets: 0,
      width: 'auto',
      render(field = {}) {
        const {
          value = {
            name: '',
          },
        } = field;
        const { name } = value;
        return TextRenderer.renderText(name);
      },
    };
  }

  get columnHeaderDefTemplate() {
    return {
      title: '카테고리명',
      targets: 1,
      width: 'auto',
      render(field = {}) {
        const {
          CategoryFilterListState = {
            name: '',
          },
        } = field;
        const { name } = CategoryFilterListState;
        return TextRenderer.renderText(name);
      },
    };
  }

  get columnAvgDefTemplate() {
    return {
      title: '평균 묵음비율',
      targets: 2,
      width: '10%',
      render(field = {}) {
        const {
          value = {
            name: '',
          },
        } = field;
        const { name } = value;
        return TextRenderer.renderPercentage(name);
      },
    };
  }

  getColumnNormalDefTemplate(ratioName, index) {
    return {
      title: ratioName,
      targets: index,
      width: '9%',
      className: 'color-01',
      render(field = {}) {
        const {
          value = {
            name: '',
          },
        } = field;
        const { name } = value;
        return TextRenderer.renderNumberWithGun(name);
      },
    };
  }

  get columnDefs() {
    const filteredRatioList = _.filter(this.silenceRatioList, ratio => {
      const isZero = (_.toString(ratio.id) === '0') || (_.toString(ratio.name) === '선택');
      return !!!isZero;
    });
    const padding = 3;
    const columnDefs = _.map(filteredRatioList, ({ name: ratioName }, index) => {
      const def = { ...this.getColumnNormalDefTemplate(ratioName, index + padding) };
      return def;
    });
    const allColumnDefs = [
      this.noColumnHeaderDefTemplate,
      this.columnHeaderDefTemplate,
      this.columnAvgDefTemplate,
      ...columnDefs,
    ];
    return allColumnDefs;
  }

  get gridOptions() {
    const { options, columnDefs } = this;
    const newOptions = {
      ...options,
      columnDefs,
    };
    return newOptions;
  }

  get totalPageNumber() {
    // 그리드 데이터에서 취득해야 한다는 점에 주의
    const totalPageNumber = TextRenderer.renderTotalPages(this.totalCount, manifest.recordCount);
    return totalPageNumber;
  }

  get defaultData() {
    const { totalCount, dataArray, gridOptions, totalPageNumber } = this;
    return {
      grid: {
        totalCount,
        totalPageNumber,
        datas: dataArray,
        options: gridOptions,
      },
    };
  }
}
