/* eslint-disable */
/**
 * 위젯 식별자: WIDGET041
 * 위젯 이름: 카테고리별 인입 현황
 */
export default {
  grid: {
    totalCount: 1000,
    // 카테고리별 인입 현황 그리드의 데이터 배열
    /*
     카테고리명,
     구분값이 차지하는 비율,
     묵음비율 10% 미만 이상이 차지하는 비율,
     묵음비율 10% ~ 20% 이상이 차지하는 비율,
     묵음비율 20% ~ 30% 이상이 차지하는 비율,
     묵음비율 30% ~ 40% 이상이 차지하는 비율,
     묵음비율 40% ~ 50% 이상이 차지하는 비율,
     묵음비율 50% ~ 60% 이상이 차지하는 비율,
     묵음비율 60% ~ 70% 이상이 차지하는 비율,
     묵음비율 70% ~ 80% 이상이 차지하는 비율,
     묵음비율 80% 이상이 차지하는 비율,
     */
    datas: [
      // 컬럼별 평균값
      {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "0",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "1",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "2",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "3",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "4",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "5",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "6",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "7",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "8",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      }, {
        record: [
          {
            value: {
              TargetSilenceFilterStateId: '',
              name: "9",
            }
          },
          {
            CategoryFilterListState: {
              id: '71',
              name: "평균 묵음비율"
            },
            value: {
              TargetSilenceFilterStateId: '',
              name: ''
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: 'avg',
              name: '0.1890'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '1',
              name: '6606'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '2',
              name: '3571'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '3',
              name: '4221'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '4',
              name: '4246'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '5',
              name: '4360'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '6',
              name: '3628'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '7',
              name: '7290'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '8',
              name: '3110'
            }
          },
          {
            value: {
              TargetSilenceFilterStateId: '9',
              name: '3110'
            }
          },
        ],
      },
    ],
  },
};
