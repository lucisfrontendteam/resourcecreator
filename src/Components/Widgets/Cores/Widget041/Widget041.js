import React, { PureComponent } from 'react';
import { v4 } from 'uuid';
import $ from 'jquery';
import BasicDataTable from '../../../../Components/Widgets/Parts/Grid/BasicDataTable';
import TextRenderer from '../../Utils/TextRenderer';
import Widget041DataAdapter from './Widget041DataAdapter';
import Widget041DefaultOptions from './Widget041DefaultOptions';
import manifest from './manifest';
import './custom.css';
import ExcelDownloader from '../../Utils/ExcelDownloader';
import _ from 'lodash';
import WidgetRenderer from '../../Utils/WidgetRenderer';
import ReactPaginate from 'react-paginate';
import WidgetDataComparator from '../../Utils/WidgetDataComparator';

export class Widget041 extends PureComponent {
  static defaultProps = {
    tableClassName: 'lucis-table stripe nowrap',
    ...manifest,
    tableId: v4(),
  };

  constructor(props) {
    super(props);
    this.shouldComponentUpdate = props.shouldComponentUpdate.bind(this);
    this.firePagingAction = props.firePagingAction.bind(this);
    this.handlePagingClick = props.handlePagingClick.bind(this);
    this.handleCellClick = props.handleCellClick.bind(this);
    this.render = props.render.bind(this);
    this.state = { pageNumber: 1 };
  }

  componentDidUpdate(prevProps, prevState) {
    const { props, state } = this;
    const pageChanged = state.pageNumber !== prevState.pageNumber;
    const isFirstPage = state.pageNumber === 1;
    if (pageChanged || isFirstPage) {
      this.firePagingAction(
        props,
        props.widget041InitAction,
        state.pageNumber,
        manifest.recordCount
      );
    }
  }

  componentDidMount() {
    const { props, state } = this;
    if (props.isTestMode === 'true') {
      this.firePagingAction(
        props,
        props.widget041InitAction,
        state.pageNumber,
        manifest.recordCount
      );
    }
    this.setCellClickEvent();
  }

  componentWillUnmount() {
    this.unsetCellClickEvent();
  }

  get cellSelector() {
    const { props } = this;
    const selector = `#${props.tableId} tbody td[detailPlayer]`;
    return selector;
  }

  setCellClickEvent() {
    const self = this;
    $(document.body).on('click', this.cellSelector, (e) => {
      self.handleCellClick(e, 1, manifest.recordCount);
    });
  }

  unsetCellClickEvent() {
    $(document.body).off('click', this.cellSelector);
  }

  handleExportExcelClick(e) {
    e.preventDefault();
    const { props } = this;
    const { excelUrl } = new ExcelDownloader(props, manifest);
    window.open(excelUrl);
  }

  renderInfos(infos) {
    const messages = _.map(infos, info => <p key={v4()} style={{ marginBottom: '4px' }}>{info}</p>);
    return messages;
  }

  renderWidgetCore(widgetState) {
    const { props } = this;
    const {
      AdvencedFilterArgManagerState,
      QueryStateManagerState,
    } = props;
    const adapter = new Widget041DataAdapter(
      widgetState,
      Widget041DefaultOptions,
      AdvencedFilterArgManagerState,
    );
    let core = {};
    if (WidgetDataComparator.valid(adapter)) {
      const { defaultData } = adapter;
      const infos = TextRenderer.renderQueryStates(
        QueryStateManagerState,
        AdvencedFilterArgManagerState
      );
      core = (
        <div className="lucis-widget">
          <div className="lucis-widget-header">
            <h2 className="lucis-widget-title">{props.name}</h2>
            <div className="pos-right">
              <button
                type="button"
                className="btn-tool btn-file"
                onClick={(e) => this.handleExportExcelClick(e)}
              >엑셀저장</button>
            </div>
          </div>
          <div className="lucis-widget-body">
            <div className="lucis-table-label" style={{ height: '66px' }}>
              {this.renderInfos(infos)}
            </div>
            <BasicDataTable
              {...props}
              options={defaultData.grid.options}
              datas={defaultData.grid.datas}
              tableClassName={props.tableClassName}
            />
            <div className="clearfix">
              <ReactPaginate
                pageNum={defaultData.grid.totalPageNumber}
                clickCallback={this.handlePagingClick}
                {...props.pagingProps}
              />
            </div>
          </div>
        </div>
      );
    } else {
      core = WidgetRenderer.renderDataFailWidgetCore(adapter);
    }
    return core;
  }
}
