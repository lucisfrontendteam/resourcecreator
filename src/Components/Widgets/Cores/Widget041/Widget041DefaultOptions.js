import $ from 'jquery';

export default {
  grid: {
    options: {
      searching: false,
      paging: false,
      ordering: false,
      headerCallback(thead) {
        if ($(this).find('thead tr').length === 1) {
          const $firstHeader = $(thead);
          $firstHeader.find('th:eq(3)').addClass('th-bg');

          const $secondHeader = $firstHeader.clone();
          $firstHeader.find('th:eq(0)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(1)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th:eq(2)').attr('rowspan', 2);
          $secondHeader.find('th:eq(0)').remove();

          $firstHeader.find('th').slice(3, 11).remove();
          $firstHeader.find('th:eq(3)').attr('colspan', 9).text('묵음 비율');

          $firstHeader.after($secondHeader);
        }
      },
      rowCallback(row, fields) {
        const headerLength = 2;
        $(row).find('td').slice(headerLength)
          .each((columnIndex, column) => {
            const [, col1Field] = fields;
            const targetField = fields[columnIndex + headerLength];
            const datas = {
              ...col1Field,
              ...targetField,
            };
            $(column).data(datas).attr('detailPlayer', true);
          });
      },
    },
  },
};
