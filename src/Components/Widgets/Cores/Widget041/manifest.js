export default {
  id: 'Widget041',
  stateId: 'Widget041',
  name: '상담 유형별 묵음비율 분석',
  usage: '읽기',
  sizes: [12],
  recordCount: 25,
  type: 'IdList',
  inputIdList: [
    {
      key: 'pageNumber',
      id: 'Widget041pageNumber',
    },
    {
      key: 'pageSize',
      id: 'Widget041pageSize',
    },
  ],
  method: 'post',
  apiUrl: '/categoryorg/widget041.api',
  excelUrl: '/categoryorg/widget041.excel',
};
