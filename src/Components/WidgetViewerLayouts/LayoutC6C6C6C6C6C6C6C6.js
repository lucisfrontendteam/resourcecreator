import React, { PureComponent } from 'react';
import WidgetRenderer from '../../Components/Widgets/Utils/WidgetRenderer';

export class LayoutC6C6C6C6C6C6C6C6 extends PureComponent {
  static defaultProps = {
    name: '6-6-6-6-6-6-6-6 레이아웃',
    type: 'WidgetViewerLayout',
    TargetWidgets: ['', '', '', '', '', '', '', ''],
  };

  render() {
    const { props } = this;

    return (
      <div>
        <div className="content-wrapper">
          <section className="content-inner">
            <div className="row js-row-group">
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[0], 6)}
              </div>
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[1], 6)}
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[2], 6)}
              </div>
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[3], 6)}
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[4], 6)}
              </div>
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[5], 6)}
              </div>
            </div>
            <div className="row">
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[6], 6)}
              </div>
              <div className="col-md-6">
                {WidgetRenderer.renderWidget(props.TargetWidgets[7], 6)}
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
