import React, { PureComponent } from 'react';
import WidgetRenderer from '../../Components/Widgets/Utils/WidgetRenderer';

export class ExperimentalLayout extends PureComponent {
  static defaultProps = {
    name: '개발용 레이아웃',
    type: 'WidgetViewerLayout',
    TargetWidgets: ['', '', ''],
  };

  render() {
    const { props } = this;
    return (
      <div>
        <div className="content-wrapper">
          <section className="content-inner">
            <div className="row js-row-group">
              <div className="col-md-4">
                <h1 style={{ fontSize: '15px' }}>렌더링 사이즈: col-md-4</h1>
                {WidgetRenderer.renderWidget(props.TargetWidgets[0], 4)}
              </div>
            </div>
            <div className="row js-row-group">
              <div className="col-md-6">
                <h1 style={{ fontSize: '15px' }}>렌더링 사이즈: col-md-6</h1>
                {WidgetRenderer.renderWidget(props.TargetWidgets[1], 6)}
              </div>
            </div>
            <div className="row js-row-group">
              <div className="col-md-12">
                <h1 style={{ fontSize: '15px' }}>렌더링 사이즈: col-md-12</h1>
                {WidgetRenderer.renderWidget(props.TargetWidgets[2], 12)}
              </div>
            </div>
          </section>
        </div>
      </div>
    );
  }
}
