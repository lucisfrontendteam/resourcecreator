import React, { PureComponent } from 'react';
import WidgetViewer from '../../Components/Common/WidgetViewer';
import ExternalResourcesList from '../../Components/Common/ExternalResourcesList';
import { version, name as programName } from '../../../package.json';
import _ from 'lodash';
import './Resources/dist/css/AdminLTE.min.css';
import '../../Resources/css/jquery-ui.css';
import '../../Resources/css/jquery.timepicker.css';
import '../../Resources/css/jquery.dataTables.css';
import '../../Resources/css/jquery.mCustomScrollbar.css';
import '../../Resources/css/jqtree.css';
import '../../Resources/css/bootstrap.css';
import '../../Resources/css/bootstrap-datepicker.standalone.css';
import '../../Resources/css/lucis-base.css';
import '../../Resources/css/lucis-custom.css';
import '../../Resources/css/lucis-widget.css';
import '../../Resources/css/lucis-theme.css';
import AdvencedFilterContainer from '../../Components/Common/AdvencedFilterContainer';

export default class Page extends PureComponent {
  componentDidMount() {
    const { props } = this;
    props.advencedfilterargmanagerreadInitAction(props);
  }

  render() {
    const { props } = this;
    const selectedProjectTheme = _.find(props.ProjectThemeListState, ({ selected }) => selected);
    const wrapperClassNames = `wrapper ${selectedProjectTheme.name}`;

    return (
      <div className={wrapperClassNames}>
        <header className="main-header">
          <a href="#" className="logo">
            <span className="logo-lg">{programName}</span>
          </a>
        </header>
        <aside className="main-sidebar">
          <section className="sidebar">
            <ExternalResourcesList {...props} />
          </section>
        </aside>
        <AdvencedFilterContainer {...props} />
        <WidgetViewer {...props} isTestMode="true" />
        <footer className="main-footer">
          <div className="pull-right hidden-xs">
            버젼: {version}
          </div>
          <strong>Copyright &copy; 2016 <a href="#">LUCIS</a>.</strong> All rights reserved.
        </footer>
        <div className="control-sidebar-bg"></div>
      </div>
    );
  }
}
