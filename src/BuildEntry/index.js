/**
 * 빌드 시작지점
 */
import * as WidgetCores from '../Components/Widgets/Cores';
import * as WidgetThemes from '../Components/WidgetThemes';
import * as WidgetViewerLayouts from '../Components/WidgetViewerLayouts';
import WidgetViewer from '../Components/Common/WidgetViewer';
import ProjectWriter from '../Utils/ProjectWriter';
import TreeTraveser from '../Utils/TreeTraveser.js';
import projectSampleDatas from '../Redux/projectSampleDatas';
import EnvironmentManager from '../Utils/EnvironmentManager';
import AjaxManager from '../Utils/AjaxManager';
import BasicDataTable from '../Components/Widgets/Parts/Grid/BasicDataTable';
import TextRenderer from '../Components/Widgets/Utils/TextRenderer';
import WidgetRefiner from '../Components/Widgets/Utils/WidgetRefiner';
import WidgetDefaultProps from '../Components/Widgets/Utils/WidgetDefaultProps';
import WidgetRenderer from '../Components/Widgets/Utils/WidgetRenderer';
import PropsCreator from '../Utils/PropsCreator';
import { version } from '../../package.json';
import * as CommonMangers from '../Utils/CommonManagers';
import WidgetDataComparator from '../Components/Widgets/Utils/WidgetDataComparator';
import CommonManagerRefiner from '../Utils/CommonManagerRefiner';
const {
  CommonManagerManifest,
  CommonManagerSampleDatas,
  CommonManagerReducers,
  CommonManagerActionList,
  CommonManagerActionTypeList,
  CommonManagerAdapters,
} = new CommonManagerRefiner(CommonMangers);

const UIContents = {
  WidgetCores,
  WidgetViewer,
  WidgetThemes,
  WidgetViewerLayouts,
  ProjectWriter,
  TreeTraveser,
  projectSampleDatas,
  EnvironmentManager,
  AjaxManager,
  BasicDataTable,
  TextRenderer,
  WidgetRefiner,
  WidgetDefaultProps,
  WidgetRenderer,
  PropsCreator,
  WidgetDataComparator,
  CommonManagerManifest,
  CommonManagerSampleDatas,
  CommonManagerReducers,
  CommonManagerActionList,
  CommonManagerActionTypeList,
  CommonManagerAdapters,
  version,
};
export default UIContents;
