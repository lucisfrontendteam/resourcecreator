import * as actionTypes from './actionTypes';
import * as defaultStates from './defaultStates';
import * as WidgetCores from '../Components/Widgets/Cores';
import WidgetRefiner from '../Components/Widgets/Utils/WidgetRefiner';
const { widgetActionList } = new WidgetRefiner(WidgetCores);
import { actions as dashboardActions } from '../../../userdashboard/src/Redux';
import {
  UserManagerActions,
  AdvencedFilterArgManagerActions,
} from '../../../resourcecreator/src/Utils/CommonManagers';

function widgetCoreListInitAction(widgetCoreListState = defaultStates.widgetCoreListState) {
  return {
    type: actionTypes.WIDGET_CORE_LIST_INIT,
    payload: { widgetCoreListState },
  };
}

function widgetSelectAction(displayName) {
  return {
    type: actionTypes.WIDGET_SELECT,
    payload: { displayName },
  };
}

function widgetThemeListInitAction(widgetThemeListState = defaultStates.widgetThemeState) {
  return {
    type: actionTypes.WIDGET_THEME_LIST_INIT,
    payload: { widgetThemeListState },
  };
}

function widgetThemeSelectAction(displayName) {
  return {
    type: actionTypes.WIDGET_THEME_SELECT,
    payload: { displayName },
  };
}

function widgetViewerThemeListInitAction(
  widgetViewerThemeListState = defaultStates.widgetViewerThemeListState
) {
  return {
    type: actionTypes.WIDGET_VIEWER_THEME_LIST_INIT,
    payload: { widgetViewerThemeListState },
  };
}

function widgetViewerThemeSelectAction(displayName) {
  return {
    type: actionTypes.WIDGET_VIEWER_THEME_SELECT,
    payload: { displayName },
  };
}

function projectThemeListInitAction(
  projectThemeListState = defaultStates.projectThemeListState,
) {
  return {
    type: actionTypes.PROJECT_THEME_LIST_INIT,
    payload: { projectThemeListState },
  };
}

function projectThemeSelectAction(displayName) {
  return {
    type: actionTypes.PROJECT_THEME_SELECT,
    payload: { displayName },
  };
}

function projectInitAction(
  projectState = defaultStates.projectState
) {
  return {
    type: actionTypes.PROJECT_INIT,
    payload: { projectState },
  };
}

function networkModeOffAction(
  networkModeState = defaultStates.networkModeState
) {
  return {
    type: actionTypes.NETWORK_MODE_OFF,
    payload: { networkModeState },
  };
}

function networkModeOnAction(
  networkModeState = defaultStates.networkModeState
) {
  return {
    type: actionTypes.NETWORK_MODE_ON,
    payload: { networkModeState },
  };
}

export const actions = {
  ...dashboardActions,
  ...widgetActionList,
  ...UserManagerActions,
  ...AdvencedFilterArgManagerActions,
  widgetCoreListInitAction,
  widgetSelectAction,
  widgetThemeListInitAction,
  widgetThemeSelectAction,
  widgetViewerThemeListInitAction,
  widgetViewerThemeSelectAction,
  projectThemeListInitAction,
  projectThemeSelectAction,
  projectInitAction,
  networkModeOffAction,
  networkModeOnAction,
};
