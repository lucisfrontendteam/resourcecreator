/**
 * 로그인 한 사용자가 조회 가능한 프로젝트 정보 샘플 데이터 리스트
 */
export default [
  {
    id: '프로젝트 식별자',
    siteName: '프로젝트 이름',
    accessPath: 'testProject',
    owner: '소유자',
    thumbnail: 'base64 인코딩 된 이미지',
    // base64 인코딩 된 CI 이미지
    // eslint-disable-next-line
    ci: 'iVBORw0KGgoAAAANSUhEUgAAAIcAAAAeCAYAAAAGjg2BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjcyRDEyMzhBODU0QzExRTZBRkU2RkJBMjA3QTJBQTI1IiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjcyRDEyMzhCODU0QzExRTZBRkU2RkJBMjA3QTJBQTI1Ij4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NzJEMTIzODg4NTRDMTFFNkFGRTZGQkEyMDdBMkFBMjUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NzJEMTIzODk4NTRDMTFFNkFGRTZGQkEyMDdBMkFBMjUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7UTaPRAAAW6ElEQVR42uxbCZgdVZX+q97S/XpLpzudTtLdWUholixkIxECIYLIIgoCQXQAR0VQUUdUUEQBA4qDfIgLjAgoDijbICNIWBwZBgkmJJAQErKYjZB0esnS/Xp/S9X8p+6p9+pVXifh05mPmY/6+vStuvty7jn/Ofe+KM5dhvxjk1zAknfLhJb3bxyy0blAdDqTpyHiHo5oNsLsLvNEmWEn6U3SSuZdwri1fM9qReZxXRR/IljQ/i2UOl2szN4v1WL7Lstafp+8qoaqK5g/H4afA6UXqzvYdrh8uO0DP65XjylnMb+bmxa/bLBP+XZQkHf/tvL1hsflvRcOZMh2guOX7+hBBkOmiC9ENnZ+orRvblN1B6YM60a/U4oX94xEb6qUWbigtjMeEWee10cX2/jvOYb/yq+XtefvPf8HnwMxx0I48WsrYv3HnDNpBc6btAmReAzr++vRMlCDVf0j0c+dPrY8hW09ZJIMqyJ3k/XGw3Eug5U5C5bzMOu5hdT63lT//2COKGXK9cjEvtJQnqy4fvZinN28CS+0HoOfrDoebyTrkY5EkHajiJe5+Nb7WpGoTGPFruHoTdvkhwzakjGsbS8ds7kzfiVcZzLgXMF6N72L56GM1EhyApKOahFv/S9IvmbSBFKJtiW6tYP0Bqn73cQcwhg3UVVcnYilraunPoeFzWuxZPd0fH3ZmXi7t5pDYP9t9j/mIpuNYuVgLW6e+zZOmbQX6SzTMgNIpwbhZAaxpqUEi5ZO+uCqtsrfcN4vYf0biqIONz1U/yiSkCBlQsCoNxTng4IRpApSKjDRfaS9B5mH91PXfpdhHXVtjO9S127S1ELMk3vqtJ3BQJ+knX2HwEwy58eQziWdKXiOeKKcbUQNxOOX6/p9Xk16lPQ0qf0Q1rOSdCPpQzkgYtq7mfSLUP8bSAOaz9H6o5omcf3R0OR+nfHfIPjEzPr1OH38JkqJYVjaOc4wRhk3U0wYg1mjRKOUFC92jkDbQBbDyzNkiCzibj+601n8cUcUExO7WcdurGofPgdI/5RS5ONsY08YpO2Lj8fIwTeLDVbyX0bqUmawNZQJWF6Ekb5NEibs1J0vEuFx0hWBfKNIY7UeVylJeoj0RS7MaAVnsmBzlNGCrNFD+qz2a4+Wl13/B9JXZVIPsHjS7jdIl5IF4gZoIstwB9uklHL72OZIxo1nXBPjmmShLSNFbiI9puMKrlmdhtKPuPSD5Q43jJcDqJMYjtB8EdKntK8jAlJSxlNP+r7GvRxkjlNIV0lZK5rFSSO3o4TM4EaHIWuxzYgwBZRYf5xE9TJqOGexslJeUVuapTSJIZV0ccea0djUdgSlAtcgTspap3I5ZPGuDDPHyurP4KzWzyNtV4Y33maOjLvHupjxCY3LcKD3Fpl4ET+PciJkN3+O+Ss1vjGUr0F3Y1wnJc0yaa13mI/WGQpjPRwoF9Od9QLz3cr0SoaXCSLXRW4w6BwhXso9E0m/JM3P53GFwe9iPa+xvi0iEfk+kiFVsXsySRaxnks8lbkf4PukwOL5Zso1pHnKxMLw3axLNoDDumwdz0LSTO2/0HphcKb9kHkm6RxkfQtTLaOMzxw1pOsYX+PJyIiL+rI0nEg50lYCc0f1om5YPzqynC8RfsIYcRatjOCCI1xUV1ShnYLwT20uplSUYGJ9Er/6yC48sTaOpzYl8GZb3ExGxv4C+/x7tvxCfnwOU2xsrPgwJvY+yxHFgxP6Ioe2jhM/l509RhlHJqC8iJkp8S9xsG+LyDaL581fOmRJr2WePzD9bCPB3fFGUnhgOmgKyg4bHzAPWwLSZgXjEoy7iGGFFGc2Sgy3N2Qx5jUn1RbzzjeuAi/DsyrRNofytij9kdrtL8x6H7vJLWiROT3VJ76HPwXM6L8w7XLmSxSYsQGTVMcxPmAGl5P+WbHNJC3Xw7BSyotWFQYxngoRQS5O9JnRdSwMZuOEFhEMcgPNaejBhydSgtpkiCip1DDHxNERnDC+BPFYBVZ1V+LSJcPxg7UEpiwzc5SL6+e34Y5Tt+DoGm7mNCWv7a38d8KSwyEDvpU4kRZxWhc6T+wwd4E70h8oqYTBdBmzvwhmQDkaxpiI7x8I1OM3OMA8X+f3xwyjwDHlvAZ+z/+fERHLuMcCHCUvf2YZwQlXB/S77bfLLKICPkr6CGlKSHwQV1hz/XbIkl0MflyEMcKuhCdY5v6ADyNKulDnwc/0CF9PYfpVok4NX3gM+BjDq0krVbJJGWGGLzFOxidrUaX9l3rPZ3iaqdswiIjB0aSLckNhjONE8RKtEsGdWUpKi/xw+YwWNI8gfnHIGFGjVubVR1FfQWkfK0eirBxJMsxjHaVYmyynmimjBIphwvAUasq56ILxIt5izSKdkGeACKLZThy39zYKlrJiM3QU89Xl1sgDi9bJHFOV66LAiZSftKAjyM1RIE8/01kvTmI2m98CyG4gfUJF/92kT7LUT1S6CE48i3EEXng77CzTiZ9LepBvVFnupaSot9HM5Ns+VvCYVlQYway+5/oadGwF3rtDjsR4EafeMjNPZk60/58m/ZD0D6xqmYl3RTOM4fdy7U9E24myDmG6s3wHHdQlycXCceEGV3c0ob1rBEpiGfSlozisOoWbTmxFw/Cs8WnEbYyptFEajcOhNDmyxsYJEyLoZdovNpZhfVccmzsrcOdr47CsjZs5kYU6QPmB84KLJpimxPOQGnGoO1FCgjb3PN+q8pghb/6dXjiJ+foO5EHVp57lLmLeUhWpbzC4WS0O/xEVcQvTXtXdKAD1muDiKFP4CyS7kurS/S/GriM5AeGxzbRh+UxaywKXK7Md6DlJNm6O0Q2j/lteWuWeyd4GF9lseRbTvQqy5VnHzA+qJI0o/mhUjIYAIGW/hcmsnKFD05UdcInqfTkt8ZEMdiRr8Zutk3Ht6DVIMro3Y+O0piTuTLTjihWN2JGKeYsZoQhKs0xTwsJZoyy8tNXCoztKsLqlFvZAJdaJY93KGMZI5xaymYVLfFNw1OCqoq5zPheSztIJdRi0cFpkYAIGr+UEvZ43j60C12/YLRza7dWirnQ3eXuB7ynTTkH7HV79rjVb045RYJoKAk8jtq2XFYP0qaRwAvVk1JxsZt5pRrhZFyh2EifhVrV8ZMGq1Go4jfQFZh3n5s3pHytWCUsXsdTiOt4uZZDgPPRY8OfGY8gq36rSMiLab2PqGIZzjRqGhzmO9ZjEl9EekzjeaB5YPxnLWsagMi7qJIvuwSxOJv6464ROHFaZwaPbLezoTqOCdUfcFAZTLJVxvHnZsDuKde1Ro0qkj05+Z/NvItuaRHDjWZtTkg/BKrRS2HnrCnb6NnanzLgd8C9U1iJFXtFFnkb6NekDofMNy5c8Bzjr6GGeLj+dofgzEmFLw9vhwHTksI3VSsrmVSLgMxTrk8FktW9OTrLoi1okFPGW7PyMMpWYqU95Jqrr3sPwDqb9huGTDG/zLAnLwygb+f41vn0jz5gFWEvG061ScCxptj8HOo5pysDS5h6OYY9YMsGzKMbVGLyWr1ckx4Q87NcF8vBBFh19ZbjxlVm4c/hraBjRS5Bage7+DObXJXH/vAi+uLoW5y+J49yxaewadPHkdpklzl3K8coTTAj845Rl864WqGqxrGpYpWjufpioLiNTbKt6EytiDjt3grEYvH5x4qxr2F/RvxepKSnAT0Deg4LsFcU/wO+Ur8utodWLuPOfI83RhZ2ufoTvBFRLBeNvYV2TA7vwLpF2hXgjt6ttFdv7H9zld+gahpcYrOO+n2VON5LEOpYZjkXejJa8+xguEcuE9AzrWL//QV/uVczg35Iu10W/yWAVV/wjAjQ/GTgofJLxbXytDkoXyzJQOXiIF4WPBPLbw0R56iWNpbvqcMPyybh1wV9RXZnEIK2O7oE4plcncd8sB5csr8X1L8cNKqB0QVqYI2MYJKNxHpZwUID+vZ0WwcjUGsrpQSK0UhGx/8ROLdRFFe/dKtlN6qDynUt/Vf16MQd2OfNPU2fZuRzndhZ9zQAXs22H4A/p1M+YNpNlztSJE6fQCcpo8pwqzON1VcSsMKFl/crbOiGJFD4J1iehTrhUEX/MM8y7hHnvFt+KuM9do+ocA7ixi828ze89imWy6pH1RZZsM4JqwjXTFVHPi0RDi4mu7niZM0oTjNQyaQHMTF8UELK60GJyuDGjmnJMVxI1bAJlmohp11ImkgQ7gyc2NBKY2vje/C2orexCH/HGPuYbm0jj/pkpfGV5DZ5vodqzBayKtBAmERWjakbiso5RLa4yoEN7meqoxx6FSrdFliBlHExIMllMvBViPiqThAGYiNA7XQO0FjCJEsflTvd0d9wHVYGBFrs50Ma0T/H7q9w1C1lPozLDnMAis09Wm2Ukxk99kFeIbdwCBgk8h5F+EACw2ZCHzwqc5bhWTkXl6rF049r5DZxzxrUqM6wP+UcuFu+rSl9xbolTb4Nuskf4/hTjU4KJGPYyXMowySZ72NxbInUFY4naY/zaqBgLMAY718425yYSJeckEVWfVLOPrmlEbzqBRfO24LCRHegZHIbu9CAaSvvwyzk9+PJLdXhic7mXFw6ZIi1M4Zp6RHpkdW5cn0mE+rGq8jOozmxFqdNJxWTfWezeRbG7E/rsU/f44wFRO80zPfPLVuRORa7+dmb8JqN+wq95TBERP1oLSd1rmedlxm0Y+t5GEHsU9HO7J+0si6rD87tYQceUYhTHE3K+jghgpQBushQDuOrcspnSwveaImMStfsjpgl+GavAWBZaPKDCLN9U/4xgo1JlkH8X05ZNvCLMz7jn1Ursi3qXdNxI07BoDxbUv4i/7JmJ1lStwQvZmIJUsTYsLF5fj919pbhl/jrMaGhFZ6oKPZlSVMS7cPvM3SjLNOCh9bXKCBnDHBllNMe3RT3wwYm3zGGYmwktmGd7X8LOHaELBD27eEPPL9JDXeLR8rIr4m5e15cMgTvqVHJUsrIdzJhVPf+EgEc98zjQI17PkoBKiYckCBfKEifUYj3Mu5z9maXpIvl+znAVVzxh5Z01hVZWjpkQ9N1E+Ebp6m4cAn9AXeg0p3EG819g8I3V5Lr7SzmxrozUcddbxnH2oG9FCnO8Ctc+mUwWO2fMEtTE0rh3y9nezM6oW0OJUo5Xk+MMs0QG8MqOKnz6mSn4/ry1OG3CLiTT5cQgEVSw3K2zWlCeaca9q5tEbRgJJGpF3j2JAcMontpwt4Z3cn5De1wt4HR+QHo8peBsiCNcy58gcXBtFicTI2xzoFX0ppcIS1ncf4SYx/lFvorh8mK8F5JesstEBCc07S3Z5KHJ30a6L28xuLN0wcXcfdy4vv/2J+w804O2Gxmey57FlbF2M2kxX9Ywkxxkivt/BsNz1IU+id+CvwSzXMn3bWKtvETT9Qs9mUR1W/8YLGz4M57ddRzi8UH86Oj78budC/DqvglGeshi2/3YsjuBLz0/FYvmvo5zmrchlY2QQaIoiw7iuqntSPXMwP1vTjQqSawXN6BOjIqlDrQGi4l61cM/l3MSOVvxHUVq/rnFRAUKr/FtUU9nVCOHuhPRpqi+i1WIyRzROY7nEXzerCuCK5aZk89cw70+PhqCqSIBYSBMW3IIzrp3xBj6jGDcT30nocrP36kltiXUR5GqtzL/d4WR9KDuHOmbeIhFJ70G21mSycaxdO+RaB7+Nq47+j58+/D70ZTYSetEeMI2Dt80QUia79YAOpIRfPmFGbjr1bH83Itoph3JbuIahtdPeRanjNqkx1SqShz4gHQf6fFcPJlmWcUVcAuxgUiPfZ4vIidm5ZQRCFJ4AXSypcc7dNdu1XsZQ02sa84jrKzvNtbTSRTzgoYWY0AvA21T6ggzb2jxI/4ZjzKZhf+RxxU1crp/B4XB8wyv0Du+YeaVDbpGryD8Z6DfZ4gJbJsd5N7DfjsrOw/DhmQDTh+7AvNGvYm+TBSZtDFJy9xBjIh0YWSsi5jT8YB8etDBoqWzcNuyaegnY8TSBKpdvYTnbbh0wp9REulTUFpgqciJ7MpgD/vtmoKzEaWYx8m5HSwLaOmcWgfZTR6Uy4G6PCO6xRgqpmcnvhMwm5Nvh3CReagLv2FGJvUFrgNIgx7AtEIcFTSJ830IA3JrKKkhQ2jM+1a8dmn1Wa0Hufy8lxmfDhoAcgUhqj37D0RSz2zrbjzzTy2zMam8ndZoiXc6N6/uDcSiaUyq2oUjhrfi8dZj8estH0TW83ymaYhEcfvqGdjX4+CqaU8jaqWQHHAxrWI1GkuOx+aBJnPMYAbXRUlxc3jbNw4uK7YQwuX7mLXJHCF7dn4mfzD0dxPDe9WEi+kyROH+rXcDi1pX29h2Rk9ALd9Z93cch//s8hW1mtpT2RU5ou/NKfDQLXUP4wFzQ+ltERz1WWPP23LZJPahrX21lbNrN6CxfA8G0nGMrWr3GGR2zWZ0DpTiZ5vPwJ7BaoM/cj4LYPWeUaiK7Mb7qtegfyBFiNKDJbuOxrbe+rxr1JWLKh4aLpjI9/X8jIa8E170AXOgZB2run+EWixb1AvokXcEYOX9Afk0L4z6BsB+Lsv8I67kC1l/jQqPqGKdTi3v12UXtBt41++CdnT3BcjDGHLoJSBYfBVijgZPRw9U95DtFGEaqfe4gHUy3pzEWqv0Rh0KPaPeiff1fLnYP6WVfrHsTRY+GvzdCi5DJnH7sSNWJ26ffg/qEl3opQRJ2Gm82dWIq17/LDb3sq3oQEB+KyDMJDBr+FrcMeMuVMY7acHGcP7Sr+GvPRMNmHXdhznHl1FyJEMWIRYkb0SpW/R3K8ez6t9ScIyzzFagXne3Fzi5fA9Swe8/XHU0epL7DX5+Ua/3FbX7OBHXMfhuQF1s1Im0Q2bkfqI/MNnivr7tAJu9lAUeYMbztHyvenszObU2dN1BUCxu/28dEHW4rtwn+THznhyI3MhhPK3+jIya1+LX+aAcHwTakHsrcufj9fAF43sQ7a9Yvmfy97762mWl1055GEcO287t49AitVEX78b23kEuvE3rV81THxQ4FtLUNU42i2ElfXh0y4l4q7fJ5IH1iLn15CbfofSU3718jrx8AwcmbvI69r8ugPrhH6wGF7HAp2R+nBU7SDu3GoTufoJl5Bphc+FiFVpGvhUTEs3LDyL65ZTpGubsZ/wHzFjMogTrK2zTygHtQNrOQ1AxAjI/quc453tuActqNlcdPHXm31GJ6wKmjPluPWkuF3neVoQlh39p+/Nw4jeMSnSMXNjwIs4YvRL1ZbthUxos2zceT7XMweKOWciKCS1OOHZ+dLwLVzY/jo9PeB6Ld87EorUXY+dgbRZ25rfcwF9jSx05lHToksO3UoUpphjc4Q2syI7KHZ0HFyyi1sqy8G11q/ivv45Qt3fUDWzdor8i2+8XYnLfFeuKouPCdqLmpphbz8+4L97C1w1y+ZHff/p/RxjQH8gfw/rktpccMB7O6KPkWqNnNxrvqaiAdeIqV5XdUcD0xZnD220LkI1R3FrvL4/tw1HlLTimagcmVu3Gyr3j8Fj7TDhOKc3dNhxfvR7HDd+AERWdeLp1Oh7bMR8DmfINsAfvNWcStH2tAIR+58wROCex8A6Y45AA3cF+FnmIzFEcCQzZTuFPIQ+ROQ7NmC1+auzPjx3I4ww1F/6p7FDPC7DTK8nXF/Smqz+2orNmzoq9UysNuhJnWNpTGQl7AP2UIA/uXICV3U1OKl1Fpkg/hMjAIyy7Hu8977bHOdSMB/mtLEGZ5d7Nxf492XcBN/ap5IzZjD+S8TE5ud3YPR4bu5rb+L4CdmoJwepiMs2av8XcfO95dzz/LcAARmwbQ1K8K3kAAAAASUVORK5CYII=',
    // 프로젝트 테마 식별자
    projectThemeId: 'theme-a',
    // 시나리오 트리
    children: [
      {
        // 시나리오 루트 식별자
        id: 'UI',
        // 시나리오 루트 이름
        name: 'UI',
        // 시나리오 루트 uri
        uri: 'UI',
        children: [
          {
            // 시나리오 식별자
            id: 'ExperimentalScenario',
            name: '시나리오 제목',
            uri: 'testUri',
            // 레이아웃 식별자
            viewerThemeId: 'ExperimentalLayout',
            // 위젯 테마 식별자
            widgetThemeId: 'ThemeOne',
            // 위젯 리스트
            widgetCoreIds: [
              'Widget000',
              'Widget000',
              'Widget000',
              'Widget000',
              'Widget000',
              'Widget000',
            ],
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 작성 권한
            C: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 읽기 권한
            R: true,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 갱신 권한
            U: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 삭제 권한
            D: false,
          },
          {
            id: 'UI-01-01',
            name: 'UI-01-01',
            uri: 'UI-01-01',
            // 레이아웃 식별자
            viewerThemeId: 'LayoutC6C6C4C4C4',
            // 위젯 테마 식별자
            widgetThemeId: 'ThemeOne',
            // 위젯 리스트
            widgetCoreIds: [
              'Widget000',
              'Widget001',
              'Widget002',
              'Widget003',
              'Widget004',
            ],
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 작성 권한
            C: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 읽기 권한
            R: true,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 갱신 권한
            U: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 삭제 권한
            D: false,
          },
        ],
      },
      {
        id: 'UI-02',
        name: 'UI-02',
        // 시나리오 루트 uri
        uri: 'UI-02',
        children: [
          {
            id: 'UI-02-02',
            name: 'UI-02-02',
            uri: 'UI-02-02',
            // 레이아웃 식별자
            viewerThemeId: 'LayoutC6C6C12',
            // 위젯 테마 식별자
            widgetThemeId: 'ThemeOne',
            // 위젯 리스트
            widgetCoreIds: [
              'Widget000',
              'Widget008',
              'Widget009',
            ],
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 작성 권한
            C: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 읽기 권한
            R: true,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 갱신 권한
            U: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 삭제 권한
            D: false,
          },
          {
            id: 'UI-02-03',
            name: 'UI-02-03',
            uri: 'UI-02-03',
            // 노드 타입
            // 레이아웃 식별자
            viewerThemeId: 'LayoutC12',
            // 위젯 테마 식별자
            widgetThemeId: 'ThemeOne',
            // 위젯 리스트
            widgetCoreIds: [
              'Widget010',
            ],
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 작성 권한
            C: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 읽기 권한
            R: true,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 갱신 권한
            U: false,
            // 사용자가 속한 권한그룹의 해당 시나리오에 대한 삭제 권한
            D: false,
          },
        ],
      },
    ],
  },
];
