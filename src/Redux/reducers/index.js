import { combineReducers } from 'redux';
import WidgetCoreListState from './WidgetCoreListReducer';
import WidgetThemeListState from './WidgetThemeListReducer';
import WidgetViewerLayoutListState from './WidgetViewerLayoutListReducer';
import ProjectState from './ProjectReducer';
import ProjectThemeListState from './ProjectThemeListReducer';
import NetWorkModeState from './NetworkModeReducer';
import * as WidgetCores from '../../Components/Widgets/Cores';
import WidgetRefiner from '../../Components/Widgets/Utils/WidgetRefiner';
const { widgetReducers } = new WidgetRefiner(WidgetCores);
import { dashboardReducers } from '../../../../userdashboard/src/Redux/reducers';
import {
  UserManagerReducer,
  AdvencedFilterArgManagerReducer,
} from '../../../../resourcecreator/src/Utils/CommonManagers';
const reducers = {
  ...widgetReducers,
  ...dashboardReducers,
  ...UserManagerReducer,
  ...AdvencedFilterArgManagerReducer,
  WidgetCoreListState,
  WidgetThemeListState,
  WidgetViewerLayoutListState,
  ProjectState,
  ProjectThemeListState,
  NetWorkModeState,
};
export default combineReducers(reducers);
