import * as actionTypes from '../actionTypes';
import _ from 'lodash';

export default function WidgetThemeListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_THEME_LIST_INIT: {
      const { widgetThemeListState } = action.payload;
      finalState = finalState.concat(widgetThemeListState);
    } break;

    case actionTypes.WIDGET_THEME_SELECT: {
      const { displayName } = action.payload;
      finalState = _.map(states, state => {
        const isSelectedWidget = state.displayName === displayName;
        return { ...state, selected: isSelectedWidget };
      });
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
