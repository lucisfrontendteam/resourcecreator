import _ from 'lodash';
import * as actionTypes from '../actionTypes';
import ProjectWriter from '../../Utils/ProjectWriter';

export default function ProjectState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.PROJECT_INIT: {
      const { projectState } = action.payload;
      finalState = { ...projectState };
    } break;

    case actionTypes.PROJECT_THEME_SELECT: {
      const { displayName } = action.payload;
      const projectState = { ProjectState: state };
      finalState = ProjectWriter.setProject(projectState, {
        projectThemeId: displayName,
      });
    } break;

    case actionTypes.WIDGET_SELECT: {
      const { displayName } = action.payload;
      const projectState = { ProjectState: state };
      const targetScenarioObject = ProjectWriter.getSelectedScenarioObject(projectState);
      finalState = ProjectWriter.setScenario(projectState, targetScenarioObject, {
        widgetCoreIds: _.map(targetScenarioObject.model.widgetCoreIds, () => displayName),
      });
    } break;

    case actionTypes.WIDGET_THEME_SELECT: {
      const { displayName } = action.payload;
      const projectState = { ProjectState: state };
      const targetScenarioObject = ProjectWriter.getSelectedScenarioObject(projectState);
      finalState = ProjectWriter.setScenario(projectState, targetScenarioObject, {
        widgetThemeId: displayName,
      });
    } break;

    case actionTypes.WIDGET_VIEWER_THEME_SELECT: {
      const { displayName } = action.payload;
      const projectState = { ProjectState: state };
      const targetScenarioObject = ProjectWriter.getSelectedScenarioObject(projectState);
      finalState = ProjectWriter.setScenario(projectState, targetScenarioObject, {
        viewerThemeId: displayName,
      });
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
