import * as actionTypes from '../actionTypes';
import _ from 'lodash';

export default function ProjectThemeListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.PROJECT_THEME_LIST_INIT: {
      const { projectThemeListState } = action.payload;
      finalState = finalState.concat(projectThemeListState);
    } break;

    case actionTypes.PROJECT_THEME_SELECT: {
      const { displayName } = action.payload;
      finalState = _.map(states, state => {
        const isSelectedProjectTheme = state.displayName === displayName;
        return {
          ...state,
          selected: isSelectedProjectTheme,
        };
      });
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
