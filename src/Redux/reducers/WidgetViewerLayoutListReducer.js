import * as actionTypes from '../actionTypes';
import _ from 'lodash';

export default function WidgetViewerLayoutListState(states = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.WIDGET_VIEWER_THEME_LIST_INIT: {
      const { widgetViewerThemeListState } = action.payload;
      finalState = finalState.concat(widgetViewerThemeListState);
    } break;

    case actionTypes.WIDGET_VIEWER_THEME_SELECT: {
      const { displayName } = action.payload;
      finalState = _.map(states, state => {
        const isSelectedWidget = state.displayName === displayName;
        return { ...state, selected: isSelectedWidget };
      });
    } break;

    default: {
      finalState = finalState.concat(states);
    }
  }

  return finalState;
}
