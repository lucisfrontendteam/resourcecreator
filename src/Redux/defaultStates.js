import * as WidgetCores from '../Components/Widgets/Cores';
import * as WidgetThemes from '../Components/WidgetThemes';
import * as WidgetViewerLayouts from '../Components/WidgetViewerLayouts';
import _ from 'lodash';
import projectSampleDatas from './projectSampleDatas';
import ProjectWriter from '../Utils/ProjectWriter';
import TreeTraveser from '../Utils/TreeTraveser';
import WidgetRefiner from '../Components/Widgets/Utils/WidgetRefiner';

const treeTraveser = new TreeTraveser(projectSampleDatas[0]);
const targetModel = treeTraveser.getNodeById('ExperimentalScenario');
const targetScenario = targetModel.model;
const { widgetCores } = new WidgetRefiner(WidgetCores);

export const widgetCoreListState = _.map(widgetCores, widgetCore => {
  const { displayName, element } = widgetCore;
  const { name, type } = element.defaultProps;
  return {
    constructor: element,
    selected: targetScenario.widgetCoreIds[0] === displayName,
    displayName,
    name,
    type,
  };
});

export const widgetThemeState = _.map(Object.keys(WidgetThemes), displayName => {
  const constructor = WidgetThemes[displayName];
  const { name, type } = constructor.defaultProps;
  return {
    constructor,
    selected: targetScenario.widgetThemeId === displayName,
    displayName,
    name,
    type,
  };
});

export const widgetViewerThemeListState =
  _.map(Object.keys(WidgetViewerLayouts), displayName => {
    const constructor = WidgetViewerLayouts[displayName];
    const { name, type } = constructor.defaultProps;
    return {
      constructor,
      selected: targetScenario.viewerThemeId === displayName,
      displayName,
      name,
      type,
    };
  });

export const projectState = (() => {
  const defaultWidget = 'Widget000';
  const targetProjectState = {
    ProjectState: treeTraveser.treeData,
  };
  const replacement = {
    selected: true,
    widgetCoreIds: _.range(10).map(() => defaultWidget),
  };
  return ProjectWriter.setScenario(targetProjectState, targetModel, replacement);
})();

export const projectThemeListState = _.map(['theme-a', 'theme-b'], displayName => {
  const targetProject = treeTraveser.getRootModel();
  return {
    selected: targetProject.projectThemeId === displayName,
    displayName,
    name: displayName,
    type: 'ProjectTheme',
  };
});

export const networkModeState = {
  label: '오프라인 모드',
  value: false,
};
