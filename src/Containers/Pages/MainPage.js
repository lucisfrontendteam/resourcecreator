import { connect } from 'react-redux';
import { actions } from '../../Redux';
import _ from 'lodash';
import Page from '../../Components/Pages/Page';

const mapStateToProps = (state) => {
  const {
    WidgetCoreListState,
    WidgetThemeListState,
    WidgetViewerLayoutListState,
  } = state;

  const targetWidgetState =
    _.find(WidgetCoreListState, widgetCoreState => widgetCoreState.selected);
  const targetWidgetThemeState
    = _.find(WidgetThemeListState, widgetThemeState => widgetThemeState.selected);
  const targetWidgetViewerThemeState
    = _.find(WidgetViewerLayoutListState,
    widgetViewerThemeState => widgetViewerThemeState.selected);

  return {
    ...state,
    targetWidgetState,
    targetWidgetThemeState,
    targetWidgetViewerThemeState,
  };
};

export const MainPage
  = connect(mapStateToProps, actions)(Page);
