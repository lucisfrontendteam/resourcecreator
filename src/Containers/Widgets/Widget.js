import React, { PureComponent } from 'react';
import WidgetDefaultProps from '../../Components/Widgets/Utils/WidgetDefaultProps';

export default class Widget extends PureComponent {
  static defaultProps = {
    shouldComponentUpdate: WidgetDefaultProps.shouldComponentUpdate,
    render: WidgetDefaultProps.render,
    pagingProps: WidgetDefaultProps.getPagingProps(),
    firePagingAction: WidgetDefaultProps.firePagingAction,
    handlePagingClick: WidgetDefaultProps.handlePagingClick,
    handleCellClick: WidgetDefaultProps.handleCellClick,
  };

  render() {
    const { props } = this;
    const { TargetWidgetTheme, TargetWidgetCore } = props;
    const { name: widgetName } = TargetWidgetCore.defaultProps;

    return (
      <TargetWidgetTheme {...props} name={widgetName}>
        <TargetWidgetCore {...props} />
      </TargetWidgetTheme>
    );
  }
}
