import React, { PureComponent } from 'react';
import ProjectWriter from '../../Utils/ProjectWriter';

export default class SelectedWidgetViewerLayout extends PureComponent {
  get dummyScenario() {
    return {
      model: {
        widgetCoreIds: [],
      },
    };
  }

  get TargetWidgets() {
    const { props } = this;
    const TargetWidgets = ProjectWriter.getTargetWidgetList(props);
    return TargetWidgets;
  }

  get TargetWidgetViewerLayout() {
    const { props } = this;
    const { constructor: TargetWidgetViewerLayout } =
      ProjectWriter.getWidgetViewerLayout(props);
    return TargetWidgetViewerLayout;
  }

  render() {
    const { props, TargetWidgetViewerLayout, TargetWidgets } = this;
    return (
      <TargetWidgetViewerLayout
        {...props}
        TargetWidgets={TargetWidgets}
      />
    );
  }
}
