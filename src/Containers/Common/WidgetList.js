import { connect } from 'react-redux';
import { actions } from '../../Redux';
import ResourceList from '../../Components/Common/ResourceList';

function mapStateToProps(state) {
  return {
    ...state,
    TargetListState: state.WidgetCoreListState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onItemClick(displayName) {
      dispatch(actions.widgetSelectAction(displayName));
    },
  };
}

export const WidgetList
  = connect(mapStateToProps, mapDispatchToProps, null)(ResourceList);
