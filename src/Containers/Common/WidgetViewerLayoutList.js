import { connect } from 'react-redux';
import { actions } from '../../Redux';
import ResourceList from '../../Components/Common/ResourceList';

function mapStateToProps(state) {
  return {
    ...state,
    TargetListState: state.WidgetViewerLayoutListState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onItemClick(displayName) {
      dispatch(actions.widgetViewerThemeSelectAction(displayName));
    },
  };
}

export const WidgetViewerLayoutList
  = connect(mapStateToProps, mapDispatchToProps, null)(ResourceList);
