import { connect } from 'react-redux';
import { actions } from '../../Redux';
import ResourceList from '../../Components/Common/ResourceList';

function mapStateToProps(state) {
  return {
    ...state,
    TargetListState: state.WidgetThemeListState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onItemClick(displayName) {
      dispatch(actions.widgetThemeSelectAction(displayName));
    },
  };
}

export const WidgetThemeList
  = connect(mapStateToProps, mapDispatchToProps, null)(ResourceList);
