import { connect } from 'react-redux';
import { actions } from '../../Redux';
import ResourceList from '../../Components/Common/ResourceList';

function mapStateToProps(state) {
  return {
    ...state,
    TargetListState: state.ProjectThemeListState,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onItemClick(displayName) {
      dispatch(actions.projectThemeSelectAction(displayName));
    },
  };
}

export const ProjectThemeList
  = connect(mapStateToProps, mapDispatchToProps, null)(ResourceList);
