/* ===============================================
Date : 2016-09-01
Description : UI 관련 스크립트 함수 정의
=============================================== */
"use strict";

if(typeof pubUi!=="undefined"){alert("pubUi 변수가 중복사용되고 있습니다");}else{

    var pubUi = {

        /* ==================================================================
            GNB
        ================================================================== */
        gnbAction : function(){
            /* gnb Start */
            var gnb = $(".dep1>li");
            var depth2 = $(".dep2");
            depth2.hide();

            gnb.off('mouseenter focusin').on('mouseenter focusin', function(e) {
                $(this).children('ul').stop().show();
            });
            gnb.find('>a').off('focus').on('focus', function(e) {
                depth2.stop().hide();
                $(this).next('ul').stop().show();
            });
            gnb.off('mouseleave').on('mouseleave', function(e) {
                depth2.stop().hide();
            });
            gnb.off('focusout').on('focusout', function(e) {
                setTimeout(function(){
                    if(gnb.find("a:focus").length == 0){
                        depth2.stop().hide();
                    }
                },0)
            });
        },

        /* ==================================================================
            위젯(box) height control
        ================================================================== */
        boxHeightFix : function(){
            Array.max = function( array ){
                return Math.max.apply( Math, array );
            };

            Array.min = function( array ){
                return Math.min.apply( Math, array );
            };
            var values = [];
            var row = $('.js-row-group');
            row.find('.front').height('auto');
            row.each(function(n){
                $(this).find('.front').each(function(){
                    values.push($(this).innerHeight());
                });
                var absMax = Array.max(values);
                $(this).find('.lucis-flip-container, .front, .back').height(absMax);
                values = [];
            })
        },

        /* ==================================================================
        문서로딩시 실행함수
        ================================================================== */
        init : function(){
            this.gnbAction(); //GNB 함수
            this.boxHeightFix(); //그리드 안에 좌 위젯끼리 높이 맞추기 위한 함수
        }


    };

    $(document).ready(function(){
        pubUi.init(); //문서로딩시 실행
        
        //데이타테이블 호출
        $('.lucis-table').DataTable({
            "searching": false,
            "paging":   false,
            "ordering": false,
            "info":     false
        });
    });
}