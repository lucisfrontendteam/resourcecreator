import React from 'react';
import Widget from '../Containers/Widgets/Widget';
import { ErrorWidgetCore } from '../Components/Widgets/Cores/ErrorWidgetCore';
import TreeTraveser from './TreeTraveser';
import _ from 'lodash';

export default class ProjectWriter {
  static getTargetWidgetList(props) {
    const { widgetCoreIds } = ProjectWriter.getSelectedScenario(props);
    const widgets = _.map(widgetCoreIds,
      widgetCoreId => ProjectWriter.getWidget(props, widgetCoreId));
    return widgets;
  }

  static getWidget(props, widgetCoreId) {
    const { constructor: TargetWidgetTheme } = ProjectWriter.getWidgetTheme(props);
    const widgetCore = ProjectWriter.getWidgetCore(props, widgetCoreId);
    let TargetWidgetCore = {};
    let widgetFound = false;
    if (_.isUndefined(widgetCore)) {
      TargetWidgetCore = ErrorWidgetCore;
      widgetFound = false;
    } else {
      TargetWidgetCore = widgetCore.constructor;
      widgetFound = true;
    }
    const newProps = {
      ...props,
      TargetWidgetTheme,
      TargetWidgetCore,
      widgetCoreId,
      widgetFound,
    };
    const TargetWidget = React.createElement(Widget, newProps);
    return TargetWidget;
  }

  static getWidgetCore(props, widgetCoreId) {
    const TargetWidgetCore = _.find(props.WidgetCoreListState, ({ displayName }) => {
      const tagetWidgetCoreFound = displayName === _.trim(widgetCoreId);
      return tagetWidgetCoreFound;
    });
    return TargetWidgetCore;
  }

  static getWidgetTheme(props) {
    const targetScenario = ProjectWriter.getSelectedScenario(props);
    const TargetWidgetTheme = _.find(props.WidgetThemeListState,
      ({ displayName }) => displayName === _.trim(targetScenario.widgetThemeId));
    return TargetWidgetTheme;
  }

  static getWidgetViewerLayout(props) {
    const targetScenario = ProjectWriter.getSelectedScenario(props);
    const TargetWidgetViewerTheme = _.find(props.WidgetViewerLayoutListState,
      ({ displayName }) => displayName === targetScenario.viewerThemeId);
    if (_.isUndefined(TargetWidgetViewerTheme)) {
      throw new Error(`레이아웃 ${targetScenario.viewerThemeId}를 찾을 수 없습니다.`);
    }
    return TargetWidgetViewerTheme;
  }

  static getSelectedScenarioObject(props) {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const targetModel = treeTraveser.getNodeByPredict(node => node.model.selected);
    if (_.isUndefined(targetModel)) {
      throw new Error('시나리오를 찾을 수 없습니다.');
    }
    return targetModel;
  }

  static getSelectedScenarioBranch(props) {
    const selectedScenario = ProjectWriter.getSelectedScenarioObject(props);
    return selectedScenario.getPath();
  }

  static getScenarioObjectByUri(props, scenarioUri) {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const targetModels = treeTraveser.getNodeListByPredict(node => node.model.uri === scenarioUri);
    return targetModels.pop();
  }

  static getAllScenarioNodeList(props) {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const rootNode = treeTraveser.getRootNode();
    const scenarioNodeList = [];
    _.each(rootNode.children, depth1Node => {
      depth1Node.walk(node => {
        if (_.isUndefined(node.model.children)) {
          scenarioNodeList.push(node);
        }
      });
    });
    return scenarioNodeList;
  }

  static getSelectedScenario(props) {
    return ProjectWriter.getSelectedScenarioObject(props).model;
  }

  static setScenario(props, targetScenarioObject, replacement) {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    treeTraveser.setNode(targetScenarioObject, replacement);
    return treeTraveser.treeData;
  }

  static getFirstScenarioByRootId(props, rootId = '') {
    // 루트 아이티로 가장 첫번째 시나리오를 선택
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const rootNodeModel = treeTraveser.getNodeById(rootId);
    let scenarioNode;
    if (rootNodeModel.hasChildren()) {
      const secondChildren = rootNodeModel.children[0];
      if (secondChildren.hasChildren()) {
        scenarioNode = secondChildren.children[0];
      } else {
        scenarioNode = secondChildren;
      }
    }
    return scenarioNode;
  }

  static selectScenarioByUri(props, scenarioUri = '') {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const rootNode = treeTraveser.getRootNode();
    _.map(rootNode.children, depth1Node => {
      _.map(depth1Node.children, depth2Node => {
        depth2Node.walk(node => {
          const isTarget = (node.model.uri === scenarioUri);
          treeTraveser.setNode(node, { selected: isTarget });
        });
      });
    });
    return treeTraveser.treeData;
  }

  static removeScenario(props, id) {
    const treeTraveser = new TreeTraveser(props.ProjectState);
    const removeNode = treeTraveser.getNodeById(id);
    if (removeNode) {
      removeNode.drop();
    }
    return treeTraveser.treeData;
  }

  static setProject(props, replacement) {
    const { treeData: targetProject } = new TreeTraveser(props.ProjectState);
    return {
      ...targetProject,
      ...replacement,
    };
  }

  static getCiImage(props) {
    let { ci } = props.ProjectState;
    if (_.isEmpty(ci)) {
      // eslint-disable-next-line
      ci = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAE8AAAAWCAYAAACBtcG5AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAABBRJREFUeNrsWFlsTFEYvjPGltqaiti38FIPaolaHoSEB2sQayx9kPJSafFEqiEhYhdJEyXigYTYpRFtqJ20gpIgtlhaQVFaLRrL+P7Jdzn+nBl3pNKp9Eu+mXPOPXf7zn/+5fqCwaDTMqeoleM4eWAfMAvc8iF1UNBRwDz56wb2AGvAe2C5U7/RBSzj+0QENBENZH4F2pV+jqeDg0UfcBO43CKcHDsIPgHPgJfBN+BFsG+Y+80C35MXYky0xmAB+AwsBQdEmNsGzIQGJZz/Cu35fvw0RydNTR5puUAOOEWN+cBh4Hmwt+WcJmBrsmWMiTcKHMF2W3BpBAN4AK4CO3OsGbhNLG86TzbxTlmdCDQjwoO0osXWJ7xS/TKLYcg77bXoI8gO4Ge+5UAj1R/r4WGG1jPxrtFdpYC3aVkmloEZlvPEVaXD5+0NcNv9CR89zHlciy+WpPoSmD6xnUh3YN63Qs3vxcWUHVFCH11puUc8eIz9ORIo2e4JrlTzv/L4aggnPtwJeBSryMML76pF8W6ofj+wmO0TjPguJoFH2e4EbrfslGp5aXAd+M0QL8uY89QQb6Jl942DaHnmgD9MiD6k+mf+YFlXGVDqEh3BwjAuJg5cA+70eK041Rerz9eT/BarEvPMVfnNF1GefkJDUpbJPK8ukUPLi4QUS8Zgg9bElpGExLuuHSnEqrEkiHfwN5AJ8jT6iP70maV1LFxPi8VVcEdopHm43inwkhrbiqxjJehzBwJ0xtH4ridkLGGQ6tcwcRc/NoGL7OKzh+t9py89qc5dwSCU4Yr3SEW1XTEghrx8U6Mfru2K0UaNVYHP2c5nEh8tXnNXSRBZYIynw/oKsRP3+ZUVrcfg1xgQ76HqL6E/SwXbq2MPSBMJDHqLwLtM+l0WRvEcsjALwcNqfLFreWVGoDjyDwXpCu72MG8teIAfKVxMJTWKuXNcV9LdODaB1Nj3F89+m0HRzCND4rnb4DSsrlyVZZJPjVflieSAV8CblsQzEiQhnedhngi8GZzLQBAO31ghuG0JBMdZVoWDWOj6KIXroLat4wbZgKsicNYQbSjruuQIFw0yImWa59YSZFFGU4xEy/Fqph3njLFcCr6DhbvGLSa/VVFGcbluOxVMslzxxJLuc6uYPiOeK9oozIV9LIGSI4hXxYjnROlnHG5HqSxmc/t1YfpxjjndC8u5e2QH0VKGMJA8YwWyX+WiknptNfrl6t2k5t9o+RqUhh0aSmN8/BiahIFiyze8OPqRBOaEcoO3XPlQ+meUO/8L4rkIY9S4FAqp0Omn3w6J5xX8kvw/o4Vj/7grQXUmhCvQFUYDfmGDRbgLTJQLbOVZA35huErU5ZveCCPh/g2BBr1+Qsqul8wXJQ3LZrAJix8CDACE7xh43FafLAAAAABJRU5ErkJggg==';
    }
    return ci;
  }
}
