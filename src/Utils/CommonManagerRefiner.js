import _ from 'lodash';

export default class CommonManagerRefiner {
  constructor(managerResources) {
    this.managerResources = managerResources;
  }

  get CommonManagerManifest() {
    const result = {};
    _.forEach(this.managerResources, (element, key) => {
      if (key.indexOf('Manifest') >= 0) {
        result[key] = element;
      }
    });
    return result;
  }

  get CommonManagerSampleDatas() {
    const result = {};
    _.forEach(this.managerResources, (element, key) => {
      if (key.indexOf('SampleDatas') >= 0) {
        result[key] = element;
      }
    });
    return result;
  }

  get CommonManagerReducers() {
    let result = {};
    _.forEach(this.managerResources, (element, key) => {
      const isReducer = key.indexOf('Reducer') >= 0;
      if (isReducer) {
        result = {
          ...result,
          ...element,
        };
      }
    });
    return result;
  }

  get CommonManagerActionList() {
    let result = {};
    _.forEach(this.managerResources, (element, key) => {
      const isActions = key.indexOf('Actions') >= 0;
      if (isActions) {
        result = {
          ...result,
          ...element,
        };
      }
    });
    return result;
  }

  get CommonManagerActionTypeList() {
    let result = {};
    _.forEach(this.managerResources, (element, key) => {
      const isActions = key.indexOf('ActionTypes') >= 0;
      if (isActions) {
        result = {
          ...result,
          ...element,
        };
      }
    });
    return result;
  }

  get CommonManagerAdapters() {
    const result = {};
    _.forEach(this.managerResources, (element, key) => {
      const isAdappter = key.indexOf('Adapter') >= 0;
      if (isAdappter) {
        result[key] = element;
      }
    });
    return result;
  }
}
