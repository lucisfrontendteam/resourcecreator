/**
 * AdvencedFilter 초기화 샘플 데이터
 */
export const AdvencedFilterArgManagerReadSampleDatas = {
  // 카테고리 필터
  categoryFilterTree: [
    {
      // 카테고리 명
      name: '카테고리 1',
      // 카테고리 식별자 (uuid라도 무방함)
      id: 1,
      type: 'group',
      // 하부 카테고리
      children: [
        // 카테고리 레코드
        {
          name: '카테고리 1-1',
          id: 2,
          type: 'category',
          importantTerm: '안녕하세요 | 오늘은 NEAR 입니다',
          avoidTerm: 'avoidTerm',
        },
        {
          name: '카테고리 1-2',
          id: 3,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 1-2 입니다.',
          avoidTerm: 'avoidTerm',
        },
      ],
    },
    {
      // 카테고리 명
      name: '카테고리 2',
      // 카테고리 식별자 (uuid라도 무방함)
      id: 7,
      type: 'group',
      // 하부 카테고리
      children: [
        // 카테고리 레코드
        {
          name: '카테고리 2-1',
          id: 8,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 2-1 입니다',
          avoidTerm: 'avoidTerm',
        },
        {
          name: '카테고리 2-2',
          id: 9,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 2-2 입니다',
          avoidTerm: 'avoidTerm',
        },
      ],
    },
    {
      // 카테고리 명
      name: '카테고리 3',
      // 카테고리 식별자 (uuid라도 무방함)
      id: 10,
      type: 'group',
      // 하부 카테고리
      children: [
        // 카테고리 레코드
        {
          name: '카테고리 3-1',
          id: 11,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 3-1 입니다',
          avoidTerm: 'avoidTerm',
        },
        {
          name: '카테고리 3-2',
          id: 12,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 3-2 입니다',
          avoidTerm: 'avoidTerm',
        },
      ],
    },
    {
      // 카테고리 명
      name: '카테고리 4',
      // 카테고리 식별자 (uuid라도 무방함)
      id: 13,
      type: 'group',
      // 하부 카테고리
      children: [
        // 카테고리 레코드
        {
          name: '카테고리 4-1',
          id: 14,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 4-1 입니다',
          avoidTerm: 'avoidTerm',
        },
        {
          name: '카테고리 4-2',
          id: 15,
          type: 'category',
          importantTerm: '안녕하세요 | 카테고리 4-2 입니다',
          avoidTerm: 'avoidTerm',
        },
      ],
    },
  ],
  // 고객 상담사 커스텀 데이터 트리
  customDataTree: [
    {
      // 데이터 명
      name: '커스텀 데이터 1',
      // 데이터 식별자 (uuid라도 무방함)
      id: 10,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '데이터 1-1', id: 2 },
        { name: '데이터 1-2', id: 3 },
      ],
    },
    {
      // 데이터 명
      name: '커스텀 데이터 2',
      // 데이터 식별자 (uuid라도 무방함)
      id: 20,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '데이터 2-1', id: 8 },
        { name: '데이터 2-2', id: 9 },
      ],
    },
    {
      // 데이터 명
      name: '커스텀 데이터 3',
      // 데이터 식별자 (uuid라도 무방함)
      id: 30,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '데이터 3-1', id: 11 },
        { name: '데이터 3-2', id: 12 },
      ],
    },
    {
      // 데이터 명
      name: '커스텀 데이터 4',
      // 데이터 식별자 (uuid라도 무방함)
      id: 40,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '데이터 4-1', id: 13 },
        { name: '데이터 4-2', id: 14 },
      ],
    },
    {
      // 데이터 명
      name: '커스텀 데이터 5',
      // 데이터 식별자 (uuid라도 무방함)
      id: 50,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '데이터 5-1', id: 15 },
        { name: '데이터 5-2', id: 16 },
      ],
    },
    {
      // 데이터 명
      name: '커스텀 데이터 6',
      // 데이터 식별자 (uuid라도 무방함)
      id: 60,
      // 하부 데이터
      children: [
        // 데이터 레코드
        { name: '[벨류]', id: 17 },
        { name: '[밸류]', id: 18 },
      ],
    },
  ],
  // 묵음비율 배열
  silenceRatioList: [
    {
      // 비율 식별자
      id: 'silence0',
      // 비율 국문명
      name: '선택',
    },
    {
      // 비율 식별자
      id: 'silence1',
      // 비율 국문명
      name: '10% 미만',
    },
    {
      // 비율 식별자
      id: 'silence2',
      // 비율 국문명
      name: '10% ~ 20%',
    },
    {
      // 비율 식별자
      id: 'silence3',
      // 비율 국문명
      name: '20% ~ 30%',
    },
    {
      // 비율 식별자
      id: 'silence4',
      // 비율 국문명
      name: '30% ~ 40%',
    },
    {
      // 비율 식별자
      id: 'silence5',
      // 비율 국문명
      name: '40% ~ 50%',
    },
    {
      // 비율 식별자
      id: 'silence6',
      // 비율 국문명
      name: '50% ~ 60%',
    },
    {
      // 비율 식별자
      id: 'silence7',
      // 비율 국문명
      name: '60% ~ 70%',
    },
    {
      // 비율 식별자
      id: 'silence8',
      // 비율 국문명
      name: '70% ~ 80%',
    },
    {
      // 비율 식별자
      id: 'silence9',
      // 비율 국문명
      name: '80% 이상',
    },
  ],
  // // 상담사 발화비율 배열
  // csSpeechRatioList: [
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech0',
  //     // 비율 국문명
  //     name: '선택',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech1',
  //     // 비율 국문명
  //     name: '10% 미만',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech2',
  //     // 비율 국문명
  //     name: '10% ~ 20%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech3',
  //     // 비율 국문명
  //     name: '20% ~ 30%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech4',
  //     // 비율 국문명
  //     name: '30% ~ 40%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech5',
  //     // 비율 국문명
  //     name: '40% ~ 50%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech6',
  //     // 비율 국문명
  //     name: '50% ~ 60%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech7',
  //     // 비율 국문명
  //     name: '60% ~ 70%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech8',
  //     // 비율 국문명
  //     name: '70% ~ 80%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech9',
  //     // 비율 국문명
  //     name: '80% 이상',
  //   },
  // ],
  // // 고객 발화비율 배열
  // clientSpeechRatioList: [
  //   {
  //     // 비율 식별자
  //     id: 'clientSpeech0',
  //     // 비율 국문명
  //     name: '선택',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'clientSpeech1',
  //     // 비율 국문명
  //     name: '10% 미만',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'clientSpeech2',
  //     // 비율 국문명
  //     name: '10% ~ 20%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'clientSpeech3',
  //     // 비율 국문명
  //     name: '20% ~ 30%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech4',
  //     // 비율 국문명
  //     name: '30% ~ 40%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech5',
  //     // 비율 국문명
  //     name: '40% ~ 50%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech6',
  //     // 비율 국문명
  //     name: '50% ~ 60%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech7',
  //     // 비율 국문명
  //     name: '60% ~ 70%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech8',
  //     // 비율 국문명
  //     name: '70% ~ 80%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'csSpeech9',
  //     // 비율 국문명
  //     name: '80% 이상',
  //   },
  // ],
  // // 말겹침 비율 배열
  // dupsRatioList: [
  //   {
  //     // 비율 식별자
  //     id: 'dups0',
  //     // 비율 국문명
  //     name: '선택',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups1',
  //     // 비율 국문명
  //     name: '10% 미만',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups2',
  //     // 비율 국문명
  //     name: '10% ~ 20%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups3',
  //     // 비율 국문명
  //     name: '20% ~ 30%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups4',
  //     // 비율 국문명
  //     name: '30% ~ 40%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups5',
  //     // 비율 국문명
  //     name: '40% ~ 50%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups6',
  //     // 비율 국문명
  //     name: '50% ~ 60%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups7',
  //     // 비율 국문명
  //     name: '60% ~ 70%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups8',
  //     // 비율 국문명
  //     name: '70% ~ 80%',
  //   },
  //   {
  //     // 비율 식별자
  //     id: 'dups9',
  //     // 비율 국문명
  //     name: '80% 이상',
  //   },
  // ],
  // 호 방향 배열
  hoDirectionList: [
    {
      // 호 방향 식별자
      id: 0,
      // 호 방향 국문명
      name: '선택',
    },
    {
      // 호 방향 식별자
      id: 1,
      // 호 방향 국문명
      name: '인바운드',
    },
    {
      // 호 방향 식별자
      id: 2,
      // 호 방향 국문명
      name: '아웃바운드',
    },
  ],
  // 통화시간 배열
  intervalList: [
    {
      // 통화시간 식별자
      id: 0,
      // 통화시간 국문명
      name: '선택',
    },
    {
      // 통화시간 식별자
      id: 1,
      // 통화시간 국문명
      name: '1분 미만',
    },
    {
      // 통화시간 식별자
      id: 2,
      // 통화시간 국문명
      name: '1분 ~ 3분',
    },
    {
      // 통화시간 식별자
      id: 3,
      // 통화시간 국문명
      name: '3분 ~ 5분',
    },
    {
      // 통화시간 식별자
      id: 4,
      // 통화시간 국문명
      name: '5분 ~ 7분',
    },
    {
      // 통화시간 식별자
      id: 5,
      // 통화시간 국문명
      name: '7분 ~ 10분',
    },
    {
      // 통화시간 식별자
      id: 6,
      // 통화시간 국문명
      name: '10분 ~ 15분',
    },
    {
      // 통화시간 식별자
      id: 7,
      // 통화시간 국문명
      name: '15분 ~ 20분',
    },
    {
      // 통화시간 식별자
      id: 8,
      // 통화시간 국문명
      name: '20분 ~ 30분',
    },
    {
      // 통화시간 식별자
      id: 9,
      // 통화시간 국문명
      name: '30분 ~ 50분',
    },
    {
      // 통화시간 식별자
      id: 10,
      // 통화시간 국문명
      name: '50분 이상',
    },
  ],
  // 시간대 배열
  timeSlotList: [
    {
      // 타임슬롯 식별자
      id: 0,
      // 타임슬롯 국문명
      name: '선택',
    },
    {
      // 타임슬롯 식별자
      id: 1,
      // 타임슬롯 국문명
      name: '아침',
    },
    {
      // 타임슬롯 식별자
      id: 2,
      // 타임슬롯 국문명
      name: '점심',
    },
    {
      // 타임슬롯 식별자
      id: 3,
      // 타임슬롯 국문명
      name: '저녁',
    },
  ],
  // 조직도 트리
  organizationTree: [
    {
      // 조직명
      name: '업무지원팀',
      // 조직명 식별자 (uuid라도 무방함)
      id: 1,
      // 하부 조직
      children: [
        // 조직원 레코드
        { name: '대장님', number: 324, id: 2 },
        { name: '쫄병', number: 345, id: 3 },
        {
          name: '업무지원팀 하청',
          id: 4,
          children: [
            { name: '하청 대장님', number: 356, id: 5 },
            { name: '쫄쫄병', number: 3556, id: 6 },
          ],
        },
      ],
    },
    {
      name: '개발팀',
      id: 7,
      children: [
        { name: '왕대장', number: 3056, id: 8 },
        {
          name: '개발팀 하청',
          id: 9,
          children: [
            { name: '그냥 대장', number: 3456, id: 10 },
            { name: '쫄쫄쫄병', number: 3956, id: 11 },
          ],
        },
      ],
    },
  ],
};
