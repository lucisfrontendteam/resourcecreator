export const AdvencedFilterArgManagerCategoryGroupAddManifest = {
  id: 'AdvencedFilterArgManagerCategoryGroupAdd',
  stateId: 'AdvencedFilterArgManager',
  name: '카테고리 그룹 메니져',
  usage: '추가하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'AdvencedFilterArgManagerCategoryGroupAddUserId',
    },
    {
      key: 'projectId',
      id: 'AdvencedFilterArgManagerCategoryGroupAddProjectId',
    },
    {
      key: 'categoryGroupName',
      id: 'AdvencedFilterArgManagerCategoryGroupAddCategoryGroupName',
    },
  ],
  method: 'post',
  apiUrl: '/project/categoryGroup.api',
};
