export const AdvencedFilterArgManagerCategoryMoveManifest = {
  id: 'AdvencedFilterArgManagerCategoryMove',
  stateId: 'AdvencedFilterArgManager',
  name: '카테고리 그룹 메니져',
  usage: '이동하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'AdvencedFilterArgManagerCategoryMoveUserId',
    },
    {
      key: 'projectId',
      id: 'AdvencedFilterArgManagerCategoryMoveProjectId',
    },
    {
      key: 'categoryFilterTree',
      id: 'AdvencedFilterArgManagerCategoryMoveCategorycategoryFilterTree',
      json: [
        {
          name: '카테고리 1',
          id: 1,
          type: 'group',
          children: [
            {
              name: '카테고리 1-1',
              id: 2,
              type: 'category',
              importantTerm: '안녕하세요 | 오늘은 NEAR 입니다',
              avoidTerm: 'avoidTerm',
            },
            {
              id: 3,
              name: '카테고리 1-2',
              type: 'category',
            },
            {
              id: 8,
              name: '카테고리 2-1',
              type: 'category',
            },
            {
              id: 9,
              name: '카테고리 2-2',
              type: 'category',
            },
          ],
        },
        {
          name: '카테고리 2',
          id: 7,
          type: 'group',
          children: [
            {
              name: '카테고리 2-1',
              id: 8,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 2-1 입니다',
              avoidTerm: 'avoidTerm',
            },
            {
              name: '카테고리 2-2',
              id: 9,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 2-2 입니다',
              avoidTerm: 'avoidTerm',
            },
          ],
        },
        {
          name: '카테고리 3',
          id: 10,
          type: 'group',
          children: [
            {
              name: '카테고리 3-1',
              id: 11,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 3-1 입니다',
              avoidTerm: 'avoidTerm',
            },
            {
              name: '카테고리 3-2',
              id: 12,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 3-2 입니다',
              avoidTerm: 'avoidTerm',
            },
          ],
        },
        {
          name: '카테고리 4',
          id: 13,
          type: 'group',
          children: [
            {
              name: '카테고리 4-1',
              id: 14,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 4-1 입니다',
              avoidTerm: 'avoidTerm',
            },
            {
              name: '카테고리 4-2',
              id: 15,
              type: 'category',
              importantTerm: '안녕하세요 | 카테고리 4-2 입니다',
              avoidTerm: 'avoidTerm',
            },
          ],
        },
      ],
      rows: 20,
    },
  ],
  method: 'patch',
  apiUrl: '/project/category.api',
};
