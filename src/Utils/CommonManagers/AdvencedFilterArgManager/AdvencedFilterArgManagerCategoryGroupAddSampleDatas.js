/**
 * 카테고리 그룹명 변경시의 성공 및 실패 샘플 데이터
 */
export const AdvencedFilterArgManagerCategoryGroupAddSampleDatas = {
  // 카테고리 그룹 이름 추가 성공시
  success: {
    // 성공 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 카테고리 트리
    categoryFilterTree: [
      {
        // 카테고리 명
        name: '카테고리 1',
        // 카테고리 식별자 (uuid라도 무방함)
        id: 1,
        type: 'group',
        // 하부 카테고리
        children: [
          // 카테고리 레코드
          {
            name: '카테고리 1-1',
            id: 2,
            type: 'category',
            importantTerm: '안녕하세요 | 오늘은 NEAR 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 1-2',
            id: 3,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 1-2 입니다.',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
      {
        // 카테고리 명
        name: '카테고리 2',
        // 카테고리 식별자 (uuid라도 무방함)
        id: 7,
        type: 'group',
        // 하부 카테고리
        children: [
          // 카테고리 레코드
          {
            name: '카테고리 2-1',
            id: 8,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 2-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 2-2',
            id: 9,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 2-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
      {
        // 카테고리 명
        name: '카테고리 3',
        // 카테고리 식별자 (uuid라도 무방함)
        id: 10,
        type: 'group',
        // 하부 카테고리
        children: [
          // 카테고리 레코드
          {
            name: '카테고리 3-1',
            id: 11,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 3-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 3-2',
            id: 12,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 3-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
      {
        // 카테고리 명
        name: '카테고리 4',
        // 카테고리 식별자 (uuid라도 무방함)
        id: 13,
        type: 'group',
        // 하부 카테고리
        children: [
          // 카테고리 레코드
          {
            name: '카테고리 4-1',
            id: 14,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 4-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 4-2',
            id: 15,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 4-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
    ],
  },
  // 카테고리 그룹 이름 추가 실패시
  fail: {
    // 성공 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '카테고리 그룹 추가에 실패했습니다.',
    // 카테고리 그룹 데이터
    categoryGroup: {
      tempId: '90a81955-8785-4e73-9318-1d2a6832dffe',
      id: '1234567890',
      name: '추가된 카테고리 그룹의 이름',
    },
  },
};
