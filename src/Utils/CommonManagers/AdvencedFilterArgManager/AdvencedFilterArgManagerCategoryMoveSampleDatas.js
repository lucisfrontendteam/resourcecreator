/**
 * 카테고리 이동시의 성공 및 실패 샘플 데이터
 */
export const AdvencedFilterArgManagerCategoryMoveSampleDatas = {
  // 카테고리 그룹 이름 추가 성공시
  success: {
    // 성공 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 카테고리 그룹 데이터
    categoryFilterTree: [
      {
        name: '카테고리 1',
        id: 1,
        type: 'group',
        children: [
          {
            name: '카테고리 1-1',
            id: 2,
            type: 'category',
            importantTerm: '안녕하세요 | 오늘은 NEAR 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            id: 3,
            name: '카테고리 1-2',
            type: 'category',
          },
          {
            id: 8,
            name: '카테고리 2-1',
            type: 'category',
          },
          {
            id: 9,
            name: '카테고리 2-2',
            type: 'category',
          },
        ],
      },
      {
        name: '카테고리 2',
        id: 7,
        type: 'group',
        children: [
          {
            name: '카테고리 2-1',
            id: 8,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 2-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 2-2',
            id: 9,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 2-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
      {
        name: '카테고리 3',
        id: 10,
        type: 'group',
        children: [
          {
            name: '카테고리 3-1',
            id: 11,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 3-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 3-2',
            id: 12,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 3-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
      {
        name: '카테고리 4',
        id: 13,
        type: 'group',
        children: [
          {
            name: '카테고리 4-1',
            id: 14,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 4-1 입니다',
            avoidTerm: 'avoidTerm',
          },
          {
            name: '카테고리 4-2',
            id: 15,
            type: 'category',
            importantTerm: '안녕하세요 | 카테고리 4-2 입니다',
            avoidTerm: 'avoidTerm',
          },
        ],
      },
    ],
  },
  // 카테고리 그룹 이름 추가 실패시
  fail: {
    // 성공 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '카테고리를 그룹으로 이동시키는데 실패했습니다.',
  },
};
