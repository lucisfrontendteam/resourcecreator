export const AdvencedFilterArgManagerReadManifest = {
  id: 'AdvencedFilterArgManagerRead',
  stateId: 'AdvencedFilterArgManager',
  name: '필터 어규먼트 메니져',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'advencedFilterArg',
    },
  ],
  method: 'get',
  apiUrl: '/project/filterArgs.api',
};
