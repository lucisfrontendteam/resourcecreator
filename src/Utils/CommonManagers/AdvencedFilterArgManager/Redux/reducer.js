import _ from 'lodash';
import * as actionTypes from './actionTypes';

export function AdvencedFilterArgManagerState(state = [], action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.ADVENCED_FILTER_ARG_STATE_INIT: {
      const { widgetState } = action.payload;
      const { AdvencedFilterArgManagerState: targetState } = widgetState;
      if (targetState) {
        finalState = { ...targetState };
      } else {
        finalState = { ...widgetState };
      }
    } break;

    case actionTypes.CATEGORY_GROUP_EDIT_START_MODE: {
      const { groupId } = action.payload;
      const categoryFilterTree = _.map(state.categoryFilterTree, categoryGroup => {
        const newCategoryGroup = {
          ...categoryGroup,
          editMode: groupId === categoryGroup.id,
        };
        return newCategoryGroup;
      });
      finalState = {
        ...state,
        categoryFilterTree,
      };
    } break;

    case actionTypes.CATEGORY_GROUP_EDIT_END_MODE: {
      const { widgetState } = action.payload;
      const { categoryGroup: modifiedGroup } = widgetState;
      if (_.isEmpty(modifiedGroup)) {
        finalState = { ...widgetState };
      } else {
        const categoryFilterTree = _.map(state.categoryFilterTree, categoryGroup => {
          let newCategoryGroup = {};
          if (modifiedGroup.id === categoryGroup.id) {
            newCategoryGroup = {
              ...categoryGroup,
              name: modifiedGroup.name,
              editMode: false,
            };
          } else {
            newCategoryGroup = { ...categoryGroup };
          }
          return newCategoryGroup;
        });
        finalState = {
          ...state,
          categoryFilterTree,
        };
      }
    } break;

    case actionTypes.CATEGORY_GROUP_ADD: {
      const { widgetState } = action.payload;
      const { categoryFilterTree = [] } = widgetState;
      finalState = {
        ...state,
        categoryFilterTree,
      };
    } break;

    case actionTypes.CATEGORY_GROUP_REMOVE: {
      const { widgetState } = action.payload;
      const { stateRemoveAfter } = widgetState;
      if (_.isEmpty(stateRemoveAfter)) {
        finalState = { ...widgetState };
      } else {
        finalState = {
          ...state,
          categoryFilterTree: stateRemoveAfter,
        };
      }
    } break;

    case actionTypes.CATEGORY_MOVE2GROUP: {
      const { widgetState } = action.payload;
      const { categoryFilterTree } = widgetState;

      finalState = {
        ...state,
        categoryFilterTree,
      };
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
