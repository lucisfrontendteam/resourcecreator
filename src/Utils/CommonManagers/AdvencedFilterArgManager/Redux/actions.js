import { v4 } from 'uuid';
import _ from 'lodash';
import * as actionTypes from './actionTypes';
import {
  AdvencedFilterArgManagerReadManifest,
  AdvencedFilterArgManagerReadSampleDatas,
  AdvencedFilterArgManagerCategoryMoveManifest,
  AdvencedFilterArgManagerCategoryMoveSampleDatas,
  AdvencedFilterArgManagerCategoryGroupEditManifest,
  AdvencedFilterArgManagerCategoryGroupEditSampleDatas,
  AdvencedFilterArgManagerCategoryGroupAddManifest,
  AdvencedFilterArgManagerCategoryGroupAddSampleDatas,
  AdvencedFilterArgManagerCategoryRemoveManifest,
  AdvencedFilterArgManagerCategoryRemoveSampleDatas,
} from '../index';
import AjaxManager from '../../../AjaxManager';

export function setCategoryGroupEditMode(groupId) {
  return {
    type: actionTypes.CATEGORY_GROUP_EDIT_START_MODE,
    payload: { groupId },
  };
}

function getUniqCategories(props, groupId, categoryNodeList, position, prevGroupId) {
  const { categoryFilterTree } = props.AdvencedFilterArgManagerState;
  let newCategoryFilterTree = _.map(categoryFilterTree, node => {
    const newNode = { ...node };
    if (newNode.id === groupId) {
      if (_.isArray(newNode.children)) {
        newNode.children.splice(position, 0, ...categoryNodeList);
      } else {
        newNode.children = [...categoryNodeList];
      }
      const uniqCategories = _.uniqBy(newNode.children, child => child.id);
      newNode.children = [...uniqCategories];
    }
    return newNode;
  });

  if (prevGroupId) {
    newCategoryFilterTree = _.map(newCategoryFilterTree, node => {
      const newNode = { ...node };
      if (newNode.id === prevGroupId) {
        const targetNodeIds = _.map(categoryNodeList, target => target.id);
        const newChildren
          = _.filter(newNode.children, child => !!!_.includes(targetNodeIds, child.id));
        newNode.children = newChildren;
      }
      return newNode;
    });
  }
  return newCategoryFilterTree;
}

function getChangedCategoryGroups(categoryFilterTree, groupId, prevGroupId) {
  const newCategoryFilterTree = _.filter(categoryFilterTree, targetGroup => {
    const isPlacedGroupId = groupId === targetGroup.id;
    const isPrevGroupId = prevGroupId === targetGroup.id;
    return isPlacedGroupId || isPrevGroupId;
  });
  return newCategoryFilterTree;
}

export function advencedfilterargmanagercategorymoveInitAction(
  props, groupId, categoryNodeList, position, prevGroupId
) {
  return dispatch => {
    const categoryFilterTree
      = getUniqCategories(props, groupId, categoryNodeList, position, prevGroupId);
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = AdvencedFilterArgManagerCategoryMoveManifest;
    const query = {
      ...QueryIdMap,
      categoryFilterTree: getChangedCategoryGroups(categoryFilterTree, groupId, prevGroupId),
    };
    const stringifyQuery = JSON.stringify(query);
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.CATEGORY_MOVE2GROUP,
      query: stringifyQuery,
      ajaxType: 'json',
      sampleData: { ...AdvencedFilterArgManagerCategoryMoveSampleDatas },
      offline() {
        dispatch({
          type: actionTypes.CATEGORY_MOVE2GROUP,
          payload: {
            widgetState: {
              categoryFilterTree,
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function advencedfilterargmanagerreadInitAction(props, getAll = false) {
  return dispatch => {
    const { NetWorkModeState, LoginManagerState, QueryIdMap } = props;
    let query = {};
    if (getAll) {
      query = {
        ...LoginManagerState,
        userId: '',
      };
    } else {
      query = {
        ...LoginManagerState,
        ...QueryIdMap,
      };
    }
    const { method, apiUrl } = AdvencedFilterArgManagerReadManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.ADVENCED_FILTER_ARG_STATE_INIT,
      query,
      ajaxType: 'normal',
      beforeDone(data) {
        return { AdvencedFilterArgManagerState: data };
      },
      offline() {
        dispatch({
          type: actionTypes.ADVENCED_FILTER_ARG_STATE_INIT,
          payload: {
            widgetState: {
              AdvencedFilterArgManagerState: {
                ...AdvencedFilterArgManagerReadSampleDatas,
                ...props.AdvencedFilterArgManagerState,
              },
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function advencedfilterargmanagercategorygroupeditInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = AdvencedFilterArgManagerCategoryGroupEditManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.CATEGORY_GROUP_EDIT_END_MODE,
      query: QueryIdMap,
      ajaxType: 'normal',
      sampleData: { ...AdvencedFilterArgManagerCategoryGroupEditSampleDatas },
      beforeDone(data) {
        const { categoryGroup } = data;
        return { categoryGroup };
      },
      offline() {
        const {
          categoryGroupId,
          categoryGroupName,
        } = QueryIdMap;
        dispatch({
          type: actionTypes.CATEGORY_GROUP_EDIT_END_MODE,
          payload: {
            widgetState: {
              categoryGroup: {
                id: categoryGroupId,
                name: categoryGroupName,
              },
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function advencedfilterargmanagercategorygroupaddInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = AdvencedFilterArgManagerCategoryGroupAddManifest;
    const { success } = AdvencedFilterArgManagerCategoryGroupAddSampleDatas;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.CATEGORY_GROUP_ADD,
      query: QueryIdMap,
      ajaxType: 'normal',
      offline() {
        const { categoryGroupName } = QueryIdMap;
        dispatch({
          type: actionTypes.CATEGORY_GROUP_ADD,
          payload: {
            widgetState: {
              categoryFilterTree: [
                {
                  id: v4(),
                  name: categoryGroupName,
                },
                ...success.categoryFilterTree,
              ],
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function advencedfilterargmanagercategoryremoveInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = AdvencedFilterArgManagerCategoryRemoveManifest;
    const { removeNodes, stateRemoveAfter } = QueryIdMap;
    const stringifyQuery = JSON.stringify(QueryIdMap);
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.CATEGORY_GROUP_REMOVE,
      query: stringifyQuery,
      ajaxType: 'json',
      sampleData: { ...AdvencedFilterArgManagerCategoryRemoveSampleDatas },
      beforeDone(data) {
        return {
          ...data,
          removeNodes,
          stateRemoveAfter,
        };
      },
      offline() {
        dispatch({
          type: actionTypes.CATEGORY_GROUP_REMOVE,
          payload: {
            widgetState: {
              status: true,
              showError: false,
              removeNodes,
              stateRemoveAfter,
            },
          },
        });
      },
    };

    AjaxManager.run(param);
  };
}
