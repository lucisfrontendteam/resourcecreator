export const AdvencedFilterArgManagerCategoryGroupEditManifest = {
  id: 'AdvencedFilterArgManagerCategoryGroupEdit',
  stateId: 'AdvencedFilterArgManager',
  name: '카테고리 그룹 메니져',
  usage: '수정하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'advencedFilterArgManagerCategoryGroupEditUserId',
    },
    {
      key: 'projectId',
      id: 'advencedFilterArgManagerCategoryGroupEditProjectId',
    },
    {
      key: 'categoryGroupId',
      id: 'advencedFilterArgManagerCategoryGroupEditCategoryGroupId',
    },
    {
      key: 'categoryGroupName',
      id: 'advencedFilterArgManagerCategoryGroupEditCategoryGroupName',
    },
  ],
  method: 'patch',
  apiUrl: '/project/categoryGroup.api',
};
