export const AdvencedFilterArgManagerCategoryRemoveManifest = {
  id: 'AdvencedFilterArgManagerCategoryRemove',
  stateId: 'AdvencedFilterArgManager',
  name: '필터 어규먼트 메니져',
  usage: '삭제하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'AdvencedFilterArgManagerCategoryRemoveUserId',
    },
    {
      key: 'projectId',
      id: 'AdvencedFilterArgManagerCategoryRemoveProjectId',
    },
    {
      key: 'removeNodes',
      id: 'AdvencedFilterArgManagerCategoryRemoveRemoveNodes',
      json: [
        {
          id: '1',
          type: 'group',
        },
        {
          id: '2',
          type: 'category',
          parentId: '1',
        },
        {
          id: '3',
          type: 'categor',
          parentId: '1',
        },
      ],
      rows: 12,
    },
  ],
  method: 'delete',
  apiUrl: '/project/category.api',
};
