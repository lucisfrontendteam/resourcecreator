/**
 * 카테고리 그룹명 변경시의 성공 및 실패 샘플 데이터
 */
export const AdvencedFilterArgManagerCategoryGroupEditSampleDatas = {
  // 카테고리 그룹 이름 변경 성공시
  success: {
    // 성공 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 카테고리 그룹 데이터
    categoryGroup: {
      id: '1234567890',
      name: '변경된 카테고리 그룹의 이름',
    },
  },
  // 카테고리 그룹 이름 변경 실패시
  fail: {
    // 성공 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '카테고리 그룹의 이름 변경에 실패했습니다.',
    // 카테고리 그룹 데이터
    categoryGroup: {
      id: '1234567890',
      name: '변경된 카테고리 그룹의 이름',
    },
  },
};
