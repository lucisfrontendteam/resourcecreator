/**
 * 로그인 성공 / 실패시의 샘플 데이터
 */
export const LoginManagerLoginSampleDatas = {
  // 로그인 성공시
  success: {
    // 로그인 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 사용자 아이디
    userId: 'root',
    // 사용자 국문명
    userName: '관리자',
  },
  // 로그인 실패시
  fail: {
    // 로그인 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '아이디 또는 패스워드가 맞지 않습니다.',
  },
};
