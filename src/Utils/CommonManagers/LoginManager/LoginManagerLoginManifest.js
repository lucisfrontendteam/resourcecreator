export const LoginManagerLoginManifest = {
  id: 'LoginManagerLogin',
  stateId: 'LoginManager',
  name: '로그인 메니져',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'loginId',
    },
    {
      key: 'userPass',
      id: 'loginPass',
    },
    {
      key: 'programId',
      id: 'loginprogramId',
    },
  ],
  method: 'post',
  apiUrl: '/user/login.api',
};
