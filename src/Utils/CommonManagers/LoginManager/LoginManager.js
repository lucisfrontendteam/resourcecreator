export class LoginManager {
  constructor(props) {
    this.props = props;
  }

  logIn() {
    // 서버에 계정 정보를 요청
    const { props } = this;
    props.loginmanagerloginInitAction(props);
  }

  logOut() {
    // 서버에 로그아웃을 요청
    const { props } = this;
    props.loginmanagerlogoutInitAction(props);
  }
}
