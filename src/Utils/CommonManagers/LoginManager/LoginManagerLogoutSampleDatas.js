/**
 * 로그아웃 성공 / 실패시의 샘플 데이터
 */
export const LoginManagerLogoutSampleDatas = {
  // 로그아웃 성공시
  success: {
    // 로그인 여부
    status: false,
    // 에러메세지 표시여부
    showError: false,
    // 상태 메시지
    message: '로그아웃에 성공했습니다.',
  },
  // 로그아웃 실패시
  fail: {
    // 로그인 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '로그아웃에 실패했습니다.',
  },
};
