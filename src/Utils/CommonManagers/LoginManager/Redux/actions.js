import * as actionTypes from './actionTypes';
const rootUser = {
  userId: 'root',
  userPass: 'root',
};
const userState = {
  // 로그인 여부
  status: false,
  statusText: '',
  // 에러메세지 표시여부
  showError: false,
  userId: '',
  userPass: '',
};
import {
  LoginManagerLoginSampleDatas,
  LoginManagerLogoutSampleDatas,
  LoginManagerLoginManifest,
  LoginManagerLogoutManifest,
} from '../index';
import AjaxManager from '../../../AjaxManager';

export function loginmanagerInitAction(
  widgetState = userState
) {
  return {
    type: actionTypes.USER_STATE_INIT,
    payload: { widgetState },
  };
}

export function loginmanagerloginInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, LoginManagerState } = props;
    const { method, apiUrl } = LoginManagerLoginManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.LOGIN,
      query: LoginManagerState,
      ajaxType: 'normal',
      offline(params) {
        const { userId, userPass } = params.query;
        const { userId: rootId, userPass: rootPass } = rootUser;
        const { success, fail } = LoginManagerLoginSampleDatas;
        if ((userId === rootId) && (userPass === rootPass)) {
          dispatch({
            type: actionTypes.LOGIN,
            payload: {
              widgetState: {
                ...success,
                showError: false,
              },
            },
          });
        } else {
          dispatch({
            type: actionTypes.LOGIN,
            payload: {
              widgetState: {
                ...fail,
                showError: true,
              },
            },
          });
        }
      },
    };
    AjaxManager.run(param);
  };
}

export function loginmanagerlogoutInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, LoginManagerState } = props;
    const { method, apiUrl } = LoginManagerLogoutManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.LOGOUT,
      query: LoginManagerState,
      ajaxType: 'normal',
      sampleData: LoginManagerLogoutSampleDatas.sucess,
    };
    AjaxManager.run(param);
  };
}
