import * as actionTypes from './actionTypes';

export function LoginManagerState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.USER_STATE_INIT:
    case actionTypes.LOGIN:
    case actionTypes.LOGOUT: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    default: {
      finalState = { ...state };
    }
  }
  return finalState;
}
