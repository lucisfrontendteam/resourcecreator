import * as actionTypes from './actionTypes';
import {
  CategoryListManagerReadManifest,
  CategoryListManagerReadSampleDatas,
} from '../index';
import AjaxManager from '../../../AjaxManager';

export function categorylistmanagerreadInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, LoginManagerState } = props;
    const { method, apiUrl }
      = CategoryListManagerReadManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.CATEGORY_LIST_INIT,
      query: LoginManagerState,
      ajaxType: 'normal',
      beforeDone(data) {
        return { categoryList: data };
      },
      offline() {
        dispatch({
          type: actionTypes.CATEGORY_LIST_INIT,
          payload: {
            widgetState: {
              categoryList: CategoryListManagerReadSampleDatas,
            },
          },
        });
      },
    };

    AjaxManager.run(param);
  };
}
