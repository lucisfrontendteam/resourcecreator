import _ from 'lodash';
import * as actionTypes from './actionTypes';

export function CategoryListManagerState(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.CATEGORY_LIST_INIT: {
      const { widgetState } = action.payload;
      const { categoryList } = widgetState;
      if (_.isEmpty(categoryList)) {
        finalState = _.map(widgetState, (value, key) => {
          const errorField = {};
          errorField[key] = value;
          return errorField;
        });
      } else {
        finalState = [...categoryList];
      }
    } break;

    default: {
      finalState = [...state];
    }
  }

  return finalState;
}
