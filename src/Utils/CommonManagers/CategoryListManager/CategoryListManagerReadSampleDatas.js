/* eslint-disable */
/**
 * 카테고리 리스트 샘플 데이터
 */
export const CategoryListManagerReadSampleDatas = [
  { name: '카테고리 1-1', id: 2, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 1-2', id: 3, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 2-1', id: 8, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 2-2', id: 9, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 3-1', id: 11, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 3-2', id: 12, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 4-1', id: 14, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 4-2', id: 15, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-1', id: 16, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-2', id: 17, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-3', id: 18, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-4', id: 19, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-5', id: 20, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-6', id: 21, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-7', id: 22, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-8', id: 23, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-9', id: 24, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
  { name: '카테고리 5-10', id: 25, type: 'category', importantTerm: '안녕하세요 | 오늘은 NEAR 입니다', avoidTerm: 'avoidTerm' },
];
