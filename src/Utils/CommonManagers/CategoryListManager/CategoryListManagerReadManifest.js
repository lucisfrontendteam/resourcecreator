export const CategoryListManagerReadManifest = {
  id: 'CategoryListManagerRead',
  stateId: 'CategoryListManager',
  name: '카테고리 리스트 메니져',
  usage: '읽기',
  type: 'LoginId',
  inputIdList: [
    {
      key: 'userId',
      id: 'categoryListManagerReadUserId',
    },
  ],
  method: 'get',
  apiUrl: '/project/category.api',
};
