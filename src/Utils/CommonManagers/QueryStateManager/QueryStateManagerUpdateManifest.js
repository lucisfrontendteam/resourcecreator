export const QueryStateManagerUpdateManifest = {
  id: 'QueryStateManagerUpdate',
  stateId: 'QueryStateManager',
  name: '쿼리 메니져',
  usage: '수정하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'projectManagerUserId',
    },
    {
      key: 'projectId',
      id: 'projectManagerProjectId',
    },
    {
      key: 'scenarioId',
      id: 'projectManagerScenarioId',
    },
    {
      key: 'tabId',
      id: 'projectManagerTabId',
    },
  ],
  method: 'patch',
  apiUrl: '/project/scenarioQuery.api',
};
