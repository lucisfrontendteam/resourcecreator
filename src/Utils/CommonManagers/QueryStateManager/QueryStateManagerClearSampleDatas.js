/**
 * 시나라오별 필터 삭제 성공여부 데이터
 */
export const QueryStateManagerClearSampleDatas = {
  // 삭제 성공시
  success: {
    // 성공 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 상태 메시지
    message: '시나리오 필터의 삭제에 성공했습니다.',
  },
  // 로그아웃 실패시
  fail: {
    // 성공 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '시나리오 필터의 삭제에 실패했습니다.',
  },
};
