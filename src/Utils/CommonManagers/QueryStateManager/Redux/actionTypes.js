export const QUERY_INIT = 'QUERY_INIT';
export const QUERY_UPDATE = 'QUERY_UPDATE';
export const QUERY_CLEAR = 'QUERY_CLEAR';
export const COUNSELOR_LIST_INIT = 'COUNSELOR_LIST_INIT';
export const ORG_LIST_INIT = 'ORG_LIST_INIT';
