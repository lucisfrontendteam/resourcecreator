import * as actionTypes from './actionTypes';
import AjaxManager from '../../../AjaxManager';
import PropsCreator from '../../../PropsCreator';
import {
  QueryStateManagerReadManifest,
  QueryStateManagerUpdateManifest,
  QueryStateManagerClearManifest,
  QueryStateManagerReadSampleDatas,
  QueryStateManagerClearSampleDatas,
} from '../index';

export function orgListInitAction(targetOrganizationListState) {
  return {
    type: actionTypes.ORG_LIST_INIT,
    payload: { targetOrganizationListState },
  };
}

export function counselorListInitAction(targetCounselorListState) {
  return {
    type: actionTypes.COUNSELOR_LIST_INIT,
    payload: { targetCounselorListState },
  };
}

export function querystatemanagerreadInitAction(props, changedQueryState = {}) {
  const newProps = PropsCreator.getAddtionalProps(props);
  return dispatch => {
    const {
      NetWorkModeState,
      QueryIdMap,
      QueryStateManagerState,
    } = newProps;
    const newQueryStateManagerState = {
      ...QueryStateManagerState,
      ...changedQueryState,
      ...QueryIdMap,
    };
    const sampleData = {
      ...QueryStateManagerReadSampleDatas,
      ...newQueryStateManagerState,
    };
    const { method, apiUrl } = QueryStateManagerReadManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.QUERY_INIT,
      query: {
        advencedFilter: newQueryStateManagerState,
        ...QueryIdMap,
      },
      sampleData,
      ajaxType: 'normal',
    };
    AjaxManager.run(param);
  };
}

export function querystatemanagerupdateInitAction(props, changedQueryState = {}) {
  const newProps = PropsCreator.getAddtionalProps(props);
  return dispatch => {
    const {
      NetWorkModeState,
      QueryIdMap,
      QueryStateManagerState,
    } = newProps;
    const newQueryStateManagerState = {
      ...QueryStateManagerState,
      ...changedQueryState,
      ...QueryIdMap,
    };
    const sampleData = {
      ...QueryStateManagerReadSampleDatas,
      ...newQueryStateManagerState,
    };
    const stringifyQuery = JSON.stringify({
      advencedFilter: newQueryStateManagerState,
      ...QueryIdMap,
    });
    const { method, apiUrl } = QueryStateManagerUpdateManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.QUERY_UPDATE,
      query: stringifyQuery,
      sampleData,
      ajaxType: 'json',
    };
    AjaxManager.run(param);
  };
}

export function querystatemanagerclearInitAction(props) {
  const newProps = PropsCreator.getAddtionalProps(props);
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = newProps;
    const { method, apiUrl } = QueryStateManagerClearManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.QUERY_CLEAR,
      query: QueryIdMap,
      sampleData: QueryStateManagerClearSampleDatas.success,
      ajaxType: 'normal',
    };
    AjaxManager.run(param);
  };
}
