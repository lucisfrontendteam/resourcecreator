import * as actionTypes from './actionTypes';
import { AdvencedFilterManagerActionTypes } from '../../AdvencedFilterManager';

export function QueryStateManagerState(state = {}, action = null) {
  let finalState = {};
  switch (action.type) {
    case actionTypes.QUERY_INIT:
    case actionTypes.QUERY_UPDATE:
    case actionTypes.QUERY_CLEAR: {
      const { widgetState } = action.payload;
      finalState = { ...widgetState };
    } break;

    case actionTypes.COUNSELOR_LIST_INIT: {
      const { targetCounselorListState } = action.payload;
      const newState = {
        ...state,
        targetCounselorListState,
      };
      finalState = newState;
    } break;

    case actionTypes.ORG_LIST_INIT: {
      const { targetOrganizationListState } = action.payload;
      const newState = {
        ...state,
        targetOrganizationListState,
      };
      finalState = newState;
    } break;

    case AdvencedFilterManagerActionTypes.ADVENCED_FILTER_STATE_INIT: {
      // debugger;
      // const { targetOrganizationListState } = action.payload;
      // const newState = {
      //   ...state,
      //   targetOrganizationListState,
      // };
      // finalState = newState;
    } break;

    default: {
      finalState = { ...state };
    }
  }

  return finalState;
}
