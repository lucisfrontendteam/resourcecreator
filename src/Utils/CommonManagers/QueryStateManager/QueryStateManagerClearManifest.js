export const QueryStateManagerClearManifest = {
  id: 'QueryStateManagerClear',
  stateId: 'QueryStateManager',
  name: '쿼리 메니져',
  usage: '삭제하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'projectManagerClearUserId',
    },
    {
      key: 'projectId',
      id: 'projectManagerClearProjectId',
    },
    {
      key: 'scenarioId',
      id: 'projectManagerClearScenarioId',
    },
    {
      key: 'tabId',
      id: 'projectManagerClearTabId',
    },
  ],
  method: 'delete',
  apiUrl: '/project/scenarioQuery.api',
};
