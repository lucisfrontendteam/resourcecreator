/* eslint-disable */
/**
 * 시나라오별 필터 조건
 */
export const QueryStateManagerUpdateSampleDatas = {
  "TargetCounselorListState": [
    {
      "name": "하청 대장님",
      "number": 356,
      "id": 5
    }
  ],
  "TargetOrganizationListState": [
    {
      "name": "업무지원팀",
      "id": 1,
      "is_open": true
    }
  ],
  "TargetDateRangeState": "RANGE",
  "RecentDaysFilterState": 7,
  "DateRangeFromFilterState": "20161017",
  "TimeRangeFromFilterState": "0000",
  "DateRangeToFilterState": "20161024",
  "TimeRangeToFilterState": "0000",
  "TargetTimeSlotState": {
    "id": 3,
    "name": "저녁"
  },
  "TargetTelTimeFilterState": {
    "id": 3,
    "name": "3분 ~ 6분"
  },
  "TargetSilenceFilterState": {
    "id": 3,
    "name": "20% ~ 30%"
  },
  "TargetHoFilterState": {
    "id": 2,
    "name": "아웃바운드"
  },
  "TargetCsSpeechFilterState": {
    "id": 4,
    "name": "30% ~ 40%"
  },
  "TargetClientSpeechFilterState": {
    "id": 4,
    "name": "30% ~ 40%"
  },
  "TargetDupsFilterState": {
    "id": 3,
    "name": "20% ~ 30%"
  },
  "CustomDataTreeChildrenState": [
    {
      "name": "데이터 1-2",
      "id": 3,
      "parentid": 10
    },
    {
      "name": "데이터 2-1",
      "id": 8,
      "parentid": 20
    },
    {
      "name": "데이터 3-2",
      "id": 12,
      "parentid": 30
    },
    {
      "name": "데이터 4-2",
      "id": 14,
      "parentid": 40
    },
    {
      "name": "데이터 5-2",
      "id": 16,
      "parentid": 50
    },
    {
      "name": "데이터 6-2",
      "id": 18,
      "parentid": 60
    }
  ],
  "CategoryGroupFilterListState": [
    {
      "name": "카테고리 1",
      "id": 1,
    }
  ],
  "CategoryFilterListState": [
    {
      "name": "카테고리 2-2",
      "id": 9
    }
  ],
};
