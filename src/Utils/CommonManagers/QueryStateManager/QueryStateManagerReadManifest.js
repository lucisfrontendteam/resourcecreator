export const QueryStateManagerReadManifest = {
  id: 'QueryStateManagerRead',
  stateId: 'QueryStateManager',
  name: '쿼리 메니져',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'projectManagerUserId',
    },
    {
      key: 'projectId',
      id: 'projectManagerProjectId',
    },
    {
      key: 'scenarioId',
      id: 'projectManagerScenarioId',
    },
    {
      key: 'tabId',
      id: 'projectManagerTabId',
    },
  ],
  method: 'post',
  apiUrl: '/project/scenarioQuery.api',
};
