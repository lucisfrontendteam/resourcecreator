export class ProjectManager {
  constructor(props) {
    this.props = props;
  }

  getList() {
    // 서버에 프로젝트 리스트를 요청
    const { props } = this;
    props.projectmanagerreadInitAction(props);
  }
}
