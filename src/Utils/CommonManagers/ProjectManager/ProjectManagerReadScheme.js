/* eslint-disable */
export default {
  "$schema": "http://json-schema.org/draft-04/schema#",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "id": {
        "type": "integer"
      },
      "siteName": {
        "type": "string"
      },
      "accessPath": {
        "type": "string"
      },
      "owner": {
        "type": "string"
      },
      "createdAt": {
        "type": "string"
      },
      "thumbnail": {
        "type": "string"
      },
      // "ci": {
      //   "type": "string"
      // },
      "projectThemeId": {
        "type": "string"
      },
      "children": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "id": {
              "type": "integer"
            },
            "name": {
              "type": "string"
            },
            "uri": {
              "type": "string"
            },
            "children": {
              "type": "array",
              "items": {
                "type": "object",
                "properties": {
                  "id": {
                    "type": "integer"
                  },
                  "name": {
                    "type": "string"
                  },
                  "uri": {
                    "type": "string"
                  },
                  "viewerThemeId": {
                    "type": "string"
                  },
                  "widgetThemeId": {
                    "type": "string"
                  },
                  "widgetCoreIds": {
                    "type": "array",
                    "items": {
                      "type": "string"
                    }
                  }
                },
                "required": [
                  "id",
                  "name",
                  "uri",
                  "viewerThemeId",
                  "widgetThemeId",
                  "widgetCoreIds"
                ]
              }
            }
          },
          "required": [
            "id",
            "name",
            "uri",
            "children"
          ]
        }
      }
    },
    "required": [
      "id",
      "siteName",
      "accessPath",
      "owner",
      "createdAt",
      "thumbnail",
      // "ci",
      "projectThemeId",
      "children"
    ]
  }
};
