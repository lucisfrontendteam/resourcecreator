export const ProjectManagerThumbnailUploadManifest = {
  id: 'ProjectManagerThumbnailUpload',
  stateId: 'ProjectManager',
  name: '프로젝트 메니져',
  usage: '프로젝트 섬네일 수정하기',
  type: 'multipart',
  inputIdList: [
    {
      key: 'projectId',
      id: 'ProjectManagerThumbnailUploadprojectId',
    },
    {
      key: 'thumbnail',
      id: 'ProjectManagerThumbnailUploadthumbnail',
      type: 'file',
    },
  ],
  method: 'patch',
  apiUrl: '/project/thumbnail.api',
};
