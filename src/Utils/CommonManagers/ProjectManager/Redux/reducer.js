import _ from 'lodash';
import * as actionTypes from './actionTypes';
import { LoginManagerActionTypes } from '../../LoginManager';

export function ProjectManagerState(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case LoginManagerActionTypes.LOGOUT: {
      finalState = [];
    } break;

    case actionTypes.PROJECT_LIST_STATE_INIT: {
      const { widgetState } = action.payload;
      const { ProjectManagerState: targetState } = widgetState;
      if (targetState) {
        finalState = [...targetState];
      } else {
        const error =
          _.map(widgetState, (value, key) => ({ [key]: value }));
        finalState = [...error];
      }
    } break;

    case actionTypes.PROJECT_REMOVE: {
      const { widgetState } = action.payload;
      const { projectId } = widgetState;
      if (projectId) {
        finalState = _.filter(state,
          ({ id }) => _.toString(id) !== _.toString(projectId));
      } else {
        const error =
          _.map(widgetState, (value, key) => ({ [key]: value }));
        finalState = [...error];
      }
    } break;

    case actionTypes.PROJECT_EDIT_START:
    case actionTypes.PROJECT_EDIT_END: {
      const { projectId, isEditMode } = action.payload;
      finalState = _.map(state, project => {
        const newState = { ...project };
        if (project.id === projectId) {
          newState.isEditMode = isEditMode;
        }
        return newState;
      });
    } break;

    case actionTypes.PROJECT_THUMBNAIL_UPLOAD: {
      const { widgetState } = action.payload;
      const { projectId, thumbnail } = widgetState;
      finalState = _.map(state, project => {
        const newState = { ...project };
        if (_.toString(project.id) === _.toString(projectId)) {
          newState.thumbnail = thumbnail;
        }
        return newState;
      });
    } break;

    case actionTypes.PROJECT_CI_UPLOAD: {
      const { widgetState } = action.payload;
      const { projectId, ci } = widgetState;
      finalState = _.map(state, project => {
        const newState = { ...project };
        if (_.toString(project.id) === _.toString(projectId)) {
          newState.ci = ci;
        }
        return newState;
      });
    } break;

    default: {
      finalState = [...state];
    }
  }
  return finalState;
}
