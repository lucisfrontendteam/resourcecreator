import * as actionTypes from './actionTypes';
import AjaxManager from '../../../AjaxManager';
import {
  ProjectManagerReadManifest,
  ProjectManagerReadSampleDatas,
  ProjectManagerRemoveManifest,
  ProjectManagerThumbnailUploadSampleDatas,
  ProjectManagerThumbnailUploadManifest,
  ProjectManagerCiUploadSampleDatas,
  ProjectManagerCiUploadManifest,
} from '../index';

export function projectEditEndAction(projectId) {
  return {
    type: actionTypes.PROJECT_EDIT_END,
    payload: { projectId, isEditMode: false },
  };
}

export function projectEditStartAction(projectId) {
  return {
    type: actionTypes.PROJECT_EDIT_START,
    payload: { projectId, isEditMode: true },
  };
}

export function projectmanagerciuploadInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = ProjectManagerCiUploadManifest;
    const { ci } = ProjectManagerCiUploadSampleDatas;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.PROJECT_CI_UPLOAD,
      query: QueryIdMap,
      ajaxType: 'multipart',
      offline() {
        const projectId = QueryIdMap.get('projectId');
        dispatch({
          type: actionTypes.PROJECT_CI_UPLOAD,
          payload: {
            widgetState: {
              projectId,
              ci,
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function projectmanagerthumbnailuploadInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = ProjectManagerThumbnailUploadManifest;
    const { thumbnail } = ProjectManagerThumbnailUploadSampleDatas;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.PROJECT_THUMBNAIL_UPLOAD,
      query: QueryIdMap,
      ajaxType: 'multipart',
      offline() {
        const projectId = QueryIdMap.get('projectId');
        dispatch({
          type: actionTypes.PROJECT_THUMBNAIL_UPLOAD,
          payload: {
            widgetState: {
              projectId,
              thumbnail,
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function projectmanagerremoveInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = ProjectManagerRemoveManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.PROJECT_REMOVE,
      query: QueryIdMap,
      ajaxType: 'normal',
      offline() {
        const { projectId } = QueryIdMap;
        dispatch({
          type: actionTypes.PROJECT_REMOVE,
          payload: {
            widgetState: {
              projectId,
              showError: false,
            },
          },
        });
      },
    };
    AjaxManager.run(param);
  };
}

export function projectmanagerreadInitAction(props, getAll = false) {
  return dispatch => {
    const { NetWorkModeState, LoginManagerState } = props;
    const { method, apiUrl } = ProjectManagerReadManifest;
    let query = { ...LoginManagerState };
    if (getAll) {
      query = {
        ...LoginManagerState,
        userId: '',
      };
    } else {
      query = { ...LoginManagerState };
    }
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.PROJECT_LIST_STATE_INIT,
      query,
      ajaxType: 'normal',
      sampleData: {
        ProjectManagerState: ProjectManagerReadSampleDatas,
      },
      beforeDone(data) {
        return { ProjectManagerState: data };
      },
    };
    AjaxManager.run(param);
  };
}
