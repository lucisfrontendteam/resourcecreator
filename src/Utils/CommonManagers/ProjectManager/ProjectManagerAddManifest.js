export const ProjectManagerAddManifest = {
  id: 'ProjectManagerAdd',
  stateId: 'ProjectManager',
  name: '프로젝트 메니져',
  usage: '프로젝트 추가',
  type: 'IdList',
  inputIdList: [
    {
      key: 'projectId',
      id: 'ProjectManagerAddprojectId',
    },
  ],
  method: 'post',
  apiUrl: '/project/list.api',
};
