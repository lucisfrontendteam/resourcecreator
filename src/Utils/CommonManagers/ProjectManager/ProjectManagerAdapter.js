import _ from 'lodash';
import { ProjectManagerReadManifest } from './ProjectManagerReadManifest';
import scheme from './ProjectManagerReadScheme';
import { validate } from 'jsonschema';

export class ProjectManagerAdapter {
  constructor(projectManagerState) {
    const result = validate(projectManagerState, scheme);
    this.errors = [];
    if (!!!result.valid) {
      const messages = _.map(result.errors, error => {
        const { property, message } = error;
        return `${property} ${message}`;
      });
      this.errors = [
        `${ProjectManagerReadManifest.apiUrl} 을 점검하여 주십시오.`,
        ...messages,
      ];
    }
    this.projectManagerState = projectManagerState;
  }

  get defaultData() {
    return this.projectManagerState;
  }
}
