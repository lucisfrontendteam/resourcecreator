/**
 * 삭제 성공여부 샘플 데이터
 */
export const ProjectManagerRemoveSampleDatas = {
  // 삭제 성공시
  success: {
    // 로그인 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 프로젝트 아이디
    projectId: '삭제된 프로젝트 아이디',
  },
  // 삭제 실패시
  fail: {
    // 로그인 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '프로젝트 삭제에 실패했습니다.',
  },
};
