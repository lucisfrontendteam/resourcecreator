export const ProjectManagerAddSampleDatas = {
  success: {
    id: '서버에서 생성된 아이디',
    createdAt: '서버에 생성된 프로젝트 생성일자',
  },
  // 추가 실패시
  fail: {
    status: false,
    showError: true,
    message: '프로젝트 추가에 실패했습니다.',
  },
};
