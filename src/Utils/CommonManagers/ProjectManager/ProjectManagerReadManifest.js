export const ProjectManagerReadManifest = {
  id: 'ProjectManagerRead',
  stateId: 'ProjectManager',
  name: '프로젝트 메니져',
  usage: '읽기',
  type: 'LoginId',
  inputIdList: [
    {
      key: 'userId',
      id: 'ProjectManagerReaduserId',
    },
  ],
  method: 'get',
  apiUrl: '/project/list.api',
};
