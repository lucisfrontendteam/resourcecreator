export const ProjectManagerRemoveManifest = {
  id: 'ProjectManagerRemove',
  stateId: 'ProjectManager',
  name: '프로젝트 메니져',
  usage: '삭제하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'ProjectManagerRemoveId',
    },
    {
      key: 'projectId',
      id: 'ProjectManagerRemoveProjectId',
    },
  ],
  method: 'delete',
  apiUrl: '/project/list.api',
};
