export const ProjectManagerCiUploadManifest = {
  id: 'ProjectManagerCiUpload',
  stateId: 'ProjectManager',
  name: '프로젝트 메니져',
  usage: '프로젝트 ci 수정하기',
  type: 'multipart',
  inputIdList: [
    {
      key: 'projectId',
      id: 'ProjectManagerCiUploadprojectId',
    },
    {
      key: 'ci',
      id: 'ProjectManagerCiUploadci',
      type: 'file',
    },
  ],
  method: 'patch',
  apiUrl: '/project/ci.api',
};
