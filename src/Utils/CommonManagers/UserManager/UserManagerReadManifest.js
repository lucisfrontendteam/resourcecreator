export const UserManagerReadManifest = {
  id: 'UserManagerRead',
  stateId: 'UserManager',
  name: '유저 메니져',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'projectId',
      id: 'UserManagerReadprojectId',
    },
    {
      key: 'userId',
      id: 'UserManagerReaduserId',
    },
  ],
  method: 'get',
  apiUrl: '/user/list.api',
};
