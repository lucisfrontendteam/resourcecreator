export const UserManagerReadSampleDatas = [
  {
    id: '사용자 식별자 1',
    userName: '사용자명 1',
    userId: '사용자 아이디1',
    userPass: '사용자 패스워드1',
    userEmail: 'user1@somewhere.com',
    regDate: '20161115',
    userOrganization: '팔선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 2',
    userName: '사용자명 2',
    userId: '사용자 아이디2',
    userPass: '사용자 패스워드2',
    userEmail: 'user2@somewhere.com',
    regDate: '20161115',
    userOrganization: '왕팔선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 3',
    userName: '사용자명 3',
    userId: '사용자 아이디3',
    userPass: '사용자 패스워드3',
    userEmail: 'user3@somewhere.com',
    regDate: '20161115',
    userOrganization: '진짜대장선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 4',
    userName: '사용자명 4',
    userId: '사용자 아이디4',
    userPass: '사용자 패스워드4',
    userEmail: 'user4@somewhere.com',
    regDate: '20161115',
    userOrganization: '왕왕팔선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 5',
    userName: '사용자명 5',
    userId: '사용자 아이디5',
    userPass: '사용자 패스워드5',
    userEmail: 'user5@somewhere.com',
    regDate: '20161115',
    userOrganization: '왕왕왕팔선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 6',
    userName: '사용자명 6',
    userId: '사용자 아이디6',
    userPass: '사용자 패스워드6',
    userEmail: 'user6@somewhere.com',
    regDate: '20161115',
    userOrganization: '슈퍼대장선녀',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
  {
    id: '사용자 식별자 7',
    userName: '사용자명 7',
    userId: '사용자 아이디7',
    userPass: '사용자 패스워드7',
    userEmail: 'user7@somewhere.com',
    regDate: '20161115',
    userOrganization: '슈퍼슈퍼대장',
    userScenarioTree: [
      {
        id: 'UI-01',
        name: '카테고리 현황',
      },
      {
        id: 'UI-02-03',
        name: '벨류 콜 현황',
      },
    ],
    userOrganizationTree: [
      {
        id: '1',
        name: '업무지원팀',
      },
      {
        id: '3',
        name: '쫄병',
      },
    ],
  },
];
