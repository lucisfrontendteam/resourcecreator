export const UserManagerUpdateSampleDatas = {
  // 유저 수정 성공시
  success: {
    // 유저 수정 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 수정된 유저의 정보
    user: {
      id: '사용자 식별자1',
      userName: '사용자명 1',
      userId: '사용자 아이디1',
      userPass: '사용자 패스워드1',
      userEmail: 'user1@somewhere.com',
      regDate: '20161115',
      userOrganization: '팔선녀',
      userScenarioTree: [
        {
          id: 'UI-01',
          name: '카테고리 현황',
        },
        {
          id: 'UI-02-03',
          name: '벨류 콜 현황',
        },
      ],
      userOrganizationTree: [
        {
          id: '1',
          name: '업무지원팀',
        },
      ],
    },
  },
  // 유저 수정 실패시
  fail: {
    // 유저 수정 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '유저 수정에 실패했습니다.',
  },
};
