export const UserManagerNewManifest = {
  id: 'UserManagerNew',
  stateId: 'UserManager',
  name: '유저 메니져',
  usage: '추가하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'projectId',
      id: 'UserManagerNewprojectId',
    },
    {
      key: 'userId',
      id: 'UserManagerNewuserId',
      desc: '유저 아이디',
      length: 18,
    },
    {
      key: 'userName',
      id: 'UserManagerNewuserName',
      desc: '유저 이름',
      length: 18,
    },
    {
      key: 'userPass',
      id: 'UserManagerNewuserPass',
      desc: '패스워드',
      length: 18,
    },
    {
      key: 'userEmail',
      id: 'UserManagerNewuserEmail',
      desc: '이메일',
      length: 256,
    },
    {
      key: 'userOrganization',
      id: 'UserManagerNewuserOrganization',
      desc: '소속',
      length: 18,
    },
    {
      key: 'userScenarioTree',
      id: 'UserManagerNewuseraccessableScenarioTree',
      desc: '사용자가 볼 수 있는 시나리오',
      json: [
        {
          id: 'UI-01',
          name: '카테고리 현황',
        },
        {
          id: 'UI-02-03',
          name: '벨류 콜 현황',
        },
      ],
      rows: 10,
    },
    {
      key: 'userOrganizationTree',
      id: 'UserManagerNewuseraccessableOrganizationTree',
      desc: '사용자가 볼 수 있는 조직 데이터',
      json: [
        {
          id: '1',
          name: '업무지원팀',
        },
      ],
      rows: 10,
    },
  ],
  method: 'post',
  apiUrl: '/user/list.api',
};
