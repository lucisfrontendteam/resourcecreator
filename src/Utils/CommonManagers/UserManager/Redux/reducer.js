import * as actionTypes from './actionTypes';
import _ from 'lodash';

export function UserManagerState(state = [], action = null) {
  let finalState = [];
  switch (action.type) {
    case actionTypes.USER_SELECT: {
      const { userId } = action.payload;
      finalState = _.map(state, user => {
        const newUser = { ...user };
        if (newUser.userId === userId) {
          if (newUser.selected) {
            newUser.selected = false;
          } else {
            newUser.selected = true;
          }
        } else {
          newUser.selected = false;
        }
        return newUser;
      });
    } break;

    case actionTypes.USER_LIST_INIT: {
      const { widgetState } = action.payload;
      if (_.isEmpty(widgetState.UserManagerState)) {
        finalState = _.map(widgetState, (value, key) => {
          const errorField = {};
          errorField[key] = value;
          return errorField;
        });
      } else {
        finalState = [...widgetState.UserManagerState];
      }
    } break;

    case actionTypes.USER_DELETE: {
      const { widgetState } = action.payload;
      if (_.isEmpty(widgetState.userId)) {
        finalState = _.map(widgetState, (value, key) => {
          const errorField = {};
          errorField[key] = value;
          return errorField;
        });
      } else {
        const { userId } = widgetState;
        finalState = _.filter(state, user => user.userId !== userId);
      }
    } break;

    case actionTypes.USER_NEW: {
      const { widgetState } = action.payload;
      if (_.isEmpty(widgetState.user)) {
        finalState = _.map(widgetState, (value, key) => {
          const errorField = {};
          errorField[key] = value;
          return errorField;
        });
      } else {
        const { user } = widgetState;
        finalState = [user, ...state];
      }
    } break;

    case actionTypes.USER_UPDATE: {
      const { widgetState } = action.payload;
      if (_.isEmpty(widgetState.user)) {
        finalState = _.map(widgetState, (value, key) => {
          const errorField = {};
          errorField[key] = value;
          return errorField;
        });
      } else {
        const { user: updateUser } = widgetState;
        finalState = _.map(state, user => {
          let newUserState = { ...user };
          if (updateUser.userId === user.userId) {
            newUserState = {
              ...newUserState,
              ...updateUser,
              selected: false,
            };
          }
          return newUserState;
        });
      }
    } break;

    default: {
      finalState = [...state];
    }
  }

  return finalState;
}
