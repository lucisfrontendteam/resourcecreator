import * as actionTypes from './actionTypes';
import {
  UserManagerReadManifest,
  UserManagerReadSampleDatas,
  UserManagerDeleteManifest,
  UserManagerDeleteSampleDatas,
  UserManagerNewManifest,
  UserManagerNewSampleDatas,
  UserManagerUpdateManifest,
  UserManagerUpdateSampleDatas,
} from '../index';
import AjaxManager from '../../../AjaxManager';

export function userSelectAction(userId) {
  return {
    type: actionTypes.USER_SELECT,
    payload: { userId },
  };
}

export function usermanagerreadInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = UserManagerReadManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.USER_LIST_INIT,
      query: QueryIdMap,
      sampleData: {
        UserManagerState: UserManagerReadSampleDatas,
      },
      ajaxType: 'normal',
      beforeDone(data) {
        return {
          UserManagerState: data,
        };
      },
    };
    AjaxManager.run(param);
  };
}

export function usermanagerdeleteInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = UserManagerDeleteManifest;
    const sampleData = {
      ...UserManagerDeleteSampleDatas.success,
      ...QueryIdMap,
    };
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.USER_DELETE,
      query: QueryIdMap,
      ajaxType: 'normal',
      sampleData,
    };
    AjaxManager.run(param);
  };
}

export function usermanagernewInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = UserManagerNewManifest;
    const { user } = UserManagerNewSampleDatas.success;
    const newUser = { ...user, ...QueryIdMap };
    const sampleData = {
      ...UserManagerNewSampleDatas.success,
      user: newUser,
    };
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.USER_NEW,
      query: QueryIdMap,
      ajaxType: 'normal',
      sampleData,
    };
    AjaxManager.run(param);
  };
}

export function usermanagerupdateInitAction(props) {
  return dispatch => {
    const { NetWorkModeState, QueryIdMap } = props;
    const { method, apiUrl } = UserManagerUpdateManifest;
    const { user } = UserManagerUpdateSampleDatas.success;
    const newUser = { ...user, ...QueryIdMap };
    const sampleData = {
      ...UserManagerUpdateSampleDatas.success,
      user: newUser,
    };
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.USER_UPDATE,
      query: QueryIdMap,
      ajaxType: 'normal',
      sampleData,
    };
    AjaxManager.run(param);
  };
}
