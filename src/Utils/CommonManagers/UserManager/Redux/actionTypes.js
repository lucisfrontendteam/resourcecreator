export const USER_LIST_INIT = 'USER_LIST_INIT';
export const USER_DELETE = 'USER_DELETE';
export const USER_NEW = 'USER_NEW';
export const USER_SELECT = 'USER_SELECT';
export const USER_UPDATE = 'USER_UPDATE';
