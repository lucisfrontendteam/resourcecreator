/**
 * 유저 삭제 성공 / 실패시의 샘플 데이터
 */
export const UserManagerDeleteSampleDatas = {
  // 유저 삭제 성공시
  success: {
    // 유저 삭제 여부
    status: true,
    // 에러메세지 표시여부
    showError: false,
    // 삭제된 유저의 아이디
    userId: 'someone',
  },
  // 유저 삭제 실패시
  fail: {
    // 유저 삭제 여부
    status: false,
    // 에러메세지 표시여부
    showError: true,
    // 상태 메시지
    message: '유저 삭제에 실패했습니다.',
  },
};
