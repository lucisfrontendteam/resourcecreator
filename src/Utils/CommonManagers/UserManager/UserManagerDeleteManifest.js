export const UserManagerDeleteManifest = {
  id: 'UserManagerDelete',
  stateId: 'UserManager',
  name: '유저 메니져',
  usage: '삭제하기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'projectId',
      id: 'UserManagerDeleteprojectId',
    },
    {
      key: 'userId',
      id: 'UserManagerDeleteuserId,',
    },
  ],
  method: 'delete',
  apiUrl: '/user/list.api',
};
