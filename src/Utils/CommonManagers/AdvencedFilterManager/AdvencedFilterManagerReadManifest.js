export const AdvencedFilterManagerReadManifest = {
  id: 'AdvencedFilterManagerRead',
  stateId: 'AdvencedFilterManager',
  name: '필터 메니져',
  usage: '읽기',
  type: 'IdList',
  inputIdList: [
    {
      key: 'userId',
      id: 'projectManagerUserId',
    },
    {
      key: 'projectId',
      id: 'projectManagerProjectId',
    },
    {
      key: 'scenarioId',
      id: 'projectManagerScenarioId',
    },
    {
      key: 'tabId',
      id: 'projectManagerTabId',
    },
  ],
  method: 'get',
  apiUrl: '/project/filter.api',
};
