import * as actionTypes from './actionTypes';
import {
  AdvencedFilterManagerReadManifest,
  AdvencedFilterManagerReadSampleDatas,
} from '../index';
import AjaxManager from '../../../AjaxManager';
import PropsCreator from '../../../PropsCreator';

export function advencedfiltermanagerreadInitAction(props, changedQueryState = {}) {
  const newProps = PropsCreator.getAddtionalProps(props);
  return dispatch => {
    const {
      NetWorkModeState,
      QueryIdMap,
      QueryStateManagerState,
    } = newProps;
    const newQueryStateManagerState = {
      ...QueryStateManagerState,
      ...changedQueryState,
      ...QueryIdMap,
    };
    const sampleData = {
      ...AdvencedFilterManagerReadSampleDatas,
      ...newQueryStateManagerState,
    };
    const { method, apiUrl } = AdvencedFilterManagerReadManifest;
    const param = {
      onlineMode: NetWorkModeState.value,
      apiUrl,
      method,
      dispatch,
      actionType: actionTypes.ADVENCED_FILTER_STATE_INIT,
      query: {
        advencedFilter: newQueryStateManagerState,
        ...QueryIdMap,
      },
      sampleData,
      ajaxType: 'normal',
    };
    AjaxManager.run(param);
  };
}
