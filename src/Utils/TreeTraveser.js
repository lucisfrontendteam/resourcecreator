import TreeModel from 'tree-model';
import _ from 'lodash';

export default class TreeTraveser {
  constructor(treeData = {}) {
    this.setTreeData(treeData);
    this.treeModel = new TreeModel();
    this.setTree(this.treeData);
    return this;
  }

  idPredict(id) {
    return (node) => node.model.id === id;
  }

  uriPredict(uri) {
    return (node) => node.model.uri === uri;
  }

  setTreeData(treeData) {
    if (_.isArray(treeData)) {
      this.treeData = { children: treeData };
    } else {
      this.treeData = treeData;
    }
  }

  setTree(treeData) {
    this.tree = this.treeModel.parse(treeData);
  }

  getTree() {
    this.tree = this.treeModel.parse(this.treeData);
    return this.tree;
  }

  getNodeById(id = '') {
    const rst = this.getTree().first(this.idPredict(id));
    return rst;
  }

  getNodeByUri(uri = '') {
    const rst = this.getTree().first(this.uriPredict(uri));
    return rst;
  }

  getNodeByPredict(callback) {
    const rst = this.getTree().first(callback);
    return rst;
  }

  getNodeListByPredict(callback) {
    const rst = this.getTree().all(callback);
    return rst;
  }

  getLeaveNodes() {
    return this.getNodeListByPredict(
      node => _.isUndefined(node.model.children)
    );
  }

  getChildNodesModel() {
    return _.map(this.getChildNodes(), node => node.model);
  }

  groupPredict(node) {
    const { children, name } = node.model;
    let isRoot = false;
    if (_.isArray(children)) {
      const childFound = children.length > 0;
      const nameFound = !!!_.isUndefined(name);
      isRoot = childFound && nameFound;
    }
    return isRoot;
  }

  getGroupNodes() {
    const rootNodes = this.getNodeListByPredict(this.groupPredict);
    return rootNodes;
  }

  childPredict(node) {
    const { children } = node;
    let isChild = false;
    if (_.isArray(children)) {
      const childNotFound = children.length === 0;
      isChild = childNotFound;
    } else {
      isChild = true;
    }
    return isChild;
  }

  getChildNodes() {
    const childNodes = this.getNodeListByPredict(this.childPredict);
    return childNodes;
  }

  getRootNodesModel() {
    return _.map(this.getGroupNodes(), node => {
      const { model } = node;
      return {
        ...model,
        children: [],
      };
    });
  }

  getRootNode() {
    return this.getTree();
  }

  getRootModel() {
    const rootNode = this.getRootNode();
    return rootNode.model;
  }

  setNode(targetModel, node = {}) {
    const targetNode = {
      ...targetModel.model,
      ...node,
    };
    const parentNode = targetModel.parent;
    const index = targetModel.getIndex();
    targetModel.drop();
    // Parse the new node
    const newNode = this.treeModel.parse(targetNode);
    // Add it to the parent
    parentNode.addChildAtIndex(newNode, index);
  }
}
