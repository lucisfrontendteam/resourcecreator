export default class EnvironmentManager {
  constructor(environment) {
    this.environment = environment;
  }

  static nodeVariables() {
    return process.env;
  }

  static getNodeEnv() {
    const { NODE_ENV } = EnvironmentManager.nodeVariables();
    return NODE_ENV;
  }

  static isDevelopmentEnv() {
    return EnvironmentManager.getNodeEnv() === 'development';
  }

  static isProductionEnv() {
    return EnvironmentManager.getNodeEnv() === 'production';
  }

  static getDistType() {
    const { DIST_TYPE } = EnvironmentManager.nodeVariables();
    return DIST_TYPE;
  }

  static getBase64Prefix() {
    let prefix = '';
    if (EnvironmentManager.isDevelopmentEnv()) {
      prefix = '';
    } else {
      prefix = 'data:image/png;base64,';
    }
    return prefix;
  }

  get rootPath() {
    return this.environment[EnvironmentManager.getDistType()].rootPath;
  }

  get loginPath() {
    return `${this.rootPath}/login`;
  }

  get errorPath() {
    return `${this.rootPath}/error`;
  }
}
