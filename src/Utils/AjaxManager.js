import $ from 'jquery';
import _ from 'lodash';
import { LoginManagerActionTypes } from './CommonManagers/LoginManager';

export default class AjaxManager {
  static sessionValidate(data) {
    let ret = data;
    if (_.toString(data.status) === '401') {
      const $d = new $.Deferred();
      const newError = {
        showError: true,
        status: false,
        sessionStatus: false,
        statusText: '세션이 만료되었습니다',
        message: '세션이 만료되었습니다',
      };
      $d.reject(newError);
      ret = $d.promise();
    } else {
      ret = data;
    }
    return ret;
  }

  // static stausValidate(data) {
  //   debugger;
  //   return data;
  // }

  static createAjaxOptions(params) {
    const {
      apiUrl,
      method,
      query,
      ajaxType,
    } = params;

    const commonAjaxOption = {
      url: apiUrl,
      method,
      data: query,
      // timeout: 10000,
    };
    let ajaxOption = {};
    switch (ajaxType) {
      case 'json': {
        ajaxOption = {
          ...commonAjaxOption,
          dataType: 'json',
          contentType: 'application/json',
        };
      } break;

      case 'multipart': {
        ajaxOption = {
          ...commonAjaxOption,
          contentType: false,
          processData: false,
        };
      } break;

      case 'normal':
      default: {
        ajaxOption = { ...commonAjaxOption };
      } break;
    }
    return ajaxOption;
  }

  // static runMultiple(options = []) {
  //   const ajaxOptions =
  //     _.map(options, option => $.ajax(option).then(options.beforeDone));
  //   $.when.apply(undefined, ajaxOptions).done(datas => {
  //     debugger;
  //   });
  // }

  static ajaxSuccessHandler(dispatch, actionType, result) {
    dispatch({
      type: actionType,
      payload: {
        widgetState: {
          ...result,
          showError: false,
        },
      },
    });
  }

  static ajaxFailHandler(dispatch, err, apiUrl, actionType) {
    const { status, statusText, sessionStatus } = err;
    if (sessionStatus === false) {
      dispatch({
        type: LoginManagerActionTypes.LOGOUT,
        payload: {
          widgetState: err,
        },
      });
    } else {
      const widgetState = {
        err: `${status}: ${statusText}`,
        status,
        statusText,
        showError: true,
        message: `${apiUrl} 데이터 취득에 실패했습니다.`,
      };
      dispatch({
        type: actionType,
        payload: { widgetState },
      });
    }
  }

  static run(params) {
    const {
      onlineMode,
      apiUrl,
      dispatch,
      actionType,
      sampleData,
      beforeDone,
      offline,
    } = params;
    const newParam = AjaxManager.createAjaxOptions(params);
    // console.log('newParam', newParam);
    if (onlineMode) {
      $.ajax(newParam)
        .then(AjaxManager.sessionValidate)
        // .then(AjaxManager.stausValidate)
        .then(beforeDone)
        .done(result => AjaxManager.ajaxSuccessHandler(dispatch, actionType, result))
        .fail(err => AjaxManager.ajaxFailHandler(dispatch, err, apiUrl, actionType));
    } else {
      if (_.isFunction(offline)) {
        offline(params);
      } else {
        AjaxManager.ajaxSuccessHandler(dispatch, actionType, sampleData);
      }
    }
  }
}
