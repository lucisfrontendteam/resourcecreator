import _ from 'lodash';
import TreeTraverser from './TreeTraveser';

export default class PropsCreator {
  static getImageUploadProps(props, formdata) {
    const queryIds = {
      ...props,
      QueryIdMap: formdata,
    };
    return queryIds;
  }

  static getProjectEditProps(props, projectId) {
    const {
      LoginManagerState = {
        userId: '',
      },
    } = props;
    const queryIds = {
      ...props,
      QueryIdMap: {
        ...LoginManagerState,
        projectId,
      },
    };
    return queryIds;
  }

  static getPaginationProps(props, pageNumber, pageSize) {
    const { QueryIdMap: queryIdMap } = PropsCreator.getQueryProps(props);
    const queryIds = {
      ...props,
      QueryIdMap: {
        ...queryIdMap,
        pageSize,
        pageNumber,
      },
    };
    // console.log('getPaginationProps', queryIds.QueryIdMap.scenarioId);
    return queryIds;
  }

  static getUserManageProps(props, user = { userOrganizationTree: [] }) {
    const { ProjectState } = props;
    const filteredUserOrganizationTree
      = _.filter(user.userOrganizationTree, node => node.id !== 'dummy');
    const queryIds = {
      ...props,
      QueryIdMap: {
        ...user,
        projectId: ProjectState.id,
        userOrganizationTree: filteredUserOrganizationTree,
      },
    };
    return queryIds;
  }

  static getQueryProps(props) {
    const {
      LoginManagerState = {
        userId: '',
      },
      routeParams = {
        scenarioUri: '',
      },
      ProjectState,
    } = props;
    const treeTraverser = new TreeTraverser(ProjectState);
    const scenario
      = treeTraverser.getNodeByUri(routeParams.scenarioUri) || {
        parent: {
          model: {
            id: ProjectState.accessPath || '프로젝트 식별자',
          },
        },
        model: {
          id: '시나리오 식별자',
        },
      };
    const { model: parentModel } = scenario.parent;
    const { model: childModel } = scenario;
    const queryIds = {
      QueryIdMap: {
        userId: LoginManagerState.userId || '',
        projectId: parentModel.id,
        scenarioId: childModel.id,
        tabId: '',
      },
    };
    return queryIds;
  }

  // 특수 쿼리의 식별자를 props에 추가함
  static getAddtionalProps(props, additionalQuery) {
    const queryIds = PropsCreator.getQueryProps(props);
    return {
      ...props,
      ...queryIds,
      ...additionalQuery,
    };
  }

  // 청취목록용 식별자를 구한다
  static distinctDetailPlayerProps(props, pageNumber, pageSize) {
    const { NetWorkModeState, DetailPlayerState } = props;
    const QueryIdMap = {
      ...DetailPlayerState.coord,
      pageNumber,
      pageSize,
    };
    const newProps = { NetWorkModeState, QueryIdMap };
    return newProps;
  }

  // 청취목록용 식별자를 구한다
  static getDetailPlayerProps(props, additionalQuery, pageNumber = 1, pageSize = 25) {
    const { QueryIdMap } = PropsCreator.getQueryProps(props);
    const {
      value,
      CategoryFilterListState = {
        id: '',
      },
      TargetHoFilterState = {
        id: '',
      },
    } = additionalQuery;
    const newProps = {
      ...props,
      QueryIdMap: {
        ...QueryIdMap,
        ...value,
        CategoryFilterListStateId: CategoryFilterListState.id,
        TargetHoFilterStateId: TargetHoFilterState.id,
        pageSize,
        pageNumber,
      },
    };
    return newProps;
  }

  // 카테고리 그룹 이름 변경용 식별자를 구한다
  static getCategoryGroupProps(props, categoryGroup) {
    const { NetWorkModeState } = props;
    const { QueryIdMap } = PropsCreator.getQueryProps(props);
    const {
      id: categoryGroupId,
      name: categoryGroupName,
    } = categoryGroup;
    const newProps = {
      NetWorkModeState,
      QueryIdMap: {
        ...QueryIdMap,
        categoryGroupId,
        categoryGroupName,
      },
    };
    return newProps;
  }

  // 카테고리 노드 삭제용 식별자를 구한다
  static getCategoryRemoveProps(props, removeNodes, stateRemoveAfter) {
    const { NetWorkModeState } = props;
    const { QueryIdMap } = PropsCreator.getQueryProps(props);
    const newProps = {
      NetWorkModeState,
      QueryIdMap: {
        ...QueryIdMap,
        removeNodes,
        stateRemoveAfter,
      },
    };
    return newProps;
  }
}
