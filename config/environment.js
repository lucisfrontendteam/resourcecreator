var environment = {
  development: {
    rootPath: '',
  },
  production: {
    rootPath: '/pub/resourcecreator',
  },
};

module.exports = environment;
