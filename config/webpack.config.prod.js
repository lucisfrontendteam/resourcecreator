var path = require('path');
var autoprefixer = require('autoprefixer');
var webpack = require('webpack');
var CleanWebpackPlugin = require('clean-webpack-plugin');
// var Dashboard = require('webpack-dashboard');
// var DashboardPlugin = require('webpack-dashboard/plugin');
// var dashboard = new Dashboard();

// This shouldn't be exposed to the user.
var isInNodeModules = 'node_modules' ===
  path.basename(path.resolve(path.join(__dirname, '..', '..')));
var relativePath = isInNodeModules ? '../../..' : '..';
if (process.argv[2] === '--debug-template') {
  relativePath = '../template';
}
var srcPath = path.resolve(__dirname, relativePath, 'src');
var dashboardPath = path.resolve(__dirname, '../../userdashboard', 'src');
var nodeModulesPath = path.join(__dirname, '..', 'node_modules');
var buildPath = path.join(__dirname, isInNodeModules ? '../../..' : '../..', 'documents/contents');

module.exports = {
  bail: true,
  devtool: 'source-map',
  entry: {
    UIContents: path.join(srcPath, 'BuildEntry'),
  },
  output: {
    path: buildPath,
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    // Good news: we can infer it from package.json :-)
    publicPath: '/contents/',
    library: 'UIContents',
    libraryTarget: 'umd',
    umdNamedDefine: true,
  },
  resolve: {
    extensions: ['', '.js'],
  },
  resolveLoader: {
    root: nodeModulesPath,
    moduleTemplates: ['*-loader']
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint',
        include: [srcPath, dashboardPath]
      }
    ],
    loaders: [
      {
        test: /\.js$/,
        include: [srcPath, dashboardPath],
        loader: 'babel',
        query: require('./babel.prod')
      },
      {
        test: /\.css$/,
        include: [srcPath, dashboardPath],
        loaders: ['style', 'css'],
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.(ttf|woff|woff2|otf)$/,
        loader: 'file',
      },
      {
        test: /\.(jpg|png|gif|eot|svg)$/,
        loader: 'base64',
      },
      {
        test: /\.(mp4|webm)$/,
        loader: 'url?limit=10000'
      },
    ]
  },
  eslint: {
    // e.g. to enable no-console and no-debugger only in prod.
    configFile: path.join(__dirname, 'eslint.js'),
    useEslintrc: false,
    parser: 'babel-eslint',
  },
  postcss: function() {
    return [autoprefixer];
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        "NODE_ENV": JSON.stringify(process.env.NODE_ENV),
        "DIST_TYPE": JSON.stringify(process.env.DIST_TYPE),
      }
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      beautify: true,
      compressor: {
        screw_ie8: true,
        warnings: false
      },
      mangle: {
        screw_ie8: true
      },
      output: {
        comments: false,
        screw_ie8: true
      }
    }),
    // new ExtractTextPlugin('[name].css'),
    new CleanWebpackPlugin([buildPath], {
      // Without `root` CleanWebpackPlugin won't point to our
      // project and will fail to work.
      root: path.resolve(__dirname, '../..', 'documents/contents')
    }),
    // new DashboardPlugin(dashboard.setData),
  ]
};
